import sys
import json
from itertools import groupby
from copy import deepcopy

import numpy as np
import hdf5plugin
import h5py

from dials.array_family import flex
from dxtbx.model import Beam
from simtbx.nanoBragg.utils import ENERGY_CONV
from libtbx.phil import parse
from dxtbx.model import Detector, Panel
from dxtbx.format.Format import Format
from dxtbx.format.FormatStill import FormatStill
from dxtbx.format.FormatMultiImageLazy import FormatMultiImageLazy

phil = """
files {
  reborn_hdf5 = None
    .type = str
    .help = path to the raw data file
  reborn_geom = None
    .type = str
    .help = path to the reborn geometry
    .help = or path to the dxtbx geometry (experimentList file)
  inds = None
    .type = str
    .help = path to a text file where each line is a single number indicating indices of hits
}
paths {
  calib_data = 'calib'
    .type = str
    .help = path of the raw dataset in the files.raw 
  wavelength = None
    .type = str
    .help = path to the EBeam data
}
geom_type = *reborn dxtbx
  .type = choice
  .help = type of the files.geom
nominal_energy_eV = None
  .type = float
  .help = the expected photon energy in eV
adu_per_photon = 1
  .type = float
  .help = detector ADUs per photon
"""

# template panel dictionary
PDICT={'name': '',
 'type': '',
 'fast_axis': (1.0, 0.0, 0.0),
 'slow_axis': (0.0, -1.0, 0.0),
 'origin': (-38.5875, 19.2375, 0.0),
 'raw_image_offset': (0, 0),
 'image_size': (256,256),
 'pixel_size': (0.075, 0.075),
 'trusted_range': (-10.0, 2000000.0),
 'thickness': 0.32,
 'material': 'Si',
 'mu': 22.053630374879877,
 'identifier': '',
 'mask': [],
 'gain': 7.0008017184189875,
 'pedestal': 0.0,
 'px_mm_strategy': {'type': 'ParallaxCorrectedPxMmStrategy'}}

beam_descr = {'direction': (0.0, 0.0, 1.0),
             'divergence': 0.0,
             'flux': 1e12,
             'polarization_fraction': 1.,
             'polarization_normal': (0.0, 1.0, 0.0),
             'sigma_divergence': 0.0,
             'transmission': 1.0,
             'wavelength': 1.4}

MASTER_PHIL = parse(phil)

def get_center_of_asics(asics):
    asic_cents = []
    for a in asics:
        nf = a['n_fs']/2
        ns = a['n_ss']/2
        orig = np.array(a['t_vec'])
        fast = np.array(a['fs_vec'])
        slow = np.array(a['ss_vec'])
        pixsize = np.linalg.norm(fast)
        fast /= pixsize
        slow /= pixsize
    
        cent = (orig + nf*fast*pixsize + nf*slow*pixsize)*1000
        asic_cents.append( cent)
    panel_cent = np.mean(asic_cents,0)
    return panel_cent


class FormatReborn(FormatMultiImageLazy, FormatStill, Format):
    def __init__(self, image_file, **kwargs):
        FormatMultiImageLazy.__init__(self, **kwargs)
        FormatStill.__init__(self, image_file, **kwargs)
        Format.__init__(self, image_file, **kwargs)

    def _group_reborn_asics(self):
        get_panel = lambda x: x['name'].split("a")[0]
        panel_geoms = groupby(sorted(self.reborn_geom, key=get_panel), key=get_panel)
        self.name_to_asics = {panel_name:list(asics) for panel_name,asics in panel_geoms}

    def reborn_to_dxtbx(self):
        new_det = Detector()
        pg0 = new_det.hierarchy()
        for panel_name, asics in self.name_to_asics.items():
            pg1 = pg0.add_group()
            panel_center = get_center_of_asics(asics)
            panel_center[-1] *= -1
            pg1.set_local_frame(pg1.get_fast_axis(), pg1.get_slow_axis(), tuple(panel_center))
            for a in asics:
                origin = np.array(a['t_vec'])*1000
                origin[-1]*=-1
                origin -= panel_center
                fast_axis = np.array(a['fs_vec'])
                slow_axis = np.array(a['ss_vec'])
                pixsize = np.linalg.norm(fast_axis)
                fast_axis = tuple(fast_axis / pixsize)
                slow_axis = tuple(slow_axis / pixsize)
                PDICT['name'] = a['name']
                PDICT['slow_axis'] = slow_axis
                PDICT['fast_axis'] = fast_axis
                PDICT['origin'] = tuple(origin)
                PDICT['pixel_size'] = pixsize*1000, pixsize*1000
                PDICT['gain'] = self.params.adu_per_photon 
                pg1.add_panel(Panel.from_dict(PDICT))
        return new_det

    @staticmethod
    def understand(image_file, quiet=True):
        try:
            user_phil = parse(open(image_file, "r").read())
            params = MASTER_PHIL.fetch(sources=[user_phil]).extract()
            nfail = 0
            nfail += int(params.files.reborn_hdf5 is None)
            nfail += int(params.files.reborn_geom is None)
            if nfail > 0:
                if not quiet:
                    print("Fails!")
                return False
            else:
                return True
        except Exception as err:
            if not quiet:
                print("Fails!", err)
            return False

    def _start(self):
        super(FormatReborn, self)._start()

        user_phil = parse(open(self._image_file, "r").read())
        self.params = MASTER_PHIL.fetch(sources=[user_phil]).extract()

        file_handle = h5py.File(self.params.files.reborn_hdf5, "r")

        # make the detector
        self.reborn_geom = json.load(open(self.params.files.reborn_geom, 'r'))
        self._group_reborn_asics()
        self._dxtbx_detector = self.reborn_to_dxtbx()
        self.beam_descr = deepcopy(beam_descr)
        self.beam_descr["direction"] = 0, 0, -1
        self.beam_descr["wavelength"] = ENERGY_CONV / self.params.nominal_energy_eV

        self.CALIB = file_handle[self.params.paths.calib_data]
        num_total_img = self.CALIB.shape[0]
        if self.params.files.inds is not None:
            self.hit_inds = np.loadtxt(self.params.files.inds, str).astype(int)
        else:
            self.hit_inds = np.arange(num_total_img)

        # TODO load per shot energies from the hdf5
        self._nominal_energies = np.ones(num_total_img)*self.params.nominal_energy_eV

    def get_beam(self, index=None):
        beam_model = Beam.from_dict(self.beam_descr)
        if index is not None:
            index = self.hit_inds[index]
            nom_en = self._nominal_energies[index]
            beam_model.set_wavelength(ENERGY_CONV /nom_en)
        return beam_model

    def get_num_images(self):
        return len(self.hit_inds)

    def get_detectorbase(self, index=None):
        raise NotImplementedError

    def get_raw_data(self, index):
        index = self.hit_inds[index]
        num_asics = len(self.reborn_geom)
        parent_shape = self.reborn_geom[0]['parent_data_shape']
        image_stack = self.CALIB[index].reshape(parent_shape)
        asic_images = []
        for p,asics in self.name_to_asics.items():
            for asic_geom in asics:
                Y,X = asic_geom['parent_data_slice']
                Y = slice(Y[0],Y[1],1)
                X = slice(X[0],X[1],1)
                asic_image = np.ascontiguousarray(image_stack[Y,X])
                asic_image = flex.double(asic_image)
                asic_images.append(asic_image)
        asic_images = tuple(asic_images)
        return asic_images

    def get_detector(self, index=None):
        return self._dxtbx_detector


if __name__=="__main__":
    fname = sys.argv[1]
    F = FormatReborn(fname)
    asic_imgs = F.get_raw_data(index=0)
    det = F.get_detector(0)
    from dxtbx.model import ExperimentList, Experiment
    import os
    E = Experiment()
    E.detector = det
    E.beam = F.get_beam()
    El = ExperimentList()
    El.append(E)
    fname="temp_format_reborn.expt"
    El.as_file(fname)
    os.system("dxtbx.plot_detector_models %s" % fname)
    num_asics = len(det)
    assert num_asics==len(asic_imgs)
    print("average over asic pixels:")
    for i in range(num_asics):
        name = det[i].to_dict()['name']
        print(f"Asic {name} ({i+1}/{num_asics}) has ave signal= {np.mean(asic_imgs[i])}")

