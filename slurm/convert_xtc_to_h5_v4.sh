#!/bin/bash
#SBATCH --partition=milano
#SBATCH --account=lcls:cxily5921
#SBATCH --job-name=xtc2h5
#SBATCH --output=slurm/logs/convert_xtc_to_h5_v4-%j.output
#SBATCH --error=slurm/logs/convert_xtc_to_h5_v4-%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=24
#SBATCH --mem-per-cpu=2g
#SBATCH --time=0-5:00:00

# Run this from the root directory of the repository (cxily59)

if [ -z ${2+x} ];
then
    # if n_cores not given set to 12 by default
    j=24
else
    j=$2
fi

if [ -z ${1+x} ];
then
    echo "usage: sbatch convert_xtc_to_h5_v2.sh run_number n_cores"
    echo "should be run from root of git repo"
    exit
else
   run=$1
fi

[[ -d slurm/logs ]] || mkdir -p slurm/logs

source setup.sh
python -u convert_xtc_to_h5_v4.py -r=${run} -j=${j}
