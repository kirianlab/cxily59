#!/bin/bash
#SBATCH --partition=milano
#SBATCH --account=lcls:cxily5921
#SBATCH --job-name=h5_basic_analysis
#SBATCH --output=h5_basic_analysis-%j.output
#SBATCH --error=h5_basic_analysis-%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --mem-per-cpu=8g
#SBATCH --time=0-5:00:00

if [ -z ${2+x} ];
then
    # if n_cores not given set to 12 by default
    j=12
else
    j=$2
fi

if [ -z ${1+x} ];
then
    echo "usage: sbatch convert_xtc_to_h5.sh run_number n_cores"
    echo "should be run from root of git repo"
    exit
else
   run=$1
fi

source /sdf/group/lcls/ds/ana/sw/conda1/manage/bin/psconda.sh;
export PYTHONPATH=$PYTHONPATH:"/sdf/data/lcls/ds/cxi/cxily5921/scratch/rca/reborn/";
python -u h5_basic_analysis.py -r=${run} -j=${j}
