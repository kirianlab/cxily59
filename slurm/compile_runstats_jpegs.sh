#!/bin/bash

base_directory="/sdf/data/lcls/ds/cxi/cxily5921/results/cxily59/results/runstats/"

for v in 'mean' 'sdev' 'min' 'max'; do
    mkdir -p "$base_directory$v"
done

cd $base_directory || exit

for run in r*/; do
    for v in mean sdev min max; do
        f="$base_directory$run$v.jpg"
        r=$(echo $run | cut -c -5)
        if [ ! -f $f ]; then
            echo "There is no runstats for $r. Try running 'python runstats.py -r $r'"
        else
            ln -s $f "$base_directory$v/$r.jpg"
        fi
    done
done
