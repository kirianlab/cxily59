#!/bin/bash
#SBATCH --job-name=runstats_slurm_job    # Job name
#SBATCH --ntasks=1                    # set nodes equal to ntasks
##SBATCH --nodes=12
##SBATCH --exclusive
#SBATCH --cpus-per-task=64
#SBATCH --mem-per-cpu=6G
#SBATCH --time=02:00:00               # Time limit hrs:min:sec
#SBATCH --output=s3df_slurm_test_%j.log   # Standard output and error log
#SBATCH --partition=milano
#SBATCH --account=lcls:cxily5921
##SBATCH --reservation=lcls

#usage: sbatch runstats_sbatch.sh run_number ncores
#should be run from cxily59 git repo directory

j=$((SLURM_CPUS_PER_TASK*SLURM_JOB_NUM_NODES))

if [ -z ${1+x} ];
then
    #if no run number given, fuck off
    echo "usage: sbatch runstats_sbatch.sh run_number ncores"
    echo "should be run from cxily59 git repo directory"
    exit
else
   run=$1
fi


source /sdf/group/lcls/ds/ana/sw/conda1/manage/bin/psconda.sh;
export PYTHONPATH=$PYTHONPATH:"/sdf/data/lcls/ds/cxi/cxily5921/results/tgrant/cxily59//reborn/"
#python -u runstats.py --run=${run} --n_processes=${j} ${2}

python -u runstats.py --n_processes=${j} $@
