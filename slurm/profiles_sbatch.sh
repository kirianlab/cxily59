#!/bin/bash
#SBATCH --job-name=profiles_slurm_job  # Job name
#SBATCH --ntasks=1                     # Run on a single CPU
#SBATCH --cpus-per-task=12
#SBATCH --mem-per-cpu=8G
#SBATCH --time=00:05:00                # Time limit hrs:min:sec
#SBATCH --output=s3df_slurm_test_%j.log  # Standard output and error log
#SBATCH --partition=milano
#SBATCH --account=lcls:cxily5921

#usage: sbatch runstats_sbatch.sh run_number ncores
#should be run from cxily59 git repo directory
if [ -z ${2+x} ];
then
    #if ncores not given set to 12 by default
    j=12
else
    j=$2
fi

if [ -z ${1+x} ];
then
    #if no run number given, fuck off
    echo "usage: sbatch runstats_sbatch.sh run_number ncores"
    echo "should be run from cxily59 git repo directory"
    exit
else
   run=$1
fi

source /sdf/group/lcls/ds/ana/sw/conda1/manage/bin/psconda.sh;
export PYTHONPATH=$PYTHONPATH:"/sdf/data/lcls/ds/cxi/cxily5921/results/tgrant/cxily59//reborn/"
python -u profiles.py -r=${run} -j=${j}
