#!/usr/bin/env python
import argparse
import numpy as np
import pyqtgraph as pg
import matplotlib.pyplot as plt
import joblib
from reborn.external.pyqtgraph import imview
from reborn.external.lcls import LCLSFrameGetter
from reborn import analysis
import config
import reborn
import os

default_config = config.default_config()
memory = joblib.Memory(default_config["joblib_directory"])

@memory.cache(ignore=["n_processes"])
def get_runstats(run_number=1, n_processes=12):
    r"""Fetches some PAD statistics for a run. See reborn docs."""
    conf = config.get_config(run_number)
    log_file = os.path.dirname(conf["runstats"]["log_file"])
    if not os.path.isdir(log_file):
        if not os.path.isdir(os.path.dirname(log_file)):
            os.mkdir(os.path.dirname(log_file))
        os.mkdir(log_file)
    runstats_conf = conf["runstats"]
    runstats_conf["stop"] = int(1e7)
    runstats_conf["start"] = 0
    runstats_conf["n_processes"] = n_processes
    detectors = conf["pad_detectors"]
    for d in detectors:
        d["mask"] = None
    framegetter = LCLSFrameGetter(
        run_number=run_number,
        max_events=runstats_conf["stop"],
        experiment_id=conf["experiment_id"],
        pad_detectors=detectors,
        cachedir=conf["cachedir"],
        photon_wavelength_pv=conf["photon_wavelength_pv"]
    )
    padstats = analysis.runstats.ParallelPADStats(
        framegetter=framegetter, max_sum=None, min_sum=None, **runstats_conf
    )
    padstats.process_frames()
    stats = padstats.to_dict()
    return stats


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--run", type=int, default=154, help="Run number")
    parser.add_argument(
        "-j", "--n_processes", type=int, default=12, help="Number of parallel processes"
    )
    args = parser.parse_args()
    print(f"Fetching runstats...")
    stats = get_runstats(run_number=args.run, n_processes=args.n_processes)

    # Generate a sample 2D array
    # Replace this with your own data
    data = stats["histogram"].astype(float)
    data = np.log(data)

    # Choose one column to plot
    column_to_plot = 2  # Change this to the column you want to plot

    # Create a figure with subplots
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 5))

    # Plot the 2D array on a log scale
    im = ax1.imshow(data, cmap='viridis', aspect='auto', interpolation='none')
    plt.colorbar(im, ax=ax1)

    # Plot one column from the array
    x = np.arange(data.shape[0])  # X-axis values
    y = data[:, column_to_plot]  # Y-axis values

    ax2.plot(x, y, marker='o', linestyle='-', color='b')
    ax2.set_xlabel('Index')
    ax2.set_ylabel('Column {}'.format(column_to_plot))
    ax2.set_title('Plot of Column {} from the Array'.format(column_to_plot))

    # Show the figure
    plt.tight_layout()
    plt.show()
