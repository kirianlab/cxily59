import time
import psana

evt_ts = [
(1691557377, 82193715, 7719),
(1691557377, 90506837, 7722),
(1691557377, 98864475, 7725),
(1691557377, 107188126, 7728),
(1691557377, 115510995, 7731),
(1691557377, 123870390, 7734),
(1691557377, 132221343, 7737),
(1691557377, 140563626, 7740),
(1691557377, 148889538, 7743),
(1691557377, 157227726, 7746),
]


ds = psana.DataSource('exp=cxily5921:run=131:idx')
det = psana.Detector("jungfrau4M")
run = ds.runs().__next__()
for i in range(len(evt_ts)):
	ts = evt_ts[i]
	evt = run.event(psana.EventTime(int((ts[0] << 32) | ts[1]), ts[2]))
	t1 = time.time()
	data = det.calib(evt)
	# data = self.detector.calib(event)
	print(time.time()-t1)