import psana
experiment_id = "cxily5921"
run_number = 1
data_string = f'exp={experiment_id}:run={run_number}:smd'       
data_source = psana.DataSource(data_string)
jungfrau_detector = psana.Detector("jungfrau4M")
for evt in data_source.events():
    evtId = evt.get(psana.EventId)                 
    jungfrau_data = jungfrau_detector.raw(evt)
    print(jungfrau_data)
