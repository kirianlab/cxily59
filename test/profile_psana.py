import psana
from time import time
experiment_id = "cxily5921"
run_number = 131
data_string = f'exp={experiment_id}:run={run_number}:smd'
data_source = psana.DataSource(data_string)
jungfrau_detector = psana.Detector("jungfrau4M")
counter = 0
event_ids = []
max_events = 10
evt = data_source.events().__next__()
d = jungfrau_detector.raw(evt)
t0 = time()
for n,evt in enumerate(data_source.events()):
    evtId = evt.get(psana.EventId)
    event_ids.append(evtId.time() + (evtId.fiducials(),))
    jungfrau_data = jungfrau_detector.raw(evt)
    if n>max_events:
        break
print("smd: %0.5f"%((time()-t0)/max_events))

data_string = f'exp={experiment_id}:run={run_number}:idx'
data_source = psana.DataSource(data_string)
run = data_source.runs().__next__()
t1 = time()
for i in range(10):
    ts = event_ids[i]
    evt = run.event(psana.EventTime(int((ts[0] << 32) | ts[1]), ts[2]))
    jungfrau_data = jungfrau_detector.raw(evt)
    if i > max_events:
        break

print("idx: %0.5f"%((time()-t1)/max_events))
