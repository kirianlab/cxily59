import argparse
import h5py
import numpy as np
import os
import pandas
import reborn
from glob import glob
from joblib import Parallel, delayed
from reborn.analysis import fluctuations
from reborn.detector import load_pad_masks, PADGeometryList, PolarPADAssembler
from reborn.source import Beam
from time import time


def analysis_worker(run_number, data_type, data_dir, results_dir,
                    frames, indices, pad_geometry, beam, mask,
                    sample_diameter, oversample, threshold,
                    _pid):
    n_frames_in_chunk = frames.shape[0]
    chunk_size = int(n_frames_in_chunk / 100)
    evids = np.array_split(frames, chunk_size)
    ididx = np.array_split(indices, chunk_size)
    assembler = PolarPADAssembler.from_resolution(pad_geometry=pad_geometry,
                                                  beam=beam,
                                                  sample_diameter=sample_diameter,
                                                  oversample=oversample)
    polar_mask = assembler.bin_sum(array=mask)
    n_q, n_p = assembler.polar_shape
    kacf = np.zeros((n_q, n_p))  # Kam FXS auto-correlation function
    pad_sum = geometry.zeros()
    pad_sum2 = geometry.zeros()
    pad_min = 1e3 * geometry.ones()
    pad_max = geometry.zeros()
    os.makedirs(f"{results_dir}/{run_number}", exist_ok=True)
    with h5py.File(f"{results_dir}/{run_number}/process_{_pid}.h5", "w") as hf:
        hf.create_dataset(name=f"sample_diameter",
                          data=sample_diameter)
        hf.create_dataset(name=f"oversample",
                          data=oversample)
        hf.create_dataset(name=f"threshold",
                          data=threshold)
        hf.create_dataset(name=f"mask",
                          data=mask)
        hf.create_dataset(name=f"n_q_bins",
                          data=n_q)
        hf.create_dataset(name=f"n_phi_bins",
                          data=n_p)
        with h5py.File(f"{data_dir}/{run_number}.h5", "r") as h5:
            for j, (_id, _ix) in enumerate(zip(evids, ididx)):
                print(f"Process {_pid}: Analyzing frames.")
                data = np.double(h5[f"data/{data_type}"][_ix])  # load ~100 patterns (~3.3 GB)
                data[data < threshold] = 0
                empty_frames = data.sum(axis=1) > 0
                data = data[empty_frames]  # mask frames without data
                evnt = _id[empty_frames]
                pad_sum[mask > 0] += data.sum(axis=0)[mask > 0]
                pad_sum2[mask > 0] += (data ** 2).sum(axis=0)[mask > 0]
                for i, (_d, _e) in enumerate(zip(data, evnt)):
                    print(f"\t Process {_pid}: Analyzing frame: {_e}")
                    pad_min[mask > 0] = np.minimum(pad_min[mask > 0], _d[mask > 0])
                    pad_max[mask > 0] = np.maximum(pad_min[mask > 0], _d[mask > 0])
                    polar_data = assembler.bin_sum(array=_d)
                    hf.create_dataset(name=f"polar_data/{data_type}/{j}_{i}",
                                      data=polar_data.ravel())
                    hf.create_dataset(name=f"event_ids/{j}_{i}",
                                      data=polar_data.ravel())
                    d = fluctuations.subtract_masked_data_mean(data=polar_data, mask=polar_mask)
                    kacf += fluctuations.correlate(s1=d)
    return dict(kam_acf=kacf, pad_sum=pad_sum, pad_sum2=pad_sum2, pad_max=pad_max, pad_min=pad_min)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--run", type=int, default=158, help="Run number")
    parser.add_argument("--start", type=int, default=None, help="start frame number")
    parser.add_argument("--stop", type=int, default=None, help="stop frame number")
    parser.add_argument("--max_events", type=int, default=1e7, help="Maximum number of events to process")
    parser.add_argument("-j", "--n_processes", type=int, default=12, help="Number of parallel processes")
    args = parser.parse_args()

    tik = time()
    njobs = args.n_processes
    run_number = args.run
    experiment_id = "cxily5921"
    # load experiment log
    log_file = "./cxily5921_run_log.csv"
    df = pandas.read_csv(filepath_or_buffer=log_file)
    elog = df.set_index("Run").replace({float("nan"): None}).to_dict(orient="index")
    log = elog[run_number]
    sample_name = log["Sample"]
    try:
        # convert to meters (always use SI units)
        sample_diameter = float(log["Size (nm)"]) * 1e-9
    except ValueError:
        sample_diameter = None
        # use the sample diameter to setup the polar binning code
        # buffer runs also use the smaple diameter to sample identically
        raise RuntimeError("This run does not have sample configured!")
    oversample = 6
    one_photon_peak = 7  # keV
    threshold = one_photon_peak / 2
    data_type = "calib"
    data_dir = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data"
    results_dir = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/analysis"
    mask_file = f"{results_dir}/geometry/dead_pixels.mask"
    radial_statistics = ("sum", "sum2", "weight_sum", "mean", "sdev", "median")
    beam = Beam(photon_energy=7 * 1e3 * reborn.const.eV)
    with h5py.File(f"{data_dir}/{run_number}.h5", "r") as h5:  # load geometry & event ids
        geometry_file = h5["geometry_file"][()].decode()
        good_ids = h5["event_ids_good"][()]
        # good_ids_all = h5["event_ids_good"][()]
        # good_ids = good_ids_all[good_ids_all.sum(axis=1) > 0]  # some data seems to be missing (ignore those frames)
    geometry = PADGeometryList(filepath=geometry_file)
    try:
        mask = load_pad_masks(mask_file)
    except FileNotFoundError:
        mask = np.ones(geometry.n_pixels)
    n_frames = good_ids.shape[0]
    # each process will handle these frames
    jobs = np.array_split(good_ids, njobs, axis=0)
    job_ids = np.array_split(np.arange(n_frames), njobs, axis=0)
    out = Parallel(n_jobs=njobs)(delayed(analysis_worker)(run_number=run_number,
                                                          data_type=data_type,
                                                          data_dir=data_dir,
                                                          results_dir=results_dir,
                                                          frames=frames,
                                                          indices=js,
                                                          pad_geometry=geometry,
                                                          beam=beam,
                                                          mask=mask,
                                                          sample_diameter=sample_diameter,
                                                          oversample=oversample,
                                                          threshold=threshold,
                                                          _pid=i)
                                                          for i, (frames, js) in enumerate(zip(jobs, job_ids)))
    kam_acf = out[0]["kam_acf"]
    pad_sum = out[0]["pad_sum"]
    pad_sum2 = out[0]["pad_sum2"]
    pad_max = out[0]["pad_max"]
    pad_min = out[0]["pad_min"]
    for o in out[1:]:
        kam_acf += o["kam_acf"]
        pad_sum += o["pad_sum"]
        pad_sum2 += o["pad_sum2"]
        pad_max = np.maximum(pad_max, o["pad_max"])
        pad_min = np.minimum(pad_min, o["pad_min"])
    print(f"Analyzing data took {time() - tik} s.")
    # create the primary run file
    tik = time()
    print(f"Setting up virtual dataset for run {run_number}.")
    # align frames so they match the order in which the data was read
    donef = sorted(glob(f"{results_dir}/{run_number}/process_*.h5"),
                   key=lambda x: int(x.split("/")[-1].split("_")[-1].split(".")[0]))
    print(f"\tCreating virtual layout.")
    # polar data layout
    n_q, n_phi = kam_acf.shape
    dlayout = []
    elayout = []
    for f in donef:
        with h5py.File(f, "r") as h5:
            dkeys = set(h5[f"polar_data/{data_type}"].keys())
            ekeys = set(h5[f"event_ids"].keys())
        for _dk, _ek in zip(dkeys, ekeys):
            dlayout.append(h5py.VirtualSource(path_or_dataset=f,
                                              name=f"polar_data/{data_type}/{_dk}",
                                              shape=n_q * n_phi))
            elayout.append(h5py.VirtualSource(path_or_dataset=f,
                                              name=f"event_ids/{_ek}",
                                              shape=3))
    d_layout = h5py.VirtualLayout(shape=(len(dlayout), n_q * n_phi),
                                  dtype=np.float64)
    e_layout = h5py.VirtualLayout(shape=(len(elayout), 3),
                                  dtype=np.float64)
    for i, (_d, _e) in enumerate(zip(dlayout, elayout)):
        d_layout[i] = _d
        e_layout[i] = _e
    with h5py.File(f"{results_dir}/{run_number}.h5", "w") as hf:
        hf.create_dataset(name=f"n_frames_analyzed",
                          data=len(dlayout))
        hf.create_dataset(name=f"sample_diameter",
                          data=sample_diameter)
        hf.create_dataset(name=f"oversample",
                          data=oversample)
        hf.create_dataset(name=f"threshold",
                          data=threshold)
        hf.create_dataset(name=f"mask",
                          data=mask)
        hf.create_dataset(name=f"n_q_bins",
                          data=n_q)
        hf.create_dataset(name=f"n_phi_bins",
                          data=n_phi)
        hf.create_virtual_dataset(name=f"event_ids_analyzed",
                                  layout=e_layout,
                                  fillvalue=0)
        hf.create_virtual_dataset(name=f"polar_data/{data_type}",
                                  layout=d_layout,
                                  fillvalue=0)
        hf.create_dataset(name=f"polar_data/{data_type}_kam_acf_sum",
                          data=kam_acf)
        hf.create_dataset(name=f"stats/pad_sum",
                          data=pad_sum)
        hf.create_dataset(name=f"stats/pad_sum2",
                          data=pad_sum2)
        hf.create_dataset(name=f"stats/pad_max",
                          data=pad_max)
        hf.create_dataset(name=f"stats/pad_min",
                          data=pad_min)
    tok = time()
    print(f"Analysis of run {run_number} took {time() - tik} (s)")
