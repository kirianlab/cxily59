#!/usr/bin/env python
import argparse
from reborn.external.lcls import LCLSFrameGetter
from reborn.viewers.qtviews import PADView
import runstats
from config import default_config
import numpy as np
from reborn.detector import load_pad_geometry_list, load_pad_masks
from reborn.source import Beam
import matplotlib.pyplot as plt
from reborn.analysis.saxs import RadialProfiler
from scipy import stats

def find_nearest_i(array,value):

    """Return the index of the array item nearest to specified value"""

    return (np.abs(array-value)).argmin()

def index(x, x_min, x_max, n):
    delta = (x_max - x_min) / (n - 1)
    return int((x - x_min + 0.5) / delta)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--r_background', type=int, default=60, help='Run number of the background')
    parser.add_argument('--r_signal', type=int, default=61, help='Run number of the signal')
    parser.add_argument("--mask", type=str, help="Path to reborn mask file.",
                        default="./geometry/dead_pixels.mask")
    parser.add_argument("--geometry", type=str, help="Path to geometry file.",
                        default="./geometry/2023.08.05AgBhnt_r15.json")
    parser.add_argument( '--save_radial_profile_denss',
                    action='store_true', help='save radial profile in a format denss can read')
    parser.add_argument( '--view',
                    action='store_true', help='view the difference frame')
    parser.add_argument('--q_min_guinier', type=float, default=None, help='lower bound of "good" q region to fit gunier plot')
    parser.add_argument('--q_max_guinier', type=float, default=None, help='upper bound gunier plot fit')
    args = parser.parse_args()
    config = default_config()

    stats_bg = runstats.get_runstats(args.r_background)
    mean_bg = stats_bg['sum'] / np.array(stats_bg['n_frames'])
    
    stats_s = runstats.get_runstats(args.r_signal)
    mean_s = stats_s['sum'] / np.array(stats_s['n_frames'])
    diff_2d = mean_s - mean_bg
     
    if args.view:
        pv = PADView(data=diff_2d, pad_geometry=config['pad_detectors'][0]["geometry"], debug_level=1, percentiles=(10, 90), post_processors=[lambda x: np.log(x)])
        pv.set_mask_color([128, 0, 0, 75])
        pv.start()

    base_geometry = load_pad_geometry_list(file_name=args.geometry)
    base_mask = load_pad_masks(file_name=args.mask)

    beam=Beam(file_name="geometry/beam.json")#FIXME: right beam?
    rp = RadialProfiler(beam=beam, pad_geometry=base_geometry, mask=base_mask)
    q = rp.bin_centers
    radial_profile = rp.get_profile_statistic(diff_2d, statistic=np.mean)
    sample_radial = rp.get_profile_statistic(mean_s, statistic=np.mean)
    background_radial = rp.get_profile_statistic(mean_bg, statistic=np.mean)

    if args.view:
        plt.plot(q*1e-10,sample_radial,label='sample run %d'%args.r_signal)
        plt.plot(q*1e-10,background_radial,label='background run %d'%args.r_background)
        plt.xlabel('q (1/A)')
        plt.ylabel('I(q)')
        plt.legend()
        plt.savefig('r%d-%d_radial_plot.png',dpi=150)
        plt.show()

    if args.save_radial_profile_denss:
        data = np.empty((np.shape(q)[0], 3))
        data[:,0] = q * 1e-10
        data[:,1] = radial_profile
        data[:,2] = radial_profile * 0.01
        np.savetxt(f'results/r{args.r_signal}-r{args.r_background}radial_profile.dat', data)
        
    if args.view:
        plt.plot(q,radial_profile)
        plt.title("radial profile")
        plt.xlabel("resolution (A^-1)")
        plt.ylabel("intenstiy")
        plt.show()
    
    if args.q_min_guinier and args.q_max_guinier:
        n_bins =len(q)
        n1 = index(args.q_min_guinier, q[0], q[-1], n_bins)
        n2 = index(args.q_max_guinier, q[0], q[-1], n_bins)
        q2 = q[n1:n2]**2
        lni = np.log(radial_profile[n1:n2])
        m, b, _, _, _ = stats.linregress(q2, lni)
        rg = np.sqrt(-3 * m)
        print(f"approximate radius of gyration: {rg}")

        if args.view:
            plt.plot(q2, lni)
            plt.plot(q2, m * q2 + b)
            plt.title("guinier profile")
            plt.xlabel("resolution squared (A^-2)")
            plt.ylabel("log intenstiy")
            plt.show()

        '''
        q_max = 1.3 / rg
        q_max_i = find_nearest_i(q,q_max)

        print(q_max, q[q_max_i])
        print(q_max_i)
        
        q2 = q[n1:q_max_i]**2
        lni = np.log(radial_profile[n1:q_max_i])
        m, b, _, _, _ = stats.linregress(q2, lni)
        rg = np.sqrt(-3 * m)
        print(rg)

        #plt.plot(q,radial_profile)
        plt.plot(q2,lni)
        plt.plot(q2,m*q2+b)
        plt.show()
        '''
    
