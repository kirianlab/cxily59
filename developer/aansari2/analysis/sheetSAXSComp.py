import h5py
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam

pads = load_pad_geometry_list('../../../geometry/current.json')
beam = load_beam('../../../geometry/beam.json')
# beam.set_wavelength(...)
# print(pads)
print(beam)
#f = h5py.File('q.h5', 'w')
X = pads.q_vecs(beam=beam).T # q coordinates
solidAngles = pads.solid_angles();
polarizationFactors = pads.polarization_factors(beam=beam)
#f.close()


maskdir = "../../../geometry/"
mask = reborn.detector.load_pad_masks([maskdir+"edge_mask.mask", maskdir+"mask_r0174_dead_pixels_maybe.mask", maskdir+"mask_r0174_hot_pixels_maybe.mask", maskdir+"outside_aperture.mask"])
geom = reborn.detector.PADGeometryList(filepath="../../../geometry/current.json")
mask = geom.concat_data(mask) # ravel data consistently
#mask = geom.edge_mask()*mask
mask = mask.astype(bool) # convert int64 to boolean

# load buffer and sample files
runno = 152
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v2/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')

runno = 159
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v2/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_sample = h5py.File(filename, 'r')

dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print('data shape', dataspace)

# precompute radial bin
#X = scipy.io.loadmat('qvs_data.mat')['qvs'].T*1.147526 # correction factor
R = np.linalg.norm(X,axis=0)
print('R',R)
maxr = np.max(R)
num_bins = 1000
rbins = np.linspace(0, maxr, num_bins+1)
binindex = np.digitize(R, rbins[:-1])-1  # Adjust to include the rightmost edge
accum_data_buffer = np.zeros(num_bins)
accum_data_sample = np.zeros(num_bins)

# for accumulating polarization factor * solid angles
accum_sapf = np.zeros(num_bins) 
np.add.at(accum_sapf, binindex[mask], polarizationFactors[mask]*solidAngles[mask])
# accumulate radial pixel count
accum_rad = np.copy(accum_data_buffer)
np.add.at(accum_rad, binindex[mask], 1) 

rad = 0.5 * (rbins[1:] + rbins[:-1])

#frame counter
buffer = 0
sample = 0

brightness_buffer = np.zeros(dataspace[0])
brightness_sample = np.zeros(dataspace[0])
batch_size = 64

# Loop over array elements in batches
for start_idx in range(0, 1280+0*dataspace[0], batch_size):
    batch_elements = np.arange(start_idx, min(dataspace[0], start_idx + batch_size))
    print(batch_elements)
    # get brightness info for filtering 
    data_buffer = file_buffer['/data/calib'][batch_elements,0:4194304]
    data_buffer[data_buffer<2] = 0 # threshold out pixels below 2
    #data_buffer /= (polarizationFactors*solidAngles)
    brightness_buffer[batch_elements] = np.mean(data_buffer, axis=1)
    data_sample = file_sample['/data/calib'][batch_elements,0:4194304]
    data_sample[data_sample<2] = 0 # threshold out pixels below 2
    #data_sample /= (polarizationFactors*solidAngles)
    brightness_sample[batch_elements] = np.mean(data_sample, axis=1)
    
    for i in batch_elements:
        if 0.002 < brightness_buffer[i] < 0.02: # add condition 0.002 0.02
            np.add.at(accum_data_buffer, binindex[mask], data_buffer[i-batch_elements[0],mask])
            #np.add.at(stdev_data_buffer, binindex, data_buffer[i-batch_elements[0],:]**2)
            buffer += 1
        if 0.0015 < brightness_sample[i] < 0.015: # add condition
            np.add.at(accum_data_sample, binindex[mask], data_sample[i-batch_elements[0],mask])
            #np.add.at(stdev_data_sample, binindex, data_sample[i-batch_elements[0],:]**2)
            sample += 1

    print('tally', buffer, sample)
    bufferSAXS = accum_data_buffer/accum_rad/accum_sapf/buffer/7.0 #photons per solid angle
    #bufstdSAXS = (stdev_data_buffer/accum_rad/buffer - bufferSAXS**2)**.5
    sampleSAXS = accum_data_sample/accum_rad/accum_sapf/sample/7.0
    #samstdSAXS = (stdev_data_sample/accum_rad/sample - sampleSAXS**2)**.5
    plt.clf()
    plt.plot(rad, bufferSAXS/1.9, label='Buffer SAXS', alpha=0.5)
    plt.plot(rad, sampleSAXS, label='Sample SAXS', alpha=0.5)
    plt.ylabel('Photons per solid angle')
    plt.xlabel('q (m^{-1}')
    #plt.ylim([5e-10,1e-7])
    plt.yscale('log')  # Set the y-axis to logarithmic scale
    plt.legend()
    plt.title(f'SAXS Plot running average until Frame {i}')

    plt.draw()
    
    plt.pause(0.001)





'''

print('bin_ind',binindex, binindex.shape, np.min(binindex),np.max(binindex))
print('data',data.shape)
# Calculate accumulated data based on binindex


#plt.scatter(X[0,::10],X[1,::10], c=binindex[::10], cmap='viridis', marker='.')
# Add colorbar for radius values
#cbar = plt.colorbar()
#cbar.set_label('Radius (R)')


plt.show()
'''



