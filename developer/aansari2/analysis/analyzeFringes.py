import h5py
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam
import os

pads = load_pad_geometry_list('../../../geometry/current.json') #geometry
beam = load_beam('../../../geometry/beam.json') # beam
# beam.set_wavelength(...)
print(beam)


runno = 65
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')
dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print('data shape [no of frames,no of pixels]:', dataspace)

frameNumber = 2065
data_buffer = file_buffer['/data/calib'][frameNumber,:]

reshapedPixels = np.array(pads.split_data(data_buffer))
i = 20 # panel number

I = reshapedPixels[i,:,:];
fig, axs = plt.subplots(1, 3, figsize=(10, 5))

# Plot I
axs[0].imshow(I, cmap='hot', clim=[0, 50])
axs[0].set_title(f"Pad {i} from Run {runno}, frame {frameNumber}")

# Plot FFT of I
fft_I = np.fft.fft2(I)
fft_I_shifted = np.fft.fftshift(fft_I)
magnitude_spectrum = np.log(np.abs(fft_I_shifted))  # Log scale for better visualization

axs[1].imshow(magnitude_spectrum, cmap='hot')
axs[1].set_title("2D FFT of Pad")


axs[2].plot(np.log(np.abs(fft_I[:,0])))

plt.show()
