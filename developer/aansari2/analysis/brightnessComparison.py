
# Loop over array elements in batches
for start_idx in range(0, 2000+0*dataspace[0], batch_size):
    batch_elements = np.arange(start_idx, min(dataspace[0], start_idx + batch_size))
    print(batch_elements)
    # get brightness info for filtering 
    data_buffer = file_buffer['/data/calib'][batch_elements,0:4194304]
    data_buffer[data_buffer<2] = 0 # threshold out pixels below 2
    #data_buffer /= (polarizationFactors*solidAngles)
    brightness_buffer[batch_elements] = np.sum(data_buffer, axis=1)
    data_sample = file_sample['/data/calib'][batch_elements,0:4194304]
    data_sample[data_sample<2] = 0 # threshold out pixels below 2
    #data_sample /= (polarizationFactors*solidAngles)
    brightness_sample[batch_elements] = np.sum(data_sample, axis=1)
    '''
    for i in batch_elements:
        if 0.002 < brightness_buffer[i] < 0.02: # add condition 0.002 0.02
            np.add.at(accum_data_buffer, binindex[mask], data_buffer[i-batch_elements[0],mask])
            #np.add.at(stdev_data_buffer, binindex, data_buffer[i-batch_elements[0],:]**2)
            buffer += 1
        if 0.0015 < brightness_sample[i] < 0.015: # add condition
            np.add.at(accum_data_sample, binindex[mask], data_sample[i-batch_elements[0],mask])
            #np.add.at(stdev_data_sample, binindex, data_sample[i-batch_elements[0],:]**2)
            sample += 1

    print('tally', buffer, sample)
    bufferSAXS = accum_data_buffer/accum_rad/accum_sapf/buffer/7.0 #photons per solid angle
    #bufstdSAXS = (stdev_data_buffer/accum_rad/buffer - bufferSAXS**2)**.5
    sampleSAXS = accum_data_sample/accum_rad/accum_sapf/sample/7.0
    #samstdSAXS = (stdev_data_sample/accum_rad/sample - sampleSAXS**2)**.5
    plt.clf()
    plt.plot(rad, bufferSAXS/1.9, label='Buffer SAXS', alpha=0.5)
    plt.plot(rad, sampleSAXS, label='Sample SAXS', alpha=0.5)
    plt.ylabel('Photons per solid angle')
    plt.xlabel('q (m^{-1}')
    #plt.ylim([5e-10,1e-7])
    plt.yscale('log')  # Set the y-axis to logarithmic scale
    plt.legend()
    plt.title(f'SAXS Plot running average until Frame {i}')

    plt.draw()
    
    plt.pause(0.001)    
    '''




'''

print('bin_ind',binindex, binindex.shape, np.min(binindex),np.max(binindex))
print('data',data.shape)
# Calculate accumulated data based on binindex


#plt.scatter(X[0,::10],X[1,::10], c=binindex[::10], cmap='viridis', marker='.')
# Add colorbar for radius values
#cbar = plt.colorbar()
#cbar.set_label('Radius (R)')


plt.show()
'''



