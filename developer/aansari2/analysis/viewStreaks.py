import os
import matplotlib.pyplot as plt

# Path to the directory containing the images
directory = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/aansari2/SAXSPatterns/172/'

# Get a list of all PNG files in the directory
image_files = [f for f in os.listdir(directory) if f.endswith('.png')]

# Sort the list of image files to ensure they are displayed in the correct order
image_files.sort()

# Create a figure and axis for displaying the images
fig, ax = plt.subplots()
ax.axis('off')

# Loop through each image file and display it
for image_file in image_files:
    # Load the image
    image_path = os.path.join(directory, image_file)
    image = plt.imread(image_path)
    
    # Display the image
    ax.imshow(image)
    ax.set_title(image_file)  # Set the title to the image file name
    plt.pause(0.001)  # Pause for a short duration to display the image
    plt.draw()  # Force the plot to update

# Close the plot after displaying all images
plt.close(fig)
