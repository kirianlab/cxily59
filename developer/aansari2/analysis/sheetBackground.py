import h5py
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam

pads = load_pad_geometry_list('../../../geometry/current.json') #geometry
beam = load_beam('../../../geometry/beam.json') # beam
# beam.set_wavelength(...)
print(beam)
#f = h5py.File('q.h5', 'w')
X = pads.q_vecs(beam=beam).T # q coordinates
solidAngles = pads.solid_angles();
polarizationFactors = pads.polarization_factors(beam=beam)
#f.close()


maskdir = "../../../geometry/"
mask = reborn.detector.load_pad_masks([maskdir+"edge_mask.mask", maskdir+"mask_r0174_dead_pixels_maybe.mask", maskdir+"mask_r0174_hot_pixels_maybe.mask", maskdir+"outside_aperture_limited.mask"])
maskWater = reborn.detector.load_pad_masks([maskdir+"edge_mask.mask", maskdir+"mask_r0174_dead_pixels_maybe.mask", maskdir+"mask_r0174_hot_pixels_maybe.mask", maskdir+"water_only.mask"])
geom = reborn.detector.PADGeometryList(filepath="../../../geometry/current.json")
mask = geom.concat_data(mask) # ravel data consistently
mask = mask.astype(bool) # convert int64 to boolean
mask = np.where(mask)
mask = np.array(mask)[0,:]

maskWater = geom.concat_data(maskWater) # ravel data consistently
maskWater = maskWater.astype(bool) # convert int64 to boolean
maskWater = np.where(maskWater)
maskWater = np.array(maskWater)[0,:]

# load buffer and sample files
runno = 159
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v2/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')

dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print('data shape', dataspace)

# precompute radial bin
#X = scipy.io.loadmat('qvs_data.mat')['qvs'].T*1.147526 # correction factor
R = np.linalg.norm(X,axis=0)
print('R',R)
maxr = np.max(R)
num_bins = 1000
rbins = np.linspace(0, maxr, num_bins+1)
binindex = np.digitize(R, rbins[:-1])-1  # Adjust to include the rightmost edge

diode = file_buffer['diode'][:]

# for accumulating polarization factor * solid angles
#accum_sapf_helium = np.zeros(num_bins) 
#accum_sapf_water = np.zeros(num_bins) 
#np.add.at(accum_sapf_helium, binindex[mask], polarizationFactors[mask]*solidAngles[mask])
#np.add.at(accum_sapf_water, binindex[maskWater], polarizationFactors[maskWater]*solidAngles[maskWater])
# accumulate radial pixel count
#accum_rad = np.copy(accum_sapf_water)*0
#np.add.at(accum_rad, binindex, 1) 

rad = 0.5 * (rbins[1:] + rbins[:-1])

#frame counter
buffer = 0
sample = 0

brightness_buffer = np.zeros(dataspace[0])
brightness_helium = np.zeros(dataspace[0])
brightness_water = np.zeros(dataspace[0])
batch_size = 128

# Loop over array elements in batches
for start_idx in range(0, dataspace[0], batch_size):
    end_ind = min(dataspace[0],start_idx + batch_size)
    batch_elements = np.arange(start_idx,  end_ind)
    print(batch_elements)
    # get brightness info for filtering 
    data_buffer = file_buffer['/data/calib'][batch_elements,0:4194304]
    data_buffer[data_buffer<2] = 0 # threshold out pixels below 2
    #data_buffer /= (polarizationFactors*solidAngles)
    
    brightness_buffer[batch_elements] = np.mean(data_buffer, axis=1)
    
    brightness_water[batch_elements] = np.mean(data_buffer[:,maskWater], axis=1)
    brightness_helium[batch_elements] = np.mean(data_buffer[:,mask], axis=1)
    
    
    plt.clf()
    diode = file_buffer['diode'][:]
    
    # Plot the data with specified marker and transparency
    plt.plot(diode[0:end_ind], brightness_helium[0:end_ind]/7, '.', label='Helium background', alpha=0.5, markersize=1)
    plt.plot(diode[0:end_ind], brightness_water[0:end_ind]/7, '.', label='Liquid water', alpha=0.5,markersize=1)
    
    # calculate fit with y-intercept = 0
    #np.sum(brightness_water[0:end_ind]*brightness_helium[0:end_ind]/7)/np.sum(brightness_water[0:end_ind]*brightness_water[0:end_ind]/7)
    
    # Calculate and plot the line of best fit for helium background
    helium_coeffs = np.polyfit(diode[0:end_ind], brightness_helium[0:end_ind]/7, 1)
    helium_fit = np.poly1d(helium_coeffs)
    plt.plot(diode[0:end_ind], helium_fit(diode[0:end_ind]), '--', color='blue', label=f'Line of best fit (Helium background): {helium_fit}', alpha=0.7)
    
    # Calculate and plot the line of best fit for liquid water
    water_coeffs = np.polyfit(diode[0:end_ind], brightness_water[0:end_ind]/7, 1)
    water_fit = np.poly1d(water_coeffs)
    plt.plot(diode[0:end_ind], water_fit(diode[0:end_ind]), '--', color='orange', label=f'Line of best fit (Liquid water): {water_fit}',alpha=0.7)
    
    # Add xlabel and ylabel
    plt.xlabel('Diode')
    plt.ylabel('Photon per pixel')
    
    
    # Add grid
    plt.grid(True)
    
    # Add legend
    plt.legend(loc='upper left')
    plt.ylim([0,0.025/7])
    plt.draw()
    plt.pause(0.001)
    #print(brightness_water[batch_elements], brightness_helium[batch_elements])
    

'''

#data_sample = file_sample['/data/calib'][batch_elements,0:4194304]
#data_sample[data_sample<2] = 0 # threshold out pixels below 2
#data_sample /= (polarizationFactors*solidAngles)
#brightness_sample[batch_elements] = np.mean(data_sample, axis=1)

for i in batch_elements:

    accum_data_helium = np.zeros(num_bins)
    accum_data_water = np.zeros(num_bins)
    if True:#0.002 < brightness_buffer[i] < 0.02: # add condition 0.002 0.02
        np.add.at(accum_data_helium, binindex[mask], data_buffer[i-batch_elements[0],mask])
        buffer += 1
        np.add.at(accum_data_water, binindex[maskWater], data_buffer[i-batch_elements[0],maskWater])    
        sample += 1
    heliumSAXS = accum_data_helium/accum_sapf_helium/7.0 #photons per solid angle
    waterSAXS = accum_data_water/accum_sapf_water/7.0
    brightness_helium[i] = np.nanmean(heliumSAXS)
    brightness_water[i] = np.nanmean(waterSAXS)
'''
'''
print('tally', buffer, sample)
#samstdSAXS = (stdev_data_sample/accum_rad/sample - sampleSAXS**2)**.5

plt.plot(rad, heliumSAXS, label='Helium SAXS', alpha=0.5)
plt.plot(rad, waterSAXS, label='Water SAXS', alpha=0.5)
plt.ylabel('Photons per solid angle')
plt.xlabel('q (m^{-1})')
#plt.ylim([5e-10,1e-7])
plt.yscale('log')  # Set the y-axis to logarithmic scale
plt.legend()
plt.title(f'SAXS Plot running average until Frame {i}')

plt.draw()

plt.pause(0.001)

'''



'''
#file_buffer['gas_monitor'][:]
print('bin_ind',binindex, binindex.shape, np.min(binindex),np.max(binindex))
print('data',data.shape)
# Calculate accumulated data based on binindex


#plt.scatter(X[0,::10],X[1,::10], c=binindex[::10], cmap='viridis', marker='.')
# Add colorbar for radius values
#cbar = plt.colorbar()
#cbar.set_label('Radius (R)')


plt.show()
'''



