import numpy as np
import h5py
import matplotlib.pyplot as plt
from scipy.io import loadmat
from scipy.optimize import minimize

# Load the .mat files
data = loadmat('2qsp.mat')
q_mags = data['q_mags']
profile = data['profile']
pixelcount = loadmat('accum_sapf.mat')['accum_sapf']
pixelcount[pixelcount > 4e4] = 0

scale = (75e-6 / 0.57) ** 2
maxr = 6e9
num_bins = 500
rbins = np.linspace(0, maxr, num_bins + 1)
rad = 0.5 * (rbins[1:] + rbins[:-1])

# Specify the file path
file_path_sample = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0158.h5'
with h5py.File(file_path_sample, 'r') as f:
    dataset_sample = np.array(f['/SAXS']).T
    print('Shape of the SAXS dataset:', dataset_sample.shape)

file_path_buffer = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0152.h5'
with h5py.File(file_path_buffer, 'r') as f:
    dataset_buffer = np.array(f['/SAXS']).T
    print('Shape of the SAXS dataset:', dataset_buffer.shape)

# eliminate bright frames
indices_sample = (np.mean(dataset_sample[250:,:], axis=0) < 0.6e5) & (np.mean(dataset_sample, axis=0) < 4e5)
sampleSAXS = np.mean(dataset_sample[:,indices_sample], axis=1) * scale
sampFrameCount = np.sum(indices_sample)

indices_buffer = (np.mean(dataset_buffer[250:,:], axis=0) < 1e5) & (np.mean(dataset_buffer, axis=0) < 5e5)
bufferSAXS = np.mean(dataset_buffer[:,indices_buffer], axis=1) * scale
buffFrameCount = np.sum(indices_buffer)

# error plotting
samplePhotonsPerBin = sampleSAXS * pixelcount * sampFrameCount
bufferPhotonsPerBin = bufferSAXS * pixelcount * buffFrameCount
sampleRelError = np.sqrt(samplePhotonsPerBin) / samplePhotonsPerBin
bufferRelError = np.sqrt(bufferPhotonsPerBin) / bufferPhotonsPerBin
sampleSAXSError = sampleRelError * sampleSAXS
bufferSAXSError = bufferRelError * bufferSAXS

plt.figure(1)
plt.errorbar(rad, sampleSAXS, yerr=sampleSAXSError)
plt.errorbar(rad, bufferSAXS*0.5661, yerr=bufferSAXSError)
plt.gca().set_yscale('log')

# Plotting
plt.figure(2)
plt.semilogy(rad, sampleSAXS - bufferSAXS / 1.64, label='difference')
simSAXS = np.interp(rad, q_mags.ravel(), profile.ravel() / 400 * scale)
plt.semilogy(rad, simSAXS, label='DENSS')
plt.xlabel('q $(m^{-1})$')
plt.ylabel('Photons per solid angle')
plt.title('Average SAXS Pattern of PS1 from sheet')
plt.legend()

# Minimization
conds = (rad > 0.75e9) & (rad < 2e9)
alpha = 1.64
beta = 1
initial_guess = [0.5661,   -0.0001,    0.8713]

def objective(x):
    return np.mean((sampleSAXS[conds] - bufferSAXS[conds] * x[0] + x[1] - x[2] * simSAXS[conds]) ** 2)

lb = [0, 0, 0]  # Lower bounds for alpha and beta
opt_params = minimize(objective, initial_guess)#, bounds=[(0, None), (0, None), (0, None)])
opt_params = opt_params.x
output = sampleSAXS - bufferSAXS * opt_params[0] + opt_params[1]
output[output == output[0]] = np.nan
output_sigma = np.sqrt(sampleSAXSError**2 + (bufferSAXSError * opt_params[0])**2)

plt.figure(3)
plt.errorbar(rad, output, yerr=output_sigma, fmt='.', markersize=3, label='Difference', capsize=0, linewidth=0.5)
plt.semilogy(rad, simSAXS * opt_params[2], label='DENSS', linewidth=1)
plt.gca().set_yscale('log')
plt.legend()
plt.axis([0.5e-9, 1e9 * np.pi, 1e-5, 4e-3])
plt.xlabel('$q = \dfrac{4\pi\sin(\\theta)}{\lambda}$  ($\AA^{-1}$)')

#plt.xlabel('q ($\AA^{-1}$)')
plt.ylabel('photons per pixel')
plt.xticks(plt.gca().get_xticks(), ['{:.2f}'.format(tick * 1e-10) for tick in plt.gca().get_xticks()])
plt.tight_layout()  # Adjust layout to make the plot compact
plt.show()
plt.savefig('2qspSAXS.pdf')
print(opt_params)
