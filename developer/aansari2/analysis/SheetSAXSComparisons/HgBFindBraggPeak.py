import h5py
import numpy as np
import matplotlib.pyplot as plt


# Specify the file path
file_path = "/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0158.h5"

maxr = 6e9 # np.max(R)
num_bins = 500
rbins = np.linspace(0, maxr, num_bins+1) # nodal points of radial bins
rad = 0.5 * (rbins[1:] + rbins[:-1]) # center points of radial bin

# Open the HDF5 file
with h5py.File(file_path, 'r') as f:

    # Load the dataset
    dataset_sample = f['SAXS'][:]
    print("Shape of the SAXS dataset:", dataset_sample.shape)


file_path = "/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0152.h5"    
# Open the HDF5 file
with h5py.File(file_path, 'r') as f:

    # Load the dataset
    dataset_buffer = f['SAXS'][:]
    print("Shape of the SAXS dataset:", dataset_buffer.shape)


file_path = "/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0143-rank000.h5"    
# Open the HDF5 file
with h5py.File(file_path, 'r') as f:

    # Load the dataset
    dataset_gas = f['SAXS'][:]
    print("Shape of the SAXS dataset:", dataset_gas.shape)
    
# Load the arrays from the .npz file about simulation
data = np.load('PS1Sim.npz')
q_mags = data['q_mags']
profile = data['profile']

# Load protien Data set
frameCondition = (np.arange(dataset_sample.shape[0]) > 3500) & (np.arange(dataset_sample.shape[0]) < 7500)
sampleSAXS = np.mean(dataset_sample[frameCondition], axis=0)
bufferSAXS = np.mean(dataset_buffer, axis=0)
gasSAXS = np.mean(dataset_gas, axis=0)


# Plotting
plt.figure(figsize=(10, 6))

# Iterate over chunks and calculate mean
#plt.semilogy(rad, sampleSAXS,label='sample')
plt.semilogy(rad, sampleSAXS-(bufferSAXS/7-1e-3),label='experiment')
#plt.semilogy(rad, gasSAXS,label='gas')
plt.semilogy(q_mags,profile/4000,label='DENSS')
plt.xlabel('q $(m^{-1})$')
plt.ylabel('Photons per solid angle')
plt.title('Average SAXS Pattern of PS1 from sheet')
plt.legend()
plt.show()