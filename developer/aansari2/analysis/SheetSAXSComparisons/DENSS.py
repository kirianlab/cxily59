import numpy as np
import matplotlib.pyplot as plt
from scipy.io import savemat
from reborn.external.denss import get_scattering_profile
q_mags = np.linspace(0, 5/1e-9, 1000)
_, profile = get_scattering_profile(pdb_file='2qsp.pdb', q_mags=q_mags)
plt.semilogy(q_mags/1e10, profile)
plt.xlabel('q [A]')
plt.ylabel('I(q)')
plt.show()

np.savez('2qsp.npz',  q_mags=q_mags, profile=profile)
savemat('2qsp.mat', {'q_mags': q_mags, 'profile': profile})
