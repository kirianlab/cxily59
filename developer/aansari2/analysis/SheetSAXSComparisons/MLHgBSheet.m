% Load the .mat files
data = load('2qsp.mat');
q_mags = data.q_mags;
profile = data.profile;
pixelcount = load('accum_sapf.mat');
pixelcount = pixelcount.accum_sapf;
pixelcount(pixelcount>4e4) = 0;


scale = (75e-6/.57)^2;
maxr = 6e9; % np.max(R)
num_bins = 500;
rbins = linspace(0, maxr, num_bins+1); % nodal points of radial bins
rad = 0.5 * (rbins(2:end) + rbins(1:end-1)); % center points of radial bin

% Specify the file path
file_path = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0158.h5'; % sample
f = h5read(file_path, '/SAXS'); % Open the HDF5 file
dataset_sample = double(f)';
fprintf('Shape of the SAXS dataset: %s\n', mat2str(size(dataset_sample)));
file_path = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0152.h5'; %buffer
f = h5read(file_path, '/SAXS'); % Open the HDF5 file
dataset_buffer = double(f)';
fprintf('Shape of the SAXS dataset: %s\n', mat2str(size(dataset_buffer)));

% eliminate bright frames
indices = mean(dataset_sample(:,251:end), 2) < 0.6e5 & mean(dataset_sample, 2) < 4e5;
sampleSAXS = mean(dataset_sample(indices, :))*scale;
sampFrameCount = sum(indices); % sample frame count
indices = mean(dataset_buffer(:,251:end), 2) < 1e5 & mean(dataset_buffer, 2) < 5e5;
bufferSAXS = mean(dataset_buffer(indices, :))*scale;
buffFrameCount = sum(indices); % buffer frame count
% error plotting
figure(1)
samplePhotonsPerBin = sampleSAXS.*pixelcount*sampFrameCount;
bufferPhotonsPerBin = bufferSAXS.*pixelcount*buffFrameCount;
sampleRelError = sqrt(samplePhotonsPerBin)./samplePhotonsPerBin;
bufferRelError = sqrt(bufferPhotonsPerBin)./bufferPhotonsPerBin;
sampleSAXSError = sampleRelError.*sampleSAXS;
bufferSAXSError = bufferRelError.*bufferSAXS;

errorbar(rad, sampleSAXS, sampleSAXSError)
hold on
errorbar(rad, bufferSAXS, bufferSAXSError)
hold off
set(gca, 'YScale', 'log');


% Plotting
figure(2)
semilogy(rad, sampleSAXS - bufferSAXS / 1.64, 'DisplayName', 'difference');
hold on;
simSAXS = interp1(q_mags,profile/400*scale,rad,'cubic');
semilogy(rad, simSAXS, 'DisplayName', 'DENSS');
hold off;
xlabel('q $(m^{-1})$','interpreter','latex');
ylabel('Photons per solid angle');
title('Average SAXS Pattern of PS1 from sheet');
legend('show');

%%
% Minimization
conds = rad>0.75e9 & rad<2e9;
objective = @(x) mean((sampleSAXS(conds) - bufferSAXS(conds) * x(1) +x(2)- x(3)*simSAXS(conds)).^2);
alpha = 1.64;
beta = 1;
initial_guess = [alpha, 0, beta];
lb = [0,0, 0]; % Lower bounds for alpha and beta

%options = optimset('PlotFcns',{@optimplotfval});

[opt_params, min_error] = fminsearch(objective, initial_guess);

output = sampleSAXS - bufferSAXS * opt_params(1)  + opt_params(2);
output(output==output(1)) = nan;
output_sigma = sampleSAXSError + bufferSAXSError * opt_params(1);

errorbar(rad, output,output_sigma, '.','markersize',6,'DisplayName', 'Difference', 'Capsize',0);
hold on;
semilogy(rad, simSAXS * opt_params(3), 'DisplayName', 'DENSS');
set(gca, 'YScale', 'log');

hold off;
legend('show')
axis([  0.5e-9     1e9*pi   1e-5  4e-3]);
xlabel(['q (' char(197) '^{-1})']);
ylabel('photons per pixel');
ax = gca;
tick_scale_factor = 1e-10;
ax.XTickLabel = ax.XTick * tick_scale_factor;
disp(opt_params)
saveas(gcf,'qspSAXS','m')
%figure(2)
%exportgraphics(gcf, '2qspSAXS.pdf');
%print('2qspSAXS','-djpeg');