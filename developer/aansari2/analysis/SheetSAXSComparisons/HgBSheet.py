import h5py
import numpy as np
import matplotlib.pyplot as plt

# Load the npz file
data = np.load('2qsp.npz')

# Specify the file path
file_path = "/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0158.h5"

maxr = 6e9 # np.max(R)
num_bins = 500
rbins = np.linspace(0, maxr, num_bins+1) # nodal points of radial bins
rad = 0.5 * (rbins[1:] + rbins[:-1]) # center points of radial bin

# Open the HDF5 file
with h5py.File(file_path, 'r') as f:

    # Load the dataset
    dataset_sample = f['SAXS'][:]
    print("Shape of the SAXS dataset:", dataset_sample.shape)


file_path = "/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0152.h5"    
# Open the HDF5 file
with h5py.File(file_path, 'r') as f:

    # Load the dataset
    dataset_buffer = f['SAXS'][:]
    print("Shape of the SAXS dataset:", dataset_buffer.shape)


file_path = "/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/0143-rank000.h5"    
# Open the HDF5 file
with h5py.File(file_path, 'r') as f:

    # Load the dataset
    dataset_gas = f['SAXS'][:]
    print("Shape of the SAXS dataset:", dataset_gas.shape)
    
# Load the arrays from the .npz file about simulation

q_mags = data['q_mags']
profile = data['profile']

#sampleSAXSsampleSAXS
# Load protien Data set
#frameCondition = (np.arange(dataset_sample.shape[0]) > 3500) & (np.arange(dataset_sample.shape[0]) < 7500)

# eliminate brightframes
indices = np.where(np.logical_and(dataset_sample[:,250:-1].mean(axis=1) < .6e5 , dataset_sample.mean(axis=1) < 4e5))
dataset_sample.mean(axis=0) > 4e5
#U, S, V = np.linalg.svd(dataset_sample[indices])

sampleSAXS = np.mean(dataset_sample[indices], axis=0)

indices = np.where(np.logical_and(dataset_buffer[:,250:-1].mean(axis=1) < 1e5 , dataset_buffer.mean(axis=1) < 5e5))
bufferSAXS = np.mean(dataset_buffer[indices], axis=0)
gasSAXS = np.mean(dataset_gas, axis=0)


# Plotting
plt.figure(figsize=(10, 6))

# Iterate over chunks and calculate mean
#plt.semilogy(rad, gasSAXS,label='gas')
plt.semilogy(rad, sampleSAXS,label='sample')
plt.semilogy(rad, bufferSAXS,label='buffer')

plt.semilogy(rad, sampleSAXS-bufferSAXS/1.64,label='difference')

#plt.semilogy(rad, gasSAXS,label='gas')
plt.semilogy(q_mags,profile/400,label='DENSS')
plt.xlabel('q $(m^{-1})$')
plt.ylabel('Photons per solid angle')
plt.title('Average SAXS Pattern of PS1 from sheet')
plt.legend()
plt.show()
