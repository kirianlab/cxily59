# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 01:12:16 2024

@author: aansa
"""
import time
import h5py
import numpy as np
import scipy.io
import matplotlib.pyplot as plt

def preprocess_data(data, threshold=2):
    data[data < threshold] = 0
    return data

def calculate_brightness(data, batch_elements):
    return np.mean(data[batch_elements, 0:4194304], axis=1)


st = time.time()
runno = 152
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v2/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')

runno = 159
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v2/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_sample = h5py.File(filename, 'r')

dataspace = file_buffer['/data/calib'].shape

dataspace = np.array(dataspace)
print('data shape', dataspace)

# precompute radial and theta bin
X = scipy.io.loadmat('qvs_data.mat')['qvs'].T
R = np.linalg.norm(X,axis=0)
T = np.arctan2(X[1,:],X[0,:])
T[T<0] = T[T<0] + 2*np.pi # shift theta: -pi:pi --> 0:2pi

#plt.scatter(X[0,::10],X[1,::10],c=T[::10])
#plt.show()

maxr = np.max(R)
rad_bin_count = 100
rbins = np.linspace(0, maxr, rad_bin_count+1)
rBinIndex = np.digitize(R, rbins[:-1])-1
rad = 0.5 * (rbins[1:] + rbins[:-1])

th_bin_count = 50
tbins = np.linspace(0, 2*np.pi, th_bin_count+1)
tBinIndex = np.digitize(T, tbins[:-1])-1
theta = 0.5 * (tbins[1:] + tbins[:-1])

radius_matrix, theta_matrix = np.meshgrid(rad, theta)


accum_data_buffer = np.zeros((rad_bin_count,th_bin_count))
accum_data_sample = np.zeros((rad_bin_count,th_bin_count))
accum_rad = np.copy(accum_data_buffer)
np.add.at(accum_rad, (rBinIndex,tBinIndex), 1)  # accumulate radial pixel count

#frame counter
buffer = 0
sample = 0

brightness152 = np.zeros(dataspace[0])
brightness158 = np.zeros(dataspace[0])
batch_size = 32

# Loop over array elements in batches
for start_idx in range(0, 32*256, batch_size):
    batch_elements = np.arange(start_idx, min(dataspace[0], start_idx + batch_size))
    print(batch_elements)
    # get brightness info for filtering 
    data_buffer = file_buffer['/data/calib'][batch_elements,0:4194304]
    data_buffer[data_buffer<2] = 0 # threshold out pixels below 2
    brightness152[batch_elements] = np.mean(data_buffer, axis=1)
    data_sample = file_sample['/data/calib'][batch_elements,0:4194304]
    data_sample[data_sample<2] = 0 # threshold out pixels below 2
    brightness158[batch_elements] = np.mean(data_sample, axis=1)
    
    for i in batch_elements:
        if 0.002 < brightness152[i] < 0.02: # add condition 0.002 0.02
            np.add.at(accum_data_buffer, (rBinIndex,tBinIndex), data_buffer[i-batch_elements[0],:])
            buffer += 1
        if 0.0015 < brightness158[i] < 0.015: # add condition
            np.add.at(accum_data_sample, (rBinIndex,tBinIndex), data_sample[i-batch_elements[0],:])
            sample += 1

    bufferSAXS = accum_data_buffer/accum_rad/buffer/7
    sampleSAXS = accum_data_sample/accum_rad/sample/7
    
    print('tally', buffer, sample)
    #plt.contourf(theta_matrix, radius_matrix, np.log10(sampleSAXS).T, cmap='viridis')
    # Calculate the aspect ratio based on the range of theta and radius
    theta_range = theta_matrix.max() - theta_matrix.min()
    radius_range = radius_matrix.max() - radius_matrix.min()
    aspect_ratio = theta_range / radius_range
    
    # Plot the image with imshow
    plt.clf()
    plt.imshow(np.log10(sampleSAXS-bufferSAXS/1.2), cmap='hot', extent=(theta_matrix.min(), theta_matrix.max(), radius_matrix.max(), radius_matrix.min()), aspect=aspect_ratio, origin='lower')
    plt.xlabel('Theta')
    plt.ylabel('Radius')
    plt.title('Sample SAXS (log scale) after {} shots'.format(sample))
    plt.colorbar(label='log(Photon Count per pixel per shot)')
    print(time.time()-st,'seconds elapsed')
    plt.draw()
    plt.pause(0.01)

