import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
import h5py

# load buffer and sample files
runno = 159
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v2/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')
diode = file_buffer['diode'][:]

# Load the .mat file
data = loadmat('159.mat')

# Transpose the arrays
brightness_water = np.transpose(data['brightness_water'])
brightness_helium = np.transpose(data['brightness_helium'])


# Create a figure and axis objects
fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [3, 1]})

# Plot brightness_water and brightness_helium on the first subplot
ax1.plot(brightness_water, '.', label='Brightness Water', markersize=1, alpha=0.5)

# Create a second y-axis and plot brightness_helium on it
ax3 = ax1.twinx()
ax3.plot(brightness_helium, '.', color='orange', label='Brightness Helium', markersize=1, alpha=0.5)
ax3.set_ylabel('Detector Helium background')
####
#ax1.plot(brightness_helium, '.', label='Brightness Helium', markersize=1, alpha=0.5)
ax1.set_ylabel('Bulk water and helium background')
ax3.legend()
ax1.legend()

# Plot diode on the second subplot
ax2.plot(diode, '.', color='red', label='Diode', markersize=1, alpha=0.5)
ax2.set_xlabel('Index')
ax2.set_ylabel('Diode')
ax2.grid(True)
ax2.legend()

plt.suptitle('Run {:04d}'.format(runno))

# Adjust layout
plt.tight_layout()

# Show the plot
plt.show()
