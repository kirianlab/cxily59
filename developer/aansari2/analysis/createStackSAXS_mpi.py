'''
Precompute the SAXS profile for any run for each frame
'''

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# Special print function - only process number 0 (i.e. "rank 0") prints and prefix with rank number.
def print0(*args, **kwargs):
    if rank == 0:
        print(f"Rank {rank:2d}:", *args, **kwargs)

# Special print function - all processes print and prefix with rank number.
def printr(*args, **kwargs):
    print(f"Rank {rank:2d}:", *args, **kwargs)

import argparse

# Create ArgumentParser object
parser = argparse.ArgumentParser(description="Script to process SAXS data")

# Add argument to accept integer value for run number
parser.add_argument("-r", "--runno", type=int, help="Run number")

# Parse the command line arguments
args = parser.parse_args()

# Check if run number is provided
if args.runno is None:
    print0("Error: Please provide a run number using -r option.")
else:
    # Store the run number in a variable
    runno = args.runno

    # Now you can use the runno variable in your script
    print0("Run number:", runno)
    # Add your script logic here

import time
import glob
import h5py
import hdf5plugin
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam
import os
save_directory = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/rkirian/SAXSPatterns/'

pads = load_pad_geometry_list('../../../geometry/current.json') #geometry
beam = load_beam('../../../geometry/beam.json') # beam
# beam.set_wavelength(...)
print0("beam =", beam)
#f = h5py.File('q.h5', 'w')
X = pads.q_vecs(beam=beam).T # q coordinates
solidAngles = pads.solid_angles();
polarizationFactors = pads.polarization_factors(beam=beam)


# Pixel Masking
maskdir = "../../../geometry/"
# Removes hot pixels + dead pixels + edge pixels
mask = reborn.detector.load_pad_masks([maskdir+"edge_mask.mask", maskdir+"mask_r0174_dead_pixels_maybe.mask", maskdir+"mask_r0174_hot_pixels_maybe.mask"])
mask = pads.concat_data(mask) # ravel data consistently
mask = mask.astype(bool) # convert int64 to boolean
mask = np.where(mask)
mask = np.array(mask)[0,:]

# load run number
#runno = 159
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')
dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print0('data shape [no of frames,no of pixels]:', dataspace)


# precompute radial bin
R = np.linalg.norm(X,axis=0)
print0('R',R)
maxr = 6e9 # np.max(R)
num_bins = 500
rbins = np.linspace(0, maxr, num_bins+1) # nodal points of radial bins
binindex = np.digitize(R, rbins[:-1])-1  # Adjust to include the rightmost edge

accum_sapf = np.zeros(num_bins) 
np.add.at(accum_sapf, binindex[mask], polarizationFactors[mask]*solidAngles[mask])
rad = 0.5 * (rbins[1:] + rbins[:-1]) # center points of radial bin

n_frames = dataspace[0]
all_frames = np.arange(n_frames)
# Split the frames amongst processes:
my_frames = comm.scatter(np.array_split(all_frames, size), root=0)
my_n_frames = len(my_frames)
printr(f"Processing {my_n_frames} frames ({my_frames[0]} - {my_frames[-1]})")

#SAXSBufferBatch = h5.dataset((batch_size, num_bins))
SAXSFileName = save_directory + '/{:04d}-rank{:03d}.h5'.format(runno, rank)
printr(f"SAXSFileName = {SAXSFileName}")
if rank == 0:
    os.makedirs(os.path.dirname(SAXSFileName), exist_ok=True)
comm.barrier()  # Rank 0 makes the above directory.  Stop other processes from continuing until that is done.
h5File = h5py.File(SAXSFileName,'w')
SAXSDataSet = h5File.create_dataset('SAXS',shape=(my_n_frames, num_bins), dtype=np.float64) # partitioned Data set



batch_size = 8
counter = 0
# Loop over array elements in batches
t0 = time.time()
for start_idx in range(0, my_n_frames, batch_size):
    end_ind = min(my_n_frames-1,start_idx + batch_size) # because dataset is not a multiple of batch size
    batch_elements = np.arange(my_frames[start_idx],  my_frames[end_ind])
    dt = time.time() - t0
    pct = start_idx / my_n_frames * 100
    hz = start_idx/dt*size
    if hz == 0:
        ml = -99
    else:
        ml = size*(my_n_frames - start_idx)/hz/60
    printr(f"Batch {batch_elements[0]:5d} - {batch_elements[-1]:5d} ({pct:5.1f}% done; {hz:.1f} Hz; {ml:5.2f} minutes left)")
    # print(batch_elements)
    # get brightness info for SAXS 
    data_buffer = file_buffer['/data/calib'][batch_elements,0:dataspace[1]]
    #data_buffer[data_buffer<2] = 0 # threshold out pixels below 2
    
    

    for i in batch_elements:
        accum_data_buffer = np.zeros(num_bins)
        np.add.at(accum_data_buffer, binindex[mask], data_buffer[i-batch_elements[0],mask])
        bufferSAXS = np.divide(accum_data_buffer, accum_sapf*7.0, np.zeros_like(accum_data_buffer), where=accum_sapf>0) # photons per solid angle for this shot
        try:
            SAXSDataSet[counter] = bufferSAXS
            counter += 1
        except Exception as e:  # 'catch' should be 'except', and it should catch a specific exception or 'Exception' as a general catch-all
            print("An error occurred while saving:", e)
            break
h5File.close()
# Wait for all processes
comm.barrier()
if rank == 0:
    SAXSFileName = save_directory + '/{:04d}.h5'.format(runno)
    os.makedirs(os.path.dirname(SAXSFileName), exist_ok=True)
    h5File = h5py.File(SAXSFileName,'w')
    SAXSDataSet = h5File.create_dataset('SAXS',shape=(n_frames, num_bins), dtype=np.float64)
    n = 0
    for f in sorted(glob.glob(f"{save_directory}/*-rank*.h5")):
        printr(f"Aggregating file {os.path.basename(f)} into {os.path.basename(SAXSFileName)}")
        h5f = h5py.File(f,'r')
        h5b = h5f["SAXS"][:]
        m = h5b.shape[0]
        printr(f"Inserting frames {n} through {n+m}")
        SAXSDataSet[n:n+m, :] = h5b
        h5f.close()
        printr(f"Deleting file {os.path.basename(f)}")
        os.remove(f)
        n += m
    h5File.close()
    h5File = h5py.File(SAXSFileName,'r')
    # import pyqtgraph as pg
    # pg.image(h5File["SAXS"][0:1000, :])
    # pg.mkQApp().exec_()

printr("Done")
            

    # if plot needed
    #plt.clf()
    #plt.semilogy(rad, bufferSAXS)
    # Add xlabel and ylabel
    #plt.xlabel('q (m^{-1})')
    #plt.ylabel('Average photons per solid angle')
    #plt.grid(True)
    #plt.draw()
    #plt.pause(0.001)





