'''
Precompute the SAXS profile for any run for each frame
'''

import argparse

# Create ArgumentParser object
parser = argparse.ArgumentParser(description="Script to process SAXS data")

# Add argument to accept integer value for run number
parser.add_argument("-r", "--runno", type=int, help="Run number")

# Parse the command line arguments
args = parser.parse_args()

# Check if run number is provided
if args.runno is None:
    print("Error: Please provide a run number using -r option.")
else:
    # Store the run number in a variable
    runno = args.runno

    # Now you can use the runno variable in your script
    print("Run number:", runno)
    # Add your script logic here

import h5py
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam
import os

save_directory = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/aansari2/SAXSPatterns/'

pads = load_pad_geometry_list('../../../geometry/current.json') #geometry
beam = load_beam('../../../geometry/beam.json') # beam
# beam.set_wavelength(...)
print(beam)
#f = h5py.File('q.h5', 'w')
X = pads.q_vecs(beam=beam).T # q coordinates
solidAngles = pads.solid_angles();
polarizationFactors = pads.polarization_factors(beam=beam)


# Pixel Masking
maskdir = "../../../geometry/"
# Removes hot pixels + dead pixels + edge pixels
mask = reborn.detector.load_pad_masks([maskdir+"edge_mask.mask", maskdir+"mask_r0174_dead_pixels_maybe.mask", maskdir+"mask_r0174_hot_pixels_maybe.mask"])
mask = pads.concat_data(mask) # ravel data consistently
mask = mask.astype(bool) # convert int64 to boolean
mask = np.where(mask)
mask = np.array(mask)[0,:]

# load run number
#runno = 159
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')
dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print('data shape [no of frames,no of pixels]:', dataspace)


# precompute radial bin
R = np.linalg.norm(X,axis=0)
print('R',R)
maxr = 6e9 # np.max(R)
num_bins = 500
rbins = np.linspace(0, maxr, num_bins+1) # nodal points of radial bins
binindex = np.digitize(R, rbins[:-1])-1  # Adjust to include the rightmost edge

accum_sapf = np.zeros(num_bins) 
np.add.at(accum_sapf, binindex[mask], polarizationFactors[mask]*solidAngles[mask])
rad = 0.5 * (rbins[1:] + rbins[:-1]) # center points of radial bin

#SAXSBufferBatch = h5.dataset((batch_size, num_bins))
SAXSFileName = save_directory + '/{:04d}.h5'.format(runno)
os.makedirs(os.path.dirname(SAXSFileName), exist_ok=True)
h5File = h5py.File(SAXSFileName,'w')
SAXSDataSet = h5File.create_dataset('SAXS',shape=(dataspace[0], num_bins), dtype=np.float64)

batch_size = 128
# Loop over array elements in batches
for start_idx in range(0, dataspace[0], batch_size):
    end_ind = min(dataspace[0],start_idx + batch_size) # because dataset is not a multiple of batch size
    batch_elements = np.arange(start_idx,  end_ind)
    print(batch_elements)
    # get brightness info for SAXS 
    data_buffer = file_buffer['/data/calib'][batch_elements,0:dataspace[1]]
    #data_buffer[data_buffer<2] = 0 # threshold out pixels below 2
    
    

    for i in batch_elements:
        accum_data_buffer = np.zeros(num_bins)
        np.add.at(accum_data_buffer, binindex[mask], data_buffer[i-batch_elements[0],mask])
        bufferSAXS = accum_data_buffer/accum_sapf/7.0 # photons per solid angle for this shot
        try:
            SAXSDataSet[start_idx + i] = bufferSAXS
        except Exception as e:  # 'catch' should be 'except', and it should catch a specific exception or 'Exception' as a general catch-all
            print("An error occurred while saving:", e)
            break
        
    

            

    # if plot needed
    #plt.clf()
    #plt.semilogy(rad, bufferSAXS)
    # Add xlabel and ylabel
    #plt.xlabel('q (m^{-1})')
    #plt.ylabel('Average photons per solid angle')
    #plt.grid(True)
    #plt.draw()
    #plt.pause(0.001)

h5File.close()



