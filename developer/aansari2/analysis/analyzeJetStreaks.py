'''
Precompute the SAXS profile for any run for each frame
'''

import argparse

# Create ArgumentParser object
parser = argparse.ArgumentParser(description="Script to process SAXS data")

# Add argument to accept integer value for run number
parser.add_argument("-r", "--runno", type=int, help="Run number",default=167)
# Parse the command line arguments
args = parser.parse_args()
# Store the run number in a variable
runno = args.runno
# Now you can use the runno variable in your script
print("Run number:", runno)
# Add your script logic here

import h5py
import hdf5plugin
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam
import os
from scipy.sparse import coo_matrix
from scipy.signal import find_peaks
from scipy.sparse.linalg import inv
from scipy.sparse import diags
import time
import matplotlib.animation as animation

def streakMask(X,thetaa,d):
    theta = thetaa[0]
    return np.abs(np.cos(theta)*X[1,:] - np.sin(theta)*X[0,:]) > d




save_directory = '/sdf/scratch/lcls/ds/cxi/cxily5921/scratch/data/aansari2/SAXSPatterns/'

if not os.path.exists(save_directory+f'{runno}/'):
    os.makedirs(save_directory+f'{runno}/')


pads = load_pad_geometry_list('../../../geometry/current.json') #geometry
beam = load_beam('../../../geometry/beam.json') # beam
# beam.set_wavelength(...)
print(beam)
#f = h5py.File('q.h5', 'w')
X = pads.q_vecs(beam=beam).T # q coordinates
solidAngles = pads.solid_angles();
polarizationFactors = pads.polarization_factors(beam=beam)

# Pixel Masking
maskdir = "../../../geometry/"
# Removes hot pixels + dead pixels + edge pixels
mask = reborn.detector.load_pad_masks([maskdir+"edge_mask.mask", maskdir+"mask_r0174_dead_pixels_maybe.mask", maskdir+"mask_r0174_hot_pixels_maybe.mask"])
mask = pads.concat_data(mask) # ravel data consistently

numr_bins = 500
numt_bins = 360
# load run number
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')
dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print('data shape [no of frames,no of pixels]:', dataspace)

SAXSFileName = save_directory + '/{:04d}.h5'.format(runno)
os.makedirs(os.path.dirname(SAXSFileName), exist_ok=True)
h5File = h5py.File(SAXSFileName,'w')
SAXSDataSet = h5File.create_dataset('SAXS',shape=(dataspace[0], numr_bins), dtype=np.float64)

# precompute theta bin
theta = np.arctan2(X[1,:], X[0,:])
theta[theta<0] = theta[theta<0] + 2*np.pi
print('theta',theta)


tbins = np.linspace(0, 2*np.pi, numt_bins+1) # nodal points of theta  bins
binindex = np.digitize(theta, tbins[:-1])-1  # Adjust to include the rightmost edge
tcent = 0.5 * (tbins[1:] + tbins[:-1])
accum_data_buffer = np.zeros(numt_bins)

A = coo_matrix((np.ones_like(binindex), (binindex, np.arange(dataspace[1]))), shape=(numt_bins, dataspace[1])) # create sparse matrix
#Asum = coo_matrix((np.array(A.sum(axis=1)).ravel(),(np.arange(numt_bins),np.arange(numt_bins))),shape=(numt_bins,numt_bins))
Asum = diags(np.array(A.sum(axis=1)).ravel(),0)
A = inv(Asum).dot(A)

# precompute radial bin
R = np.linalg.norm(X,axis=0)
maxr = np.max(R)
rbins = np.linspace(0, maxr, numr_bins+1)
binindex = np.digitize(R, rbins[:-1])-1  # Adjust to include the rightmost edge
B = coo_matrix((np.ones_like(binindex), (binindex, np.arange(dataspace[1]))), shape=(numr_bins, dataspace[1])) # create sparse matrix
#Bsum = coo_matrix((1/np.array(B.sum(axis=1)).ravel(),(np.arange(numr_bins),np.arange(numr_bins))),shape=(numr_bins,numr_bins))
#B = Bsum.dot(B)
#SAXSStack = np.zeros(
batch_size = 128
# Loop over array elements in batches
for start_idx in range(0, dataspace[0], batch_size):
    t0 = time.time()
    end_ind = min(dataspace[0],start_idx + batch_size) # because dataset is not a multiple of batch size
    batch_elements = np.arange(start_idx, end_ind)
    print(batch_elements)
    # get brightness info for SAXS 
    data_buffer = file_buffer['/data/calib'][batch_elements,0:dataspace[1]]
    tcounts = A.dot(data_buffer.T)/7
    
    tcountsAverage = np.mean(tcounts,axis=0)
    
    frameCounter = 0
    for i in batch_elements:
        if tcountsAverage[i-start_idx] > 0.025:#0.005
            corr = np.correlate(tcounts[:,i-start_idx],np.cos(tcent)**1000,'same')
            peaks, _ = find_peaks(corr, distance=numt_bins/2, height=0.4)#0.15
            
            
            # create a mask
            if len(peaks) > 0:
                frameCounter += 1
                streakmask = streakMask(X,tcent[peaks],.25e9) * mask
                MaskMatrix = diags(streakmask, 0) #sparse
                MaskedB = B.dot(MaskMatrix) # sparse
                MaskedBSum = diags(1/np.array(MaskedB.sum(axis=1)).reshape(numr_bins),0) # sparse
                
                data = data_buffer[i-start_idx,:]
                SAXS = MaskedBSum.dot(B).dot(data)
                #print(frameCounter,time.time()-t0)
                
                plt.clf()
                #plt.plot(tcent, corr)
                plt.scatter(X[0,::100], X[1,::100], c=data[::100]+3*streakmask[::100], marker='.', label='Buffer SAXS', alpha=0.5)
                #plt.semilogy(SAXS)
                plt.clim([0,20])
                plt.title(f'Plot of Mask at Frame {i}')
                plt.draw()
                plt.pause(0.001)
                plt.savefig(save_directory + f'{runno}/frame_{i}.png')  # Save the frame as a PNG image

                try:
                    SAXSDataSet[start_idx + i] = SAXS
                except Exception as e:  # 'catch' should be 'except', and it should catch a specific exception or 'Exception' as a general catch-all
                    print("An error occurred while saving:", e)
h5File.close()
