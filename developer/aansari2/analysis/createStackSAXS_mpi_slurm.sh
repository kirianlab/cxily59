#!/bin/bash
#SBATCH --partition=milano
#SBATCH --account=lcls:cxily5921
#SBATCH --job-name=h5test
##SBATCH --output=slurm/logs/%j.output
##SBATCH --error=slurm/logs/%j.err
#SBATCH --ntasks=48
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2g
#SBATCH --time=0-5:00:00
#[[ -d slurm/logs ]] || mkdir -p slurm/logs
p=$(pwd)
cd ../../..
source setup.sh
cd $p
mpirun python createStackSAXS_mpi.py -r 159
