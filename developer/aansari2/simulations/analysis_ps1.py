import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from reborn.external.denss import get_scattering_profile

q_mags = np.linspace(0, np.pi/1e-9, 1000)
_, profile = get_scattering_profile(pdb_file='1jb0.pdb', q_mags=q_mags)

plt.semilogy(q_mags/1e10, profile)
plt.xlabel('q [A^-1]')
plt.ylabel('I(q)')
plt.show()


'''

# Load data from MATLAB files
data = scipy.io.loadmat("SAXSdata152159.mat")
sim_data = scipy.io.loadmat("sim.mat")

# Extract relevant variables
rad = data["rad"]
bufferSAXS = data["bufferSAXS"].T
sampleSAXS = data["sampleSAXS"].T
N = 20
sf = 1.2

# Reshape arrays
rad2 = np.reshape(rad, (2000//N, N))
bufferSAXS2 = np.reshape(bufferSAXS, (2000//N, N)) / 7
sampleSAXS2 = np.reshape(sampleSAXS, (2000//N, N)) / 7

# Calculate means and standard deviations USING DATA NOISE
rad2 = np.mean(rad2, axis=1)
bufferMean = np.mean(bufferSAXS2, axis=1)
bufferSTD = np.std(bufferSAXS2, axis=1) / np.sqrt(20)
sampleMean = np.mean(sampleSAXS2, axis=1)
sampleSTD = np.std(sampleSAXS2, axis=1) / np.sqrt(20)
print("bshape",rad2.shape)
print("sshape",bufferMean.shape)

# Calculate errorbar values
yneg = sampleSTD + bufferSTD / sf / 2
ypos = sampleSTD + bufferSTD / sf / 2
xneg = 0.001658 / 2 * rad2
xpos = 0.001658 / 2 * rad2

# Plot errorbar

simulationAdjusted = profile*.82e-10;
plt.plot(q_mags, simulationAdjusted,label='Simulation (DENSS)')
#plt.plot(rad2 * 1.147526, sampleMean - bufferMean / sf,'.',label='Sample - Buffer')
#plt.fill_between(rad2 * 1.147526, sampleMean - bufferMean / sf - yneg, sampleMean - bufferMean / sf + ypos, alpha=0.2)
plt.errorbar(rad2 * 1.147526, sampleMean - bufferMean / sf, yerr=[yneg, ypos], xerr=[xneg, xpos], marker='.',label='Sample - Buffer')
# Fill the area between the upper and lower error bounds
plt.yscale('log')  # Set the y-scale to logarithmic
print(2*np.pi*np.trapz(q_mags,simulationAdjusted*q_mags))
# Add labels and legend
plt.xlabel('q (m^-1)')
plt.ylabel('Intensity (Photons/pixel)')
plt.xlim([0,3e9]);
plt.legend()
plt.show()
'''