import h5py
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam
import os
import hdf5plugin 
pads = load_pad_geometry_list('../../geometry/current.json') #geometry
beam = load_beam('../../geometry/beam.json') # beam
# beam.set_wavelength(...)
print(beam)

runno = 65
#the following file was produced by a script in cxily59 git repo
filename = '/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat/{:04d}/{:04d}.h5'.format(runno, runno)
file_buffer = h5py.File(filename, 'r')
print(file_buffer.keys())
dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print('data shape [no of frames,no of pixels]:', dataspace)
n_frames, n_pixels=dataspace
#we need to generalize this to automatically identify frames with jet streaks
frameNumber = 2067
diodes=file_buffer['/diode'][:]
background_diodes=diodes[frameNumber]
data_buffer_background = file_buffer['/data/calib'][frameNumber,:]/background_diodes #diffraction pattern as a numpy array

#print(data_buffer.shape) 
file_buffer = h5py.File(filename, 'r')
dataspace = file_buffer['/data/calib'].shape
dataspace = np.array(dataspace)
print('data shape [no of frames,no of pixels]:', dataspace)
#we need to generalize this to automatically identify frames with jet streaks
frameNumber = 2066
data_buffer = file_buffer['/data/calib'][frameNumber,:]/diodes[frameNumber] #diffraction pattern as a numpy array
data_buffer-=data_buffer_background


reshapedPixels = np.array(pads.split_data(data_buffer))
print(reshapedPixels.shape)
#sys.exit()
i = 20 # panel number,identify the other panel that also has a jet streak

I = reshapedPixels[i,:,:];
fig, axs = plt.subplots(1, 3, figsize=(10, 5))

# Plot I
axs[0].imshow(I, cmap='hot', clim=[0, np.percentile(I,99)])
axs[0].set_title(f"Pad {i} from Run {runno}, frame {frameNumber}")

# Plot FFT of I
fft_I = np.fft.fft2(I)
fft_I_shifted = np.fft.fftshift(fft_I)
fft_I_shifted[100:156,:]=0
magnitude_spectrum = np.log(np.abs(fft_I_shifted))  # Log scale for better visualization
px, py= np.array(np.unravel_index(fft_I_shifted.argmax(), fft_I_shifted.shape))
print(px,py)

axs[1].imshow(magnitude_spectrum, cmap='hot')
axs[1].set_title("2D FFT of Pad")
axs[1].plot(py,px,"o")
axs[1].plot(128,128,'+',label="origin")

#axs[2].plot(np.log(np.abs(fft_I[:,0])))

plt.show()
