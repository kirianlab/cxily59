#!/bin/bash
if [ "$(pwd)" != "/home/rkirian/work/projects/fxs/cxily59/developer/rkirian" ] ; then
echo "Run the script from the correct directory"
exit
fi
cd ../..
pwd
extra=''
extra='--delete'
#extra='--dry-run --delete'
rsync -arv --stats --progress $extra --exclude 'cache' --exclude 'developer/rkirian/slurm' --exclude 'doc' --exclude '*.out' --exclude '*.err' --exclude 'slurm/logs' --exclude 'results' --exclude 'tests' --exclude '*.pkl' --exclude joblib --exclude '*.h5' --exclude '*.so' --exclude '.git' --exclude '*.pyc' --exclude '__pycache__' --exclude '.idea' --exclude '*.md5' . rkirian@s3dfdtn.slac.stanford.edu:/sdf/data/lcls/ds/cxi/cxily5921/scratch/"$USER"/cxily59
