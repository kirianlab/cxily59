# To run in parallel, use
# > mpirun -n <# processes> h5_test.py
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../..")
import glob
import time
import argparse
import h5py
import hdf5plugin
import numpy as np
import matplotlib.pyplot as plt
import pyqtgraph as pg
from mpi4py import MPI
from reborn import source, detector
from reborn.analysis import masking
from reborn.viewers.qtviews import view_pad_data
from reborn.misc import polar
from reborn.analysis import saxs
from reborn.external.pyqtgraph import imview
from config import get_config

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--run_number", type=int, default=-1)
parser.add_argument("--plots", action="store_true", default=False)
parser.add_argument("--process_diffraction", action="store_true", default=False)
args = parser.parse_args()

# ======================================================
# CONFIGURATION
# ======================================================
base_directory = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/"
run_number = args.run_number
config = get_config(run_number)
geom = config["pad_detectors"][0]["geometry"]
beam = config["beam"]
mask_files = config["pad_detectors"][0]["mask"]
water_mask_files = mask_files
polar_mask_files = mask_files
helium_mask_files = None
debug = True
max_frames = 1e8
show_plots = False
show_plots_1 = True
diode_thresh = [20000, 50000]
photon_thresh = [7040, 7090]
process_diffraction = args.process_diffraction
polar_params = dict(n_q_bins=500, q_min=0, q_max=0.75e10, n_p_bins=360)
saxs_params = dict(n_q_bins=500, q_min=0, q_max=0.75e10)

if args.plots:
    show_plots = True


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


def printr(*args, **kwargs):
    if rank == 0:
        print(f"Run {run_number:4d}| Rank {rank:2d}|", *args, **kwargs)


def print0(*args, **kwargs):
    if rank == 0:
        print(f"Run {run_number:4d}| Rank  0|", *args, **kwargs)


# geom = detector.load_pad_geometry_list(geom_file)
# beam = source.load_beam(beam_file)
water_mask = detector.load_pad_masks(water_mask_files)
water_mask = geom.concat_data(water_mask)
polar_mask = detector.load_pad_masks(polar_mask_files)
polar_mask = geom.concat_data(polar_mask)
filepath = f"{base_directory}/h5dat/{run_number:04d}/{run_number:04d}.h5"
h5file = h5py.File(filepath, "r")
n_frames = h5file["/calib"].shape[0]
diode = h5file["/diode"][:]
photon = h5file["/photon_energy"][:]
pressure = h5file["/pressure"][:]
frame_numbers = np.arange(n_frames)
# =======================================================
# INSPECT BASIC DATA CHARACTERISTICS (DIODE, PHOTON ENERTY, ETC)
# =======================================================
if rank == 0 and show_plots and show_plots_1:
    fig, (
        (diode_scatter, diode_hist),
        (photon_scatter, photon_hist),
        (pressure_scatter, pressure_hist),
    ) = plt.subplots(3, 2, figsize=(20, 20), gridspec_kw={"width_ratios": [3, 1]})
    diode_scatter.scatter(frame_numbers, diode, color="blue", alpha=0.1, s=5)
    diode_hist.hist(
        diode, bins=100, color="blue", edgecolor="none", orientation="horizontal"
    )
    diode_hist.set_xlabel("Frequency")
    # diode_hist.set_yticks([])
    diode_scatter.set_xlabel("Frame number")
    diode_scatter.set_ylabel("Diode signal")
    diode_scatter.set_title(f"Diode signals for run {run_number}")
    if diode_thresh[0] is not None:
        diode_hist.axhline(diode_thresh[0], color="black", linestyle="--")
    if diode_thresh[1] is not None:
        diode_hist.axhline(diode_thresh[1], color="black", linestyle="--")
    photon_scatter.scatter(frame_numbers, photon, color="orange", alpha=0.1, s=5)
    photon_hist.hist(
        photon, bins=100, range=(np.min(photon[photon > 0]), np.max(photon)), color="orange", edgecolor="none", orientation="horizontal"
    )
    photon_hist.set_xlabel("Frequency")
    # photon_hist.set_yticks([])
    photon_scatter.set_xlabel("Frame number")
    photon_scatter.set_ylabel("photon energy")
    photon_scatter.set_title(f"Photon energies for run {run_number}")
    if photon_thresh[0] is not None:
        photon_hist.axhline(photon_thresh[0], color="black", linestyle="--")
    if photon_thresh[1] is not None:
        photon_hist.axhline(photon_thresh[1], color="black", linestyle="--")
    pressure_scatter.scatter(frame_numbers, pressure, color="green", alpha=0.1, s=5)
    pressure_hist.hist(
        pressure, bins=100, color="green", edgecolor="none", orientation="horizontal"
    )
    pressure_hist.set_xlabel("Frequency")
    # pressure_hist.set_yticks([])
    pressure_scatter.set_xlabel("Frame number")
    pressure_scatter.set_ylabel("Pressure")
    pressure_scatter.set_title(f"Pressures for run {run_number}")
    plt.tight_layout()
    plt.show()
# =======================================================
# PROCESS DIFFRACTION PATTERNS
# =======================================================
if not process_diffraction:
    print0("Will not process diffraction.")
    sys.exit()
frame_mask = np.ones(n_frames)
if diode_thresh[0] is not None:
    frame_mask[diode < diode_thresh[0]] = 0
if diode_thresh[1] is not None:
    frame_mask[diode > diode_thresh[1]] = 0
if photon_thresh[0] is not None:
    frame_mask[photon < photon_thresh[0]] = 0
if photon_thresh[1] is not None:
    frame_mask[photon > photon_thresh[1]] = 0
n_frames = min(max_frames, n_frames)
frame_numbers = frame_numbers[0:n_frames]
my_frames = comm.scatter(np.array_split(frame_numbers, size), root=0)
my_n_frames = len(my_frames)
# ==========================================================
# Jet streak masks
# =====================================================
print0("Getting streak masks...")
streak_mask_directory = base_directory + f"/streakmasks/r{run_number:04d}/"
streak_mask_file = streak_mask_directory + "/streakmasks.h5"
streak_dataset_name = "/masks"
if not os.path.exists(streak_mask_file):
    streak_mask_chunk_directory = streak_mask_directory + "/chunks/"
    streak_mask_chunk_file = streak_mask_chunk_directory + f"{rank:02d}.h5"
    printr(f"Will make stream masks for frames {my_frames[0]} through {my_frames[-1]}")
    if rank == 0:
        os.makedirs(streak_mask_chunk_directory, exist_ok=True)
    comm.barrier()  # No process moves forward until the above directory is made
    h5_chunk_file = h5py.File(streak_mask_chunk_file, "w")
    h5_chunk_dataset = h5_chunk_file.create_dataset(
        streak_dataset_name,
        dtype=int,
        shape=(my_n_frames, geom.n_pixels),
        **hdf5plugin.Bitshuffle(),
    )
    streaker = masking.StreakMasker(
        geom=geom,
        beam=beam,
        snr=5,
        streak_width=0.25 * np.pi / 180,
        q_range=[0.08e10, 0.3e10],
    )
    t0 = time.time()
    for n, i in enumerate(my_frames):
        msg = f"Streak mask frame {i:5d}"
        p = h5file["/calib"][i, :]
        mask = streaker.get_mask(pattern=p, mask=water_mask)
        mask = geom.concat_data(mask)
        h5_chunk_dataset[n, :] = mask
        msg += (
            f" ({int(np.sum(p / 7)):7d} photons, {int(np.sum(mask)):7d} masked pixels)"
        )
        tr = ((time.time() - t0) / (n + 1)) * (my_n_frames - n + 1)
        msg += f" {tr/60:5.1f} minutes remaining..."
        printr(msg)
    h5_chunk_file.close()
    comm.barrier()
    if rank == 0:
        g = sorted(glob.glob(f"{streak_mask_chunk_directory}/*.h5"))
        h5files = [h5py.File(f, "r") for f in g]
        h5out = h5py.File(streak_mask_file, "w")
        shape = list(h5files[0][streak_dataset_name].shape)
        shape[0] = n_frames
        shape = tuple(shape)
        layout = h5py.VirtualLayout(
            shape=shape, dtype=h5files[0][streak_dataset_name].dtype
        )
        idx = 0
        for dataset in [f[streak_dataset_name] for f in h5files]:
            n = dataset.shape[0]
            vl_slice = [slice(None)] * len(shape)
            vl_slice[0] = slice(idx, idx + n)
            vl_slice = tuple(vl_slice)
            vsource = h5py.VirtualSource(dataset)
            layout[vl_slice] = vsource
            idx += n
        h5out.create_virtual_dataset(streak_dataset_name, layout)
        h5out.close()
        printr(f"Wrote streakmasks file {streak_mask_file}")
    comm.barrier()
streak_h5 = h5py.File(streak_mask_file)
streak_dataset = streak_h5[streak_dataset_name]
# if rank == 0:
#     h5m = h5py.File(streak_mask_file)
#     dsm = h5m[streak_dataset_name]
#     dsp = h5file["/calib"]
#     for i in range(n_frames):
#         mask = dsm[i, :]
#         pat = dsp[i, :]
#         view_pad_data(data=pat, mask=mask, pad_geometry=geom, beam=beam)
# sys.exit()
# =======================================================
# Generate SAXS patterns for each shot and save to hdf5
# =======================================================
print0(f"Getting SAXS profiles for run {run_number}")
nq = saxs_params["n_q_bins"]
q_range = (saxs_params["q_min"], saxs_params["q_max"])
print0(
    f"{nq} q bins in range from {q_range[0]/1e10:0.3f}/A to {q_range[1]/1e10:0.3f}/A"
)
saxstats_directory = base_directory + f"/saxstats/r{run_number:04d}/"
saxstats_file = saxstats_directory + "/saxstats.h5"
print0(f"Looking for file {saxstats_file}")
if not os.path.exists(saxstats_file):
    saxstats_chunk_directory = saxstats_directory + "/chunks/"
    saxstats_chunk_file = saxstats_chunk_directory + f"{rank:02d}.h5"
    printr(f"Will process frames {my_frames[0]} through {my_frames[-1]}")
    if rank == 0:
        os.makedirs(saxstats_chunk_directory, exist_ok=True)
    comm.barrier()  # No process moves forward until the above directory is made
    saxstats_chunk_h5 = h5py.File(saxstats_chunk_file, "w")
    saxstats_sum = saxstats_chunk_h5.create_dataset(
        "/sums",
        dtype=np.float32,
        shape=(my_n_frames, nq),
        **hdf5plugin.Bitshuffle(),
    )
    saxstats_wsum = saxstats_chunk_h5.create_dataset(
        "/weight_sums",
        dtype=np.float32,
        shape=(my_n_frames, nq),
        **hdf5plugin.Bitshuffle(),
    )
    # Don't include polarization in the weights yet... unsure of the direction of the E field.
    weights = geom.solid_angles()  # * geom.polarization_factors(beam)
    weights = weights.astype(np.float64)
    saxs_profiler = saxs.RadialProfiler(
        pad_geometry=geom,
        beam=beam,
        n_bins=nq,
        q_range=q_range,
    )
    t0 = time.time()
    for n, i in enumerate(my_frames):
        msg = f"SAXS frame {i:6d}"
        pattern = h5file["/calib"][i, :]
        streak_mask = streak_dataset[i, :]
        mask = (streak_mask * polar_mask).astype(np.float64)
        pattern *= mask
        pattern /= (
            weights  # The quickstats method is a weighted sum, so we first "unweight"
        )
        pattern = pattern.astype(np.float64)
        mask *= weights
        mask = mask.astype(np.float64)
        stats = saxs_profiler.quickstats(pattern, weights)
        pattern_sum = stats["sum"]
        weight_sum = stats["weight_sum"]
        saxstats_sum[n, :] = pattern_sum
        saxstats_wsum[n, :] = weight_sum
        tr = ((time.time() - t0) / (n + 1)) * (my_n_frames - n + 1)
        msg += f" {tr/60:5.1f} minutes remaining..."
        printr(msg)
    saxstats_chunk_h5.close()  # Close the chunk
    comm.barrier()  # Wait for all processes to finish before virtual dataset is made
    if rank == 0:
        g = sorted(glob.glob(f"{saxstats_chunk_directory}/*.h5"))
        h5files = [h5py.File(f, "r") for f in g]
        h5out = h5py.File(saxstats_file, "w")
        shape = (n_frames, nq)
        dataset_names = ["/sums", "/weight_sums"]
        for dataset_name in dataset_names:
            layout = h5py.VirtualLayout(shape=shape, dtype=np.float32)
            idx = 0
            for dataset in [f[dataset_name] for f in h5files]:
                n = dataset.shape[0]
                vl_slice = [slice(None)] * len(shape)
                vl_slice[0] = slice(idx, idx + n)
                vl_slice = tuple(vl_slice)
                vsource = h5py.VirtualSource(dataset)
                layout[vl_slice] = vsource
                idx += n
            h5out.create_virtual_dataset(dataset_name, layout)
        h5out.close()
        printr(f"Wrote saxstats file {saxstats_file}")
    comm.barrier()
saxstats_h5 = h5py.File(saxstats_file, "r")
if rank == 0 and show_plots:
    nshow = 5000
    sums = saxstats_h5["/sums"][0:nshow, :]
    weight_sums = saxstats_h5["/weight_sums"][0:nshow, :]
    profiles = np.divide(sums, weight_sums, np.zeros_like(sums), where=weight_sums > 0)
    norms = np.mean(profiles[:, 300:400], axis=1)
    msk = profiles == 0
    profiles = (profiles.T / norms).T
    profiles[weight_sums == 0] = 0
    profiles[msk] = 0
    imview(
        profiles,
        hold=True,
        fs_label="q (1/A)",
        fs_lims=np.array(q_range) / 1e10,
        ss_label="Frame Number",
        title=f"Run {run_number}",
        gradient="flame",
    )
