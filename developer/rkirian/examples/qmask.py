from reborn import source, detector
from reborn.viewers.qtviews import view_pad_data
beam_file = "../../../geometry/beam.json"
geom_file = "../../../geometry/current.json"
beam = source.load_beam(beam_file)
geom = detector.load_pad_geometry_list(geom_file)
mask = geom.beamstop_mask(beam=beam, q_min=3e9, q_max=4e9)
view_pad_data(data=mask, pad_geometry=geom, beam=beam)