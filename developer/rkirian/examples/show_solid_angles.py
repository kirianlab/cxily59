from reborn import detector, source
from reborn.viewers.qtviews.padviews import view_pad_data
geom = detector.load_pad_geometry_list("../../../geometry/rick_agb_redo_2024-02-10.json")
beam = source.load_beam("../../../geometry/beam.json")
sa = geom.solid_angles()
view_pad_data(data=sa, pad_geometry=geom, beam=beam)
