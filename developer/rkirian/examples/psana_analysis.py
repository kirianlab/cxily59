import h5py
import numpy as np
import os
import pandas
import psana
from Detector import DetectorTypes
from reborn.analysis.fluctuations import data_correlation
from reborn.analysis.saxs import RadialProfiler
from reborn import const
from reborn.detector import load_pad_masks
from reborn.detector import PADGeometryList
from reborn.detector import PolarPADAssembler
from reborn import fileio
from reborn.misc import polar
from reborn.source import Beam
from time import time

tik = time()
# load experiment log
log_file = "./cxily5921_run_log.csv"
df = pandas.read_csv(filepath_or_buffer=log_file)
elog = df.set_index("Run").replace({float('nan'): None}).to_dict(orient="index")

# configurations
experiment_id = "cxily5921"
run_number = 158 # [None, 83, 80]
if run_number is None:
    raise RuntimeError("Need a run to analyze.")
log = elog[run_number]
results_dir = "./results"
os.makedirs(results_dir, exist_ok=True)
os.makedirs(f"{results_dir}/{run_number}", exist_ok=True)
cache_dir = f"{results_dir}/cache"
os.makedirs(cache_dir, exist_ok=True)
psana_dir = None  # Use this if the XTC data is in a strange location.
# geometry configurations
pad_id = "jungfrau4M"
geometry_file = './geometry/recentered_distance_refined_AgBe_run15.json'
geometry = PADGeometryList(filepath=geometry_file)
mask_set = {"./geometry/dead_pixels.mask"}  # use to include more than one mask
masks = [load_pad_masks(file_name=m) for m in mask_set]
concat_masks = [geometry.concat_data(data=m) for m in masks]
# mask = np.logical_and(concat_masks).astype(int)
mask = concat_masks[0].astype(int)
data_type = "calib"
# beam configurations
photon_energy = 7 * 1e3 * const.eV
beam_pulse_energy = 1 * 1e-3
beam_diameter = 110 * 1e-9
beam = Beam(photon_energy=photon_energy,
            pulse_energy=beam_pulse_energy,
            diameter_fwhm=beam_diameter)
# lcls configurations
has_indexing = False  # in case we want to skip around
# analysis configurations
radials = True
one_photon_peak = 7  # runstats (conincides with photon energy)
threshold = one_photon_peak / 2
sample_name = log["Sample"]
try:
    # convert to meters (always use SI units)
    sample_diameter = float(log["Size (nm)"]) * 1e-9
except ValueError:
    sample_diameter = None
    # use the sample diameter to setup the polar binning code
    # buffer runs also use the smaple diameter to sample identically
    raise RuntimeError("This run does not have sample configured!")
oversample = 6

# setup Shannon sampling polar pixels
pad_qmags = geometry.q_mags(beam=beam)
pad_phis = geometry.azimuthal_angles(beam=beam)
pad_pf = geometry.polarization_factors(beam=beam)
pad_sa = geometry.solid_angles()
pad_f2p = const.r_e ** 2 * beam.photon_number_fluence * pad_pf * pad_sa * mask

tp = 2 * np.pi
dq = tp / (sample_diameter * oversample)
qmax = pad_qmags.max()
qmin = pad_qmags.min()
qs = np.arange(qmin, qmax, dq)
theta = 2 * np.arcsin(qmax * beam.wavelength / (2 * tp))
qperp = qmax * np.cos(theta / 2)  # component of q-vector perpendicular to beam
dphi = dq / qperp  # phi step size
n_q = int(qs.size)  # number of q bins for Shannon sampling
n_p = int(tp / dphi)  # number of phi bins for Shannon sampling
n_p += n_p % 2  # make it even
dphi = 2 * np.pi / n_p  # Update dphi to integer phis
phis = np.arange(0, tp, dphi)
qi, pi = polar.bin_indices(n_q_bins=n_q,
                           q_bin_size=dq,
                           q_min=(qmin - dq / 2),
                           n_p_bins=n_p,
                           p_bin_size=dphi,
                           p_min=(- dphi / 2),
                           qs=qs,
                           ps=phis,
                           py=False)
polar_mask = polar.bin_sum(n_q_bins=n_q,
                           n_p_bins=n_p,
                           q_index=qi,
                           p_index=pi,
                           array=mask,
                           py=False)
m_cf = correlate(s1=polar_mask, s2=None, cached=False)
if radials:
    profiler = RadialProfiler(n_bins=n_q,
                              q_range=(qmin, qmax),
                              geometry=geometry,
                              mask=mask,
                              beam=beam)
kam_acf_sum = np.zeros((n_q, n_p))
n_frames_analyzed = 0

# setup psana
data_string = f"exp={experiment_id}:run={run_number}:smd"
if has_indexing:
    data_string = data_string.replace(":smd", ":idx")
if psana_dir is not None:
    data_string += f":dir={psana_dir}"
data_source = psana.DataSource(data_string)
# Get the list of event IDs (3-tuples)
event_ids = None
if cache_dir is not None:
    os.makedirs(cache_dir, exist_ok=True)
    cachefile = f"{cache_dir}/event_ids_{run_number:04d}.pkl"
    if os.path.exists(cachefile):
        print("Loading cached event IDs:", cachefile)
        event_ids = fileio.misc.load_pickle(cachefile)
if event_ids is None:
    event_ids = []
    for evt in data_source.events():
        evtId = evt.get(psana.EventId)
        event_ids.append(evtId.time() + (evtId.fiducials(),))
    if cache_dir is not None:
        print(f"Caching event IDs: {cachefile}")
        fileio.misc.save_pickle(event_ids, cachefile)
n_frames = len(event_ids)
# setup psana detectors
print(f"Available detectors: \n{psana.DetNames()}\n\n")
pad_detector = psana.Detector(pad_id, accept_missing=True)
if isinstance(pad_detector, DetectorTypes.MissingDet):  # psana missing detector class
    raise TypeError(f"Detector {pad_id} is missing.")
ebeam_detector = psana.Detector("EBeam")  # measures photon energy (not always available)

# let's get some data and do some analysis
h5file = f"{results_dir}/{run_number}.h5"
with h5py.File(h5file, "w") as hf:
    hf.create_dataset(f"experiment", data=experiment_id)
    hf.create_dataset(f"run", data=run_number)
    hf.create_dataset(f"geometry_file", data=geometry_file)
    hf.create_dataset(f"photon_energy", data=photon_energy)
    hf.create_dataset(f"threshold", data=threshold)
run = data_source.runs().__next__()
events = run.events()
for i, ts in enumerate(event_ids):
    event = None
    if has_indexing:
        event = run.event(psana.EventTime(int((ts[0] << 32) | ts[1]), ts[2]))
    else:
        event = events.__next__()
    if event is None:
        print(f"event {ts} (frame {i}) is None!")
        continue
    try:
        pe = ebeam_detector.get(event).ebeamPhotonEnergy() * const.eV
    except AttributeError:
        print(f"frame {i} causes ebeamPhotonEnergy failure.")
        continue
    if data_type == "calib":
        data = np.double(pad_detector.calib(event))
    elif data_type == "raw":
        data = np.double(pad_detector.raw(event))
    else:
        raise TypeError("Not an understood data_type (use calib or raw).")
    data[data < threshold] = 0  # try to suppress detector artifacts
    pad_data = geometry.concat_data(data) * pad_f2p
    # bin pixels
    polar_data = polar.bin_sum(n_q_bins=n_q,
                               n_p_bins=n_p,
                               q_index=qi,
                               p_index=pi,
                               array=pad_data,
                               py=False)
    data = subtract_masked_data_mean(data=polar_data, mask=polar_mask)
    d_cf = correlate(s1=polar_data, s2=None, cached=False)
    # m_cf = correlate(s1=polar_mask, s2=None, cached=False)  # mask is not changing shot to shot
    acf = np.zeros_like(data, dtype=float)
    np.divide(d_cf, m_cf, out=acf, where=m_cf != 0)

    kam_acf_sum += acf
    n_frames_analyzed += 1
    if radials:
        out = profiler.quickstats(data=data, weights=mask)
        m = profiler.get_median_profile(data=data, mask=mask)
    n_dropped = n_frames - n_frames_analyzed
    print(f"frames processed: {i} ({i / n_frames * 100}%)")
    print(f"frames dropped: {n_dropped} ({n_dropped / n_frames * 100}%)")
    h5file = f"{results_dir}/{run_number}/{i:06d}.h5"
    with h5py.File(h5file, "w") as hf:
        hf.create_dataset(f"index", data=i)
        hf.create_dataset(f"id/seconds", data=ts[0])
        hf.create_dataset(f"id/nanonseconds", data=ts[1])
        hf.create_dataset(f"id/fiducial", data=ts[2])
        hf.create_dataset(f"measured_photon_energy", data=pe)
        hf.create_dataset(f"data/{data_type}", data=polar_data)
        if radials:
            rkey = f"{data_type}/radial"
            hf.create_dataset(f"{rkey}/sum", data=out["sum"])
            hf.create_dataset(f"{rkey}/sum2", data=out["sum2"])
            hf.create_dataset(f"{rkey}/mean", data=out["mean"])
            hf.create_dataset(f"{rkey}/sdev", data=out["sdev"])
            hf.create_dataset(f"{rkey}/wsum", data=out["weight_sum"])
            hf.create_dataset(f"{rkey}/median", data=m)

h5file = f"{results_dir}/{run_number}.h5"
with h5py.File(h5file, "a") as hf:
    hf.create_dataset(f"n_frames", data=n_frames_analyzed)
    hf.create_dataset(f"acf_sum/{data_type}", data=kam_acf_sum)
tok = time()
print(f"Analysis of run {run_number} took {tok-tik}")
