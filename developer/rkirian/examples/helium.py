import numpy as np
from reborn import detector, source
from reborn.simulate.gas import get_gas_background, isotropic_gas_intensity_profile
from reborn.viewers.qtviews.padviews import view_pad_data
from reborn.const import eV, k, r_e
# Simulate the expected gas background.
# First: the background from the chamber.  Scatter comes from gas along
# the path of the beam downstream of the shroud.  Gas from upstream of
# the shroud is mostly shadowed by the shroud and other apertures.
# Second: the background from the region very close to the nozzle. We
# don't really know how much gas there is and how localized it is, so
# we guess....
# ALL UNITS ARE SI IN REBORN
# Use the beam and geometry from the experiment:
detector_distance = None
beam_file = "../../../geometry/beam.json"
geom_file = "../../../geometry/current.json"
poisson = False  # Include Poisson noise in the result?
per_solid_angle = False  # Use units of photons per solid angle
# Beware: "photons per solid angle" makes no sense for chamber gas.  Each pixel does not
#     have a singular value for "solid angle" due to the continuum of detector distances
n_steps = 20  # Number of points to sample along the beam path
pressure = 0.0073  # Pressure in the chamber (5.6e-5 torr)
temperature = 300  # Temperature of the gas
photon_energy = 7000 * eV  # Photon energy
beam_diameter = 100e-9  # Beam diameter (doesn't really matter)
pulse_energy = 0.3e-3  # Pulse energy of the x-ray beam
binning = 1  # Bin pixels if you want more speed (but expect more photons per pixel...)
gas_type = "humid_he"
he_percent=1.0
beam = source.load_beam(beam_file)
beam.photon_energy = photon_energy
beam.pulse_energy = pulse_energy
beam.diameter_fwhm = beam_diameter
geom = detector.load_pad_geometry_list(geom_file)
geom = geom.binned(binning)
if detector_distance is not None:
    geom.set_average_detector_distance(beam=beam, distance=detector_distance)
dist = geom.average_detector_distance(beam=beam)
print(f"Average detector distance: {dist*1e3:0.3g} mm")
chamber_helium = get_gas_background(
    pad_geometry=geom,
    beam=beam,
    path_length=[0.0, dist],
    gas_type=gas_type,
    he_percent=he_percent,
    water_percent=1-he_percent,
    temperature=temperature,
    pressure=pressure,
    poisson=poisson,
    n_simulation_steps=n_steps,
)
data = chamber_helium
print(f"{np.sum(data)} photons in total")
if per_solid_angle:
    data /= geom.solid_angles()
view_pad_data(pad_geometry=geom, beam=beam, data=data, title=f"Chamber Gas.  Photons per Solid Angle.  {he_percent*100:.2g}% He, {(1-he_percent)*100:.2g}% H2O")
# # A very crude approximation for the gas below the nozzle
# nozzle_helium = get_gas_background(
#     geom,
#     beam,
#     path_length=[0.0, 500e-6],  # ~500 um gas plume?
#     gas_type="he",
#     temperature=temperature,
#     pressure=100000e-3,  # ~1 mbar average gas pressure?
#     poisson=poisson,
#     n_simulation_steps=n_steps,
# )
# view_pad_data(pad_geometry=geom, beam=beam, data=nozzle_helium, title="Nozzle Helium Scatter")
