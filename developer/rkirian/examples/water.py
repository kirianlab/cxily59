import numpy as np
from reborn import detector, source
from reborn.simulate.solutions import get_pad_solution_intensity
from reborn.viewers.qtviews.padviews import view_pad_data
from reborn.const import eV
# Simulate the expected water scatter from a sheet.
# ALL UNITS ARE SI IN REBORN
# Use the beam and geometry from the experiment:
detector_distance = 0.55
beam_file = "../../../geometry/beam.json"
geom_file = "../../../geometry/current.json"
poisson = False  # Include Poisson noise in the result?
sheet_thickness = 100e-9  # Thickness of the sheet
temperature = 300  # Temperature of the water
photon_energy = 7000 * eV  # Photon energy
beam_diameter = 100e-9  # Beam diameter (doesn't really matter)
pulse_energy = 1e-3  # Pulse energy of the x-ray beam
binning = 1  # Bin pixels if you want more speed (but expect more photons per pixel...)
beam = source.load_beam(beam_file)
beam.photon_energy = photon_energy
beam.pulse_energy = pulse_energy
beam.diameter_fwhm = beam_diameter
geom = detector.load_pad_geometry_list(geom_file)
geom = geom.binned(binning)
if detector_distance is not None:
    geom.translate(
        [0, 0, detector_distance - geom.average_detector_distance(beam=beam)]
    )
dist = geom.average_detector_distance(beam=beam)
print(f"Average detector distance: {dist*1e3:0.3g} mm")
sheet_scatter = get_pad_solution_intensity(
    pad_geometry=geom,
    beam=beam,
    thickness=sheet_thickness,
    liquid="water",
    temperature=temperature,
    poisson=poisson,
)
sheet_scatter = geom.concat_data(sheet_scatter)
data = sheet_scatter / geom.solid_angles()
print(np.min(data), np.max(data))
view_pad_data(
    pad_geometry=geom,
    beam=beam,
    data=data,
    title=f"Sheet Scatter.  Photons per Solid Angle.  {sheet_thickness*1e9} nm Thickness.",
    poisson=poisson
)
# Rough calculation:
# for helium |F(0)|^2 ~ 4
# fluence is approx J = 1 mJ / pi (50 nm)^2
# The forward intensity is approx I(0) = N |F(0)|^2 J SA P r_e^2
# For beam area A, we have
# N = L * A * n
# where n from the ideal gas law is n = P / kT
# Since the fluence is J = E / A for pulse energy E, we have
# I(0) = L (P / kT) |F(0)|^2 E SA P r_e^2
# Plug in
# SA ~ (75 micron)^2 / (550 mm)^2
# L ~ 100e-6
#
