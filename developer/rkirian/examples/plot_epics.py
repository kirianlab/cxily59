import sys
import psana
import numpy as np
import matplotlib.pyplot as plt
if len(sys.argv) < 3:
    print("provide epics id")
run = sys.argv[1]
epics_id = sys.argv[2]
ds = psana.DataSource(f"exp=cxily5921:run={run}:smd")
det = psana.Detector(epics_id)
run = ds.runs().__next__()
dat = []
for (i, event) in enumerate(run.events()):
    val = det(event)
    if val is None:
        print("Value is None")
    dat.append(val)
    if not i % 1000:
        print(i, val)
plt.plot(np.array(dat))
plt.show()
