import h5py
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam
pads = load_pad_geometry_list('geometry/rick_and_adil_2024-02-01.json')
beam = load_beam('geometry/beam.json')
# beam.set_wavelength(...)
print(pads)
print(beam)
f = h5py.File('q.h5', 'w')
f['q'] = pads.q_mags(beam=beam)
f.close()
