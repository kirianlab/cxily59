#!/usr/bin/env python
import sys
import argparse
import numpy as np
import pyqtgraph as pg
from framegetter import LY59FrameGetterV4
from reborn import detector
from reborn.viewers.qtviews import PADView
from reborn.analysis.masking import StreakMasker
from config import get_config


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--run_number', type=int, default=65, help='Run number (default: 65)')
    parser.add_argument('-f', '--frame_number', type=int, default=-1, help='Frame number (default: random)')
    args = parser.parse_args()
    config = get_config(run_number=args.run_number)
    fg = LY59FrameGetterV4(run_number=args.run_number)
    if args.frame_number == -1:
        dataframe = fg.get_random_frame()
    else:
        dataframe = fg.get_frame(args.frame_number)
    print("Fetched frame", dataframe.get_frame_index())
    geom = dataframe.get_pad_geometry()
    beam = dataframe.get_beam()
    mask = detector.load_pad_masks(config["pad_detectors"][0]["mask"])
    mask = geom.concat_data(mask).astype(float)
    q_range = (0.06e10, 0.2e10)
    qmask = geom.beamstop_mask(q_min=q_range[0], q_max=q_range[1], beam=beam).astype(float)
    mask *= qmask
    streakmasker = StreakMasker(geom=geom, beam=beam, q_range=q_range, snr=8, streak_width=0.1*np.pi/180)
    data = dataframe.get_raw_data_flat()
    streakmask, extra = streakmasker.get_mask(pattern=data, mask=mask, return_extra=True)
    pv = PADView(pad_geometry=geom, beam=beam, mask=mask*streakmask, data=data, levels=(0, 7),)
    pv.add_ring(q_mag=q_range[0], pen=pg.mkPen(width=1))
    pv.add_ring(q_mag=q_range[1], pen=pg.mkPen(width=1))
    pv.set_mask_color([128, 0, 0, 75])
    pv.show_pad_indices()
    pg.plot(extra["snr"])
    data_list = geom.split_data(data*mask*(1-streakmask))
    ft = np.fft.fft2(data_list[20])
    ft = np.abs(ft)**2
    ft = np.fft.fftshift(np.log(ft+1))
    pg.image(ft)
    pv.start()
