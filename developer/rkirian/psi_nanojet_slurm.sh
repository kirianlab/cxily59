#!/bin/bash
[[ "$1" == "" ]] && exit
[[ -d slurm ]] || mkdir -p slurm
run="$1"
runstring="$(printf '%04d' ${run})"
mkdir -p "$(pwd)/slurm/scripts"
mkdir -p "$(pwd)/slurm/logs"
slurm_script="$(pwd)/slurm/scripts/psi_nanojet_${runstring}.sh"
slurm_log="$(pwd)/slurm/logs/psi_nanojet_${runstring}_"
echo "Run ${run}"
cat << EOF > ${slurm_script}
#!/bin/bash
#SBATCH --partition=milano
#SBATCH --account=lcls:cxily5921
#SBATCH --job-name=psi${runstring}
#SBATCH --output=${slurm_log}%j.out
#SBATCH --error=${slurm_log}%j.err
#SBATCH --ntasks=48
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2g
#SBATCH --time=0-5:00:00
cd $(pwd)
source setup.sh
mpirun python developer/rkirian/psi_nanojet.py --process_diffraction -r ${run}
EOF
source /sdf/group/lcls/ds/ana/sw/conda1/manage/bin/psconda.sh
sbatch ${slurm_script}
echo "Output of slurm written to ${slurm_logs}/psi_nanojet_${runstring}_*"