import h5py
import hdf5plugin
import numpy as np
import pyqtgraph as pg
from reborn.external.pyqtgraph import imview
file_buff = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/saxstats/r0167/saxstats.h5"
file_samp = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/saxstats/r0172/saxstats.h5"
h5_buff = h5py.File(file_buff, "r")
h5_samp = h5py.File(file_samp, "r")
sums_buff = h5_buff["/sums"][:]
weights_buff = h5_buff["/weight_sums"][:]
buffs = np.divide(sums_buff, weights_buff, np.zeros_like(sums_buff), where=weights_buff > 0)/1e17
sums_samp = h5_samp["/sums"][:]
weights_samp = h5_samp["/weight_sums"][:]
samps = np.divide(sums_samp, weights_samp, np.zeros_like(sums_samp), where=weights_samp > 0)/1e17
levels = (0, np.percentile(buffs, 90))
# imview(buffs, hold=True, levels=levels)
# imview(samps, hold=True, levels=levels)
samp = np.mean(samps, axis=0)
buff = np.mean(buffs, axis=0)
# plot = pg.plot(samp, pen="g")
# plot.plot(buff, pen="b")
buff *= np.mean(samp[200:300])/np.mean(buff[200:300])
diff = samp - buff*0.95
pg.plot(np.log10(diff[diff>0]))
pg.mkQApp().exec_()