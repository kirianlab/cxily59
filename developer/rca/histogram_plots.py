import numpy as np
import matplotlib.pyplot as plt
import random
from reborn import fileio

runs = set(range(1, 173, 1))

for r in runs:

    rfile = f"./results/runstats/r{r:04n}/checkpoints/r{r:04n}final_0_10000000_1"
    rstats = fileio.misc.load_pickle(rfile)

    h = rstats['histogram'].astype(float)
    hlog = np.log10(h + 1)
    npix, nbin = hlog.shape
    nsamples = 256

    rpf = lambda: random.sample(list(range(npix)), nsamples)

    rs = rpf()  # randomly sample pixels

    plt.figure()
    plt.imshow(hlog[rs, :].T, cmap="jet")
    plt.ylabel("Intensity")
    plt.xlabel("Pixels")
    plt.title("Repersentative Pixels")
    plt.xticks([])
    plt.show()

    meanpix = np.log(h.mean(0) + 1)
    hn = -5.126  # h.min(1)
    hm = 20.126  # h.max(1)
    x = np.linspace(hn, hm, 100)

    plt.figure()
    plt.plot(x, meanpix)
    plt.ylabel("log10(Counts + 1)")
    plt.xlabel("Intensity")
    plt.title("Mean Pixel Histogram")
    # plt.xticks([])
    plt.grid()
    plt.show()
