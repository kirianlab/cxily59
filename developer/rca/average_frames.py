#!/usr/bin/env python
import argparse
import numpy as np
from reborn.external.lcls import LCLSFrameGetter
from reborn.viewers.qtviews import PADView
from config import get_config
from reborn.fileio.getters import ListFrameGetter, FrameGetter
from reborn import detector, source, dataframe
from reborn.detector import load_pad_masks, PADGeometryList
from reborn.analysis import runstats
from reborn.analysis.saxs import RadialProfiler

import matplotlib.pyplot as plt

class SVDFrameGetter(FrameGetter):
    def __init__(self, vt, pad_geometry, beam, n_frames=10):
        super().__init__()  # Don't forget to initialize the base FrameGetter class.
        self.pad_geometry = pad_geometry
        self.beam = beam
        self.n_frames = n_frames  # This is important; you must configure the number of frames.
        self.vt = vt
        self.mask = mask
    def get_data(self, frame_number):
        intensity = self.vt[frame_number]
        df = dataframe.DataFrame()  # This creates a DataFrame instance, which combines PADGeometry with Beam and data.
        df.set_frame_index(self.current_frame)  # Not mandatory, but good practice.
        df.set_frame_id('VT %s' % frame_number)  # Not mandatory, but good practice.
        df.set_pad_geometry(self.pad_geometry)
        df.set_raw_data(intensity)
        df.set_beam(self.beam)
        df.validate()  # Not mandatory, but good practice.
        return df

def find_nearest_i(array,value,axis=None):
    """Return the index of the array item nearest to specified value"""
    return (np.abs(array-value)).argmin(axis=axis)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-rb', '--run_background', type=int, default=60, help='Run number of background/buffer')
    parser.add_argument('-rs', '--run_sample', type=int, default=61, help='Run number of sample')
    parser.add_argument('--raw', action="store_true", default=False, help="Use raw data, default is calib.")
    parser.add_argument("--mask", type=str, help="Path to reborn mask file.",
                        default="/sdf/data/lcls/ds/cxi/cxily5921/results/cxily59/geometry/r61-r60mask.mask")
    parser.add_argument("--geometry", type=str, help="Path to geometry file.",
                        default="/sdf/data/lcls/ds/cxi/cxily5921/results/cxily59/geometry/recentered_distance_refined_AgBe_run15.json")
    args = parser.parse_args()
    config = get_config(run_number=args.run_background)

    if args.raw:
        for pad_dict in config['pad_detectors']:
            pad_dict['data_type'] = 'raw'
    else:
        for pad_dict in config['pad_detectors']:
            pad_dict['data_type'] = 'calib'

    mask = load_pad_masks(file_name=args.mask)

    pad_geometry = PADGeometryList(filepath=args.geometry)
    mask = pad_geometry.concat_data(mask)
    mask = mask.astype(bool)

    fg_bg = LCLSFrameGetter(experiment_id=config['experiment_id'], run_number=args.run_background,
                         pad_detectors=config['pad_detectors'], cachedir=config['cachedir'],
                         photon_wavelength_pv=config["photon_wavelength_pv"],
                         mask=mask)
    fg_s = LCLSFrameGetter(experiment_id=config['experiment_id'], run_number=args.run_sample,
                     pad_detectors=config['pad_detectors'], cachedir=config['cachedir'],
                     photon_wavelength_pv=config["photon_wavelength_pv"],
                     mask=mask)
    # pv = PADView(frame_getter=fg, debug_level=1, percentiles=(10, 90), post_processors=[lambda x: np.log(x)])
    # pv.set_mask_color([128, 0, 0, 75])
    # pv.start()

    stats_bg = runstats.load_padstats("./results/runstats/r{r:04d}/checkpoints/r{r:04d}final_0_10000000_1".format(r=args.run_background))
    stats_s = runstats.load_padstats("./results/runstats/r{r:04d}/checkpoints/r{r:04d}final_0_10000000_1".format(r=args.run_sample))

    bg_sums = stats_bg['sums']
    s_sums = stats_s['sums']

    # plt.hist(bg_sums, bins=np.linspace(0,2e5,100))
    # plt.hist(s_sums, bins=np.linspace(0,2e5,100), alpha=0.5)
    # plt.show()

    bg_histogram = stats_bg['histogram']
    bg_cumsum = np.cumsum(bg_histogram, axis=1)
    bg_perpixsum = bg_cumsum[:,-1]
    bg_half_perpixsum_idx = find_nearest_i(bg_cumsum,bg_perpixsum[:,None]/2,axis=1)
    bin_min = stats_bg['histogram_params']['bin_min']
    bin_max = stats_bg['histogram_params']['bin_max']
    n_bins = stats_bg['histogram_params']['n_bins']
    bin_centers = np.linspace(bin_min,bin_max,n_bins)
    bg_median = bin_centers[bg_half_perpixsum_idx]
    #clip histogram from -4 to 17
    bg_histogram[:,0] = 0
    bg_histogram[:,-1] = 0
    plt.plot(bin_centers,np.mean(bg_histogram,axis=0))
    plt.show()
    bg_mean = np.sum(bg_histogram*bin_centers,axis=1)/stats_bg['n_frames']
    print(bg_mean)
    plt.plot(bin_centers,bg_histogram[0])
    plt.show()

    pv = PADView(data=bg_mean,pad_geometry=pad_geometry,beam=fg_bg.beam)
    pv.start()
    exit()

    plt.plot(bg_sums)
    plt.plot(s_sums)
    plt.show()

    df = fg_bg.get_next_frame()
    geometry = df.get_pad_geometry()
    beam = df.get_beam()
    q_mags = df.q_mags

    #only look at low q pixels in the inner asics for speed
    mask[q_mags > 0.2e10] = False

    #loop through frames and perform SVD
    #start with 10 frames
    nf = 10
    first_frame = 20000
    frame_iter = 100
    hit_threshold = 400000

    idx_hits_bg = np.argsort(bg_sums)[-nf-1:]
    idx_hits_s = np.argsort(s_sums)[-nf-1:]

    im_bg = []
    im_s = []
    j = 0
    i = 0
    while True:
        print(i,j)
        if j > nf:
            break
        print(idx_hits_bg[i])
        data_bg = fg_bg.get_frame(idx_hits_bg[i]).get_processed_data_flat()
        if data_bg.sum() > hit_threshold:
            im_bg.append(data_bg[mask])
            j += 1
        i += 1
    im_bg = np.vstack(im_bg)

    j = 0
    i = 0
    while True:
        print(i,j)
        if j > nf:
            break
        print(idx_hits_s[i])
        data_s = fg_s.get_frame(idx_hits_s[i]).get_processed_data_flat()
        if data_s.sum() > hit_threshold:
            im_s.append(data_s[mask])
            j += 1
        i += 1
    im_s = np.vstack(im_s)

    im_all = np.vstack((im_bg,im_s))

    u_bg, s_bg, vt_bg = np.linalg.svd(im_bg, full_matrices=False)
    u_s, s_s, vt_s = np.linalg.svd(im_s, full_matrices=False)
    u_all, s_all, vt_all = np.linalg.svd(im_all, full_matrices=False)

    for i in range(nf):
        plt.plot(u_all[:,i])
    plt.show()

    vt_full_bg = np.zeros((nf,data_bg.size))
    vt_full_s = np.zeros((nf,data_s.size))
    for i in range(nf):
        vt_full_bg[i,mask] = -vt_bg[i]
        vt_full_s[i,mask] = -vt_s[i]

    svd_fg_bg = SVDFrameGetter(vt_full_bg, pad_geometry=geometry,beam=beam)
    svd_fg_s = SVDFrameGetter(vt_full_s, pad_geometry=geometry,beam=beam)

    pv = PADView(frame_getter=svd_fg_s)
    pv.start()

    diff = vt_full_s[0] - vt_full_bg[0]

    pv = PADView(data=diff, pad_geometry=geometry,beam=beam)
    pv.start()


    rp = RadialProfiler(beam=beam, pad_geometry=geometry, mask=mask)
    q = rp.bin_centers
    radial_profile = rp.get_profile_statistic(diff, statistic=np.mean) 

    plt.plot(q,radial_profile)
    plt.show()