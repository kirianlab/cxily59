from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


# Special print function - only process number 0 (i.e. "rank 0") prints and prefix with rank number.
def print0(*args, **kwargs):
    if rank == 0:
        print(f"Rank {rank:2d}:", *args, **kwargs)


# Special print function - all processes print and prefix with rank number.
def printr(*args, **kwargs):
    print(f"Rank {rank:2d}:", *args, **kwargs)


import argparse
import time
import glob
import h5py
import hdf5plugin
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.source import load_beam
import os

basedir = "/sdf/data/lcls/ds/cxi/cxily5921/scratch"
datadir = f"{basedir}/data/v4/h5dat"
geomdir = f"{basedir}/rca/geometry"
savedir = f"{basedir}/data/rca"

parser = argparse.ArgumentParser(description="primary analysis script for nanojet runs (generates run statistics and frame radials)")
parser.add_argument("-r", "--run", type=int, required=True, help="run number")
args = parser.parse_args()

run = args.run
print0(f"Analyzing run {run:04d}")

# load geometry data
pads = load_pad_geometry_list(f"{geomdir}/current.json")
beam = load_beam(f"{geomdir}/beam.json")
print0(f"beam = {beam}")
# load masks (remove: edge + dead + hot pixels)
mask = reborn.detector.load_pad_masks([
    f"{geomdir}/edge_mask.mask",
    f"{geomdir}/mask_r0174_dead_pixels_maybe.mask",
    f"{geomdir}/mask_r0174_hot_pixels_maybe.mask"])
mask = pads.concat_data(mask) # ravel data consistently
mask = mask.astype(bool) # convert int64 to boolean
mask = np.where(mask)
mask = np.array(mask)[0, :]
# load data
datafile = f"{datadir}/{run:04d}/{run:04d}.h5"
file_buffer = h5py.File(datafile, "r")
n_frames, n_pixels = file_buffer["/data/calib"].shape
print0(f"data shape [# of frames, # of pixels]: {n_frames}, {n_pixels}")

# precompute radial bins
maxr = 6e9
nq_bins = 500
qvecs = pads.q_vecs(beam=beam).T
R = np.linalg.norm(qvecs, axis=0)
print0(f"R: {R}")
qbins = np.linspace(0, maxr, nq_bins + 1) # nodal points of radial bins
binindex = np.digitize(R, qbins[:-1]) - 1  # Adjust to include the rightmost edge
rad = 0.5 * (qbins[1:] + qbins[:-1]) # center points of radial bin

# precompute bin weights
solidAngles = pads.solid_angles()
polarizationFactors = pads.polarization_factors(beam=beam)
weights = np.zeros(nq_bins) 
np.add.at(weights, binindex[mask], polarizationFactors[mask] * solidAngles[mask])

# Split frames amongst processes
all_frames = np.arange(n_frames)
my_frames = comm.scatter(np.array_split(all_frames, size), root=0)
my_n_frames = len(my_frames)
printr(f"Processing {my_n_frames} frames ({my_frames[0]} - {my_frames[-1]})")

analysis_file = f"{savedir}/{run:04d}-rank{rank:03d}.h5"
printr(f"{analysis_file=}")
if rank == 0:
    os.makedirs(f"{savedir}/", exist_ok=True)
comm.barrier()  # Rank 0 makes the above directory.  Stop other processes from continuing until that is done.

h5File = h5py.File(analysis_file, "w")
SAXSDataSet = h5File.create_dataset("SAXS", shape=(my_n_frames, nq_bins), dtype=np.float64) # partitioned Data set

batch_size = 8
counter = 0
runsum = np.zeros(n_pixels)
runsum2 = np.zeros(n_pixels)
runmin = 1e6 * np.ones(n_pixels)
runmin[~mask] = 0
runmax = np.zeros(n_pixels)
histogrammer = PixelHistogram(
        bin_min=-5,
        bin_max=20,
        n_bins=100,
        n_pixels=n_pixels,
        zero_photon_peak=0,
        one_photon_peak=8,
        peak_width=None)
# Loop over array elements in batches
t0 = time.time()
for start_idx in range(0, my_n_frames, batch_size):
    end_ind = min(my_n_frames - 1, start_idx + batch_size) # because dataset is not a multiple of batch size
    batch_elements = np.arange(my_frames[start_idx],  my_frames[end_ind])
    dt = time.time() - t0
    pct = start_idx / my_n_frames * 100
    hz = start_idx / dt * size
    if hz == 0:
        ml = -99
    else:
        ml = size * (my_n_frames - start_idx) / hz / 60
    printr(f"Batch {batch_elements[0]:5d} - {batch_elements[-1]:5d} ({pct:5.1f}% done; {hz:.1f} Hz; {ml:5.2f} minutes left)")
    # get brightness info for SAXS 
    data_buffer = file_buffer["/data/calib"][batch_elements]
    runsum += data_buffer.sum(axis=0)
    runsum2 += (data_buffer ** 2).sum(axis=0)
    for i in batch_elements:
        frame_radial = np.zeros(nq_bins)
        frame = data_buffer[i - batch_elements[0]]
        np.add.at(frame_radial, binindex[mask], frame[mask])
        radial = np.divide(frame_radial, weights*7.0, np.zeros_like(frame_radial), where=weights>0) # photons per solid angle for this shot
        runmin[mask] = np.minimum(runmin[mask], frame[mask])
        runmax[mask] = np.maximum(runmax[mask], frame[mask])
        try:
            SAXSDataSet[counter] = radial
            counter += 1
        except Exception as e:  # 'catch' should be 'except', and it should catch a specific exception or 'Exception' as a general catch-all
            print("An error occurred while saving:", e)
            break
        histogrammer.add_frame(data=frame, mask=mask)
h5File.create_dataset("runmin", data=runmin)
h5File.create_dataset("runmax", data=runmax)
h5File.create_dataset("runsum", data=runsum)
h5File.create_dataset("runsum2", data=runsum2)
h5File.create_dataset("histogram", data=histogrammer.histogram)
h5File.close()
# Wait for all processes
comm.barrier()
if rank == 0:
    with h5py.File(f"{savedir}/{run:04d}.h5", "w") as h5File:
        n = 0
        SAXSDataSet = h5File.create_dataset("SAXS", shape=(n_frames, nq_bins), dtype=np.float64)
        for f in sorted(glob.glob(f"{savedir}/{run:04d}-rank*.h5")):
            printr(f"Aggregating file {os.path.basename(f)} into {savedir}/{run:04d}.h5")
            with h5py.File(f, "r") as h5f:
                h5b = h5f["SAXS"][:]
                m = h5b.shape[0]
                printr(f"Inserting frames {n} through {n+m}")
                SAXSDataSet[n:n+m, :] = h5b
                if "runmin" in h5File.keys():
                    current = h5File["runmin"][:]
                    h5File["runmin"][...] = np.minimum(current, h5f["runmin"])
                else:
                    h5File.create_dataset("runmin", data=h5f["runmin"])
                if "runmax" in h5File.keys():
                    current = h5File["runmax"][:]
                    h5File["runmax"][...] = np.maximum(current, h5f["runmax"])
                else:
                    h5File.create_dataset("runmax", data=h5f["runmax"])
                if "runsum" in h5File.keys():
                    h5File["runsum"] += h5f["runsum"]
                else:
                    h5File.create_dataset("runsum", data=h5f["runsum"])
                if "runsum2" in h5File.keys():
                    h5File["runsum2"]+= h5f["runsum2"]
                else:
                    h5File.create_dataset("runsum2", data=h5f["runsum2"])
                if "histogram" in h5File.keys():
                    h5File["histogram"] += h5f["histogram"]
                else:
                    h5File.create_dataset("histogram", data=h5f["histogram"])
            printr(f"Deleting file {os.path.basename(f)}")
            os.remove(f)
            n += m
printr("analysis done")
print0("done")
