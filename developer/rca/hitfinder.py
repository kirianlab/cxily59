#!/usr/bin/env python
import argparse
import numpy as np
import matplotlib.pyplot as plt
from runstats import get_runstats


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--run_number', type=int, default=35, help='Run number')
    args = parser.parse_args()

    stats = get_runstats(run_number=args.run_number)
    frame_ids = stats['frame_ids']
    sums = stats['sums']
    idx = np.argsort(sums)
    frame_ids_sorted = np.array(frame_ids)[idx, :]  # frame ids (seconds, nanoseconds, fiducials)

    plt.figure(figsize=(10, 10))
    plt.title(f'Run {args.run_number:04d}')
    plt.semilogy(sums[idx], '.')
    plt.ylabel('$\Sigma_k I_k (q)$')
    plt.xlabel('k')
    plt.grid()
    plt.savefig(f'hits_r{args.run_number:04d}.png')
