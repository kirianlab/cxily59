#!/usr/bin/env python
import argparse
import h5py
import numpy as np
import pandas
import reborn
from reborn.analysis.fluctuations import data_correlation
from reborn.analysis.parallel import ParallelAnalyzer
from reborn.analysis.saxs import RadialProfiler
from reborn.const import eV
from reborn.detector import PADGeometryList
from reborn.detector import PolarPADAssembler
from reborn.external.lcls import LCLSFrameGetter
from reborn.source import Beam


def load_elog():
    log_file = "./cxily5921_run_log.csv"
    df = pandas.read_csv(filepath_or_buffer=log_file)
    elog = df.set_index("Run").replace({float('nan'): None}).to_dict(orient="index")
    return set(df.keys()), elog


def get_config(run):
    # This is the place to modify the config according to run number (e.g. detector geometry, etc.)
    # base configurations
    config = dict(experiment_id='cxily5921',
                  run_id=run,
                  cachedir='cache/',
                  results_directory='/sdf/data/lcls/ds/cxi/cxily5921/scratch/cxily59/results/',  # e.g. ./results/r0045/
                  photon_energy=7*1e3*eV)
    # parallel analyzer configurations
    config.update({"log_file": f"results/r{run:04d}/logs/",
                   "checkpoint_file": f"results/r{run:04d}/checkpoints/",
                   "message_prefix": f"Run {run:04d}: "})
    # LCLS specific congigurations
    config.update({"photon_wavelength_pv": 'SIOC:SYS0:ML00:AO192'})
    # miscellaneous configurations
    config.update({"joblib_directory": "results/joblib/"})
    # detector configurations
    # make a dictionary for every available PAD detector
    # required keys: pad_id, geometry
    # possible keys: mask, motions
    # NOTES -- geometry: can be path to geom file or a pad_geometry_list_object
    #              mask: list of paths to masks (you can use multiple masks to take care of one particular feature)
    #                    example: ['badrows.mask', 'edges.mask', 'spots.mask', 'threshold.mask']
    #           motions: dictionary
    #                    example: {'epics_pv':'CXI:DS1:MMS:06.RBV', 'vector':[0, 0, 1e-3]}
    jungfrau4m = dict(pad_id='jungfrau4M',
                      geometry=PADGeometryList(filepath='./geometry/recentered_distance_refined_AgBe_run15.json'),
                      data_type='calib',
                      mask=None)
    config["pad_detectors"] = jungfrau4m  # list allows for multiple detectors
    config["detector_distance"] = 0.5
    # sample configurations
    keys, elog = load_elog()
    log = elog[run]
    config["sample_name"] = log["Sample"]
    try:
        config["sample_diameter"] = float(log["Size (nm)"]) * 1e-9
    except ValueError:
        config["sample_diameter"] = None
    # FXS analysis parameters
    config.update({"oversample": 4})
    return config


class AutocorrelationAnalysis(ParallelAnalyzer):
    r"""
    Class to compute angular autocorrelations.

    PolarPADAssembler object will be automatically created.
    The RadialProfiler will use the same parameters.

    The autocorrelation function (acf) and
        median
        sum
        sum2
        mean
        sdev
        weight_sum
    radials will be saved for each frame in individual HDF5 files.
    The files names will be based on LCLS event ids (i.e. seconds-nanoseconds-fiducials). 

    The HDF5 keys will follow the:
        frame/<index>/<data type>/acf
        frame/<index>/<data type>/radial/<statistic>
    naming convention.

    One file, named:
        r0000.h5
    will be created for the whole run and contain the mean acf, in key:
        kam_acf/<data type>
        

    Arguments:
        run (int): Run number to analyze.
        start (int): Which frame to start with.
        stop (int): Which frame to stop at.
        step (int): Step size between frames (default 1).
        n_processes (int): How many processes to run in parallel.
    """
    files = None  # list of files for final
    run_acf = None  # run autocorrelation

    def __init__(self, framegetter, run, start, stop, step, n_processes, radials=True):
        # conf = get_config(run=run)
        # fg = LCLSFrameGetter(run_number=conf["run_id"],
        #                      experiment_id=conf["experiment_id"],
        #                      pad_detectors=[conf["pad_detectors"]],
        #                      cachedir=conf["cachedir"],
        #                      photon_wavelength_pv=conf["photon_wavelength_pv"])
        super().__init__(framegetter=framegetter,
                         start=start,
                         stop=stop,
                         step=step,
                         n_processes=n_processes,
                         debug=1,
                         message_prefix=conf["message_prefix"],
                         log_file=conf["log_file"],
                         checkpoint_file=conf["checkpoint_file"],
                         clear_logs=False,
                         reduce_from_checkpoints=True,
                         checkpoint_interval=None,
                         clear_checkpoints=False)
        self.experiment_id = self.framegetter.experiment_id
        self.run_id = self.framegetter.run_id
        self.logger.info("Beginning analysis.")
        self.logger.info(f"Experiment: {self.experiment_id}\n")
        self.logger.info(f"Run       : {self.run_id}\n")
        beam = Beam(photon_energy=conf["photon_energy"])
        self.logger.info("Setting up polar assembler.")
        if conf["sample_diameter"] is None:
            raise RuntimeError("This run does not have sample!")
        self.polar_assembler = PolarPADAssembler.from_resolution(pad_geometry=conf["pad_detectors"]["geometry"],
                                                                 beam=beam,
                                                                 sample_diameter=conf["sample_diameter"],
                                                                 oversample=conf["oversample"])
        self.logger.info(f"q range  : {self.polar_assembler.q_range}")
        self.logger.info(f"q bins   : {self.polar_assembler.n_q_bins}")
        self.logger.info(f"phi range: {self.polar_assembler.phi_range}")
        self.logger.info(f"phi bins : {self.polar_assembler.n_phi_bins}")
        self.logger.info("Setting up radial profiler with polar assembler q settings.")
        self.profiler = RadialProfiler(mask=conf["pad_detectors"]["mask"],
                                       n_bins=self.polar_assembler.n_q_bins,
                                       q_range=self.polar_assembler.q_range,
                                       pad_geometry=conf["pad_detectors"]["geometry"],
                                       beam=beam)
        self.kwargs["run"] = run
        self.kwargs["radials"] = radials
        self.kwargs["data_type"] = conf["pad_detectors"]["data_type"]
        self.kwargs["results_directory"] = conf["results_directory"]
        self.files = []
        self.run_acf = np.zeros(self.polar_assembler.polar_shape)

    def add_frame(self, dat):
        if dat.validate():
            frame_idx = dat.get_frame_index()
            base = self.kwargs["results_directory"]
            base_file = f"{base}r{self.run_id:04d}"
            h5file = f"{base_file}/{frame_idx:06d}.h5"
            base_key = f"frame/{frame_idx}"
            with h5py.File(h5file, "w") as hf:
                hf.create_dataset(f"experiment", data=self.experiment_id)
                hf.create_dataset(f"run", data=self.run_id)
                self.logger.info("Adding valid Dataframe to analysis.")
                frame_id = dat.get_frame_id()
                hf.create_dataset(f"{base_key}/id/seconds", data=frame_id[0])
                hf.create_dataset(f"{base_key}/id/nanonseconds", data=frame_id[1])
                hf.create_dataset(f"{base_key}/id/fiducial", data=frame_id[2])
                data_key = f"{base_key}/{self.kwargs['data_type']}"
                frame_data = dat.get_raw_data_list()
                frame_mask = dat.get_mask_list()
                self.logger.info("Converting data to polar.")
                polar_data, polar_mask = self.polar_assembler.get_mean(
                    data=frame_data, mask=frame_mask
                )
                self.logger.info("Computing autocorrelation.")
                acf = data_correlation(n=0,
                                       data=polar_data,
                                       mask=polar_mask,
                                       cached=False)
                hf.create_dataset(f"{data_key}/acf", data=acf)
                self.run_acf += acf
                if self.kwargs["radials"]:
                    self.logger.info("Computing radials.")
                    rkey = f"{data_key}/radial"
                    out = self.profiler.quickstats(data=frame_data, weights=frame_mask)
                    hf.create_dataset(f"{rkey}/sum", data=out["sum"])
                    hf.create_dataset(f"{rkey}/sum2", data=out["sum2"])
                    hf.create_dataset(f"{rkey}/mean", data=out["mean"])
                    hf.create_dataset(f"{rkey}/sdev", data=out["sdev"])
                    hf.create_dataset(f"{rkey}/weight_sum", data=out["weight_sum"])
                    m = self.profiler.get_median_profile(data=frame_data, mask=frame_mask)
                    hf.create_dataset(f"{rkey}/median", data=m)
            self.logger.info(f"Patterns Averaged: {self.n_processed}")
            self.files.append(h5file)
        else:
            self.logger.warning("Skipping invalid Dataframe.")

    def to_dict(self):
        self.logger.info("Converting to dictionary.")
        adict = dict(experiment_id=self.experiment_id,
                     run_id=self.run_id,
                     run_sum_acf=self.run_acf,
                     run=self.kwargs["run"],
                     n_processed=self.n_processed,
                     files=self.files,
                     radials=self.kwargs["radials"])
        return adict

    def from_dict(self, adict):
        self.logger.info("Restoring from dictionary.")
        self.experiment_id = adict["experiment_id"]
        self.run_id = adict["run_id"]
        self.run_acf = adict[f"run_sum_acf"]
        self.kwargs["run"] = adict["run"]
        self.n_processed = adict["n_processed"]
        self.files = adict["files"]
        self.kwargs["radials"] = adict["radials"]

    def concatenate(self, adict):
        self.logger.info("Concatenating parallel processes.")
        self.files.extend(adict["files"])
        if self.run_acf is None:
            self.run_acf = adict["run_sum_acf"]
        else:
            self.run_acf += adict["run_sum_acf"]

    def finalize(self):
        self.logger.info(f"Finalizing. Saving Kam autocorrelation for run.")
        kam_acf = self.run_acf / self.n_processed
        base = self.kwargs["results_directory"]
        with h5py.File(f"{base}r{self.run_id:04d}.h5", "w") as hf:
            hf.create_dataset(f"experiment", data=self.experiment_id)
            hf.create_dataset(f"run", data=self.run_id)
            hf.create_dataset(f"kam_acf/{self.kwargs['data_type']}", data=kam_acf)
            for f in self.files:
                with h5py.File(f, "r") as fhf:
                    fhf.copy(source=fhf["frame"], dest=hf["frame"])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--run", type=int, default=170, help="Run number")
    parser.add_argument("--start", type=int, default=0, help="start frame")
    parser.add_argument("--stop",type=int, default=None, help="stop frame")
    parser.add_argument("--no_radials", action="store_false", help="turn off radials")
    #parser.add_argument("--screenshots", action="store_true",help="save pyqt screenshots as jpg")
    parser.add_argument(
        "-j", "--n_processes", type=int, default=12, help="Number of parallel processes"
    )
    args = parser.parse_args()

    print("Analysis starting.")
    print(f"run      : {args.run}")
    print(f"start    : {args.start}")
    print(f"stop     : {args.stop}")
    print(f"processes: {args.n_processes}")
    print(f"radials  : {args.no_radials}")
    conf = get_config(run=run)
    fg = LCLSFrameGetter(run_number=conf["run_id"],
                         experiment_id=conf["experiment_id"],
                         pad_detectors=[conf["pad_detectors"]],
                         cachedir=conf["cachedir"],
                         photon_wavelength_pv=conf["photon_wavelength_pv"])
    analysis = AutocorrelationAnalysis(framegetter=fg,
                                run=args.run,
                                       start=args.start,
                                       stop=args.stop,
                                       step=1,
                                       n_processes=args.n_processes,
                                       radials=args.no_radials)
    analysis.process_frames()
    print(f"Run {args.run} analysis finished.")
