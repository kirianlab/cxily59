import argparse
import subprocess
import os

logdir = "./slurm/logs/"
os.makedirs(logdir, exist_ok=True)

parser = argparse.ArgumentParser(description="automated slurm job submission")
parser.add_argument("-c", "--cores", type=int, default=48,
    help="number of core to run on")
parser.add_argument("-r", "--run", type=int, help="run number")
parser.add_argument("-s", "--script", type=str,
    required=True, help="mpi python script for slurm job")
args = parser.parse_args()

cores = args.cores
run = args.run
script = args.script

sbatch = f"slurm_{script.strip('.py')}_r{run:04d}.sh"
autoscript= f"""#!/bin/bash
#SBATCH --partition=milano
#SBATCH --account=lcls:cxily5921
#SBATCH --job-name={script}_r{run:04d}
##SBATCH --output=slurm/logs/%j.output
##SBATCH --error=slurm/logs/%j.err
#SBATCH --ntasks={cores}
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2g
#SBATCH --time=0-2:00:00
cd ../..
source setup.sh
cd developer/rca/
mpirun python {script} -r {args.run}
"""

with open(sbatch, "w") as f:
    f.write(autoscript)

subprocess.run(["sbatch", sbatch], capture_output=True)
subprocess.run(["mv", sbatch, f"{logdir}{sbatch}"], capture_output=True)
