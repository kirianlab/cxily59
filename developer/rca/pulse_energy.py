# from andy
import psana as ps
import matplotlib.pyplot as plt
import numpy as np

ds  = ps.DataSource('exp=cxily5921:run=150:smd')
fee = ps.Detector('FEEGasDetEnergy') #gas monitor
dio = ps.Detector('CXI-USR-DIO') #diode
pressor = ps.Detector('CXI:SC2:GCC:01:PMON')
wave8 = np.empty_like([])
gas = np.empty_like([])
for n,e in enumerate(ds.events()):
    if n > 2000: break
    gem = fee.get(e)
    if gem is None: continue
    d = dio.get(e)
    if d is None: continue
    wave8 = np.append(wave8, -1.0*d.peakA()[15]) #note diode was on channel 15
    gas = np.append(gas,gem.f_11_ENRC()) # note f_12, f_21, f_22, f_63, and f_64 are all gas monitors

print(pressor())
plt.scatter(gas,wave8)
plt.show()


