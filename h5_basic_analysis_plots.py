import argparse
import h5py
import numpy as np
import matplotlib.pyplot as plt
from reborn.const import eV
from reborn.detector import PADGeometryList, PolarPADAssembler
from reborn.source import Beam
from reborn.viewers.mplviews.padviews import view_pad_data

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--run", type=int, default=158, help="Run number")
    args = parser.parse_args()

    run_number = args.run

    with h5py.File(f"../data/{run_number}.h5", "r") as hf:
        geometry_file = hf["geometry_file"][()]
        n_events, _ = hf["event_ids_all"].shape

    geometry = PADGeometryList(filepath=geometry_file)
    beam = Beam(photon_energy=7 * 1e3 * eV)

    with h5py.File(f"../analysis/{run_number}.h5", "r") as hf:
        n_frames, _ = hf["event_ids_analyzed"].shape
        n_q = hf['n_q_bins'][()]
        n_p = hf['n_phi_bins'][()]
        pd = hf['polar_data/calib'][:].reshape((n_frames, n_q, n_p))
        kacf = hf["polar_data/calib_kam_acf_sum"][:]
        padsum = hf["stats/pad_sum"][:]
        padsum2 = hf["stats/pad_sum2"][:]
        padmax = hf["stats/pad_max"][:]
        padmin = hf["stats/pad_min"][:]
        sample_diameter = hf["sample_diameter"][()]
        oversample = hf["oversample"][()]
        threshold = hf["threshold"][()]

    print(f"Run {run_number} events with data above threshold {threshold}: {(n_frames / n_events) * 100:02f}%")

    assembler = PolarPADAssembler.from_resolution(pad_geometry=geometry,
                                                  beam=beam,
                                                  sample_diameter=sample_diameter,
                                                  oversample=oversample)

    view_pad_data(data=padsum, pad_goemetry=geometry, title=f"Run {run_number} PAD Statistics (Sum)")
    view_pad_data(data=padsum2, pad_goemetry=geometry, title=f"Run {run_number} PAD Statistics (Sum^2)")
    view_pad_data(data=padmax, pad_goemetry=geometry, title=f"Run {run_number} PAD Statistics (Max)")
    view_pad_data(data=padmin, pad_goemetry=geometry, title=f"Run {run_number} PAD Statistics (Min)")

    ys = np.arange(0, n_q, 5)
    yticks = np.round(assembler.q_bin_centers[ys] * 1e-10, 5)

    phisum = pd.sum(axis=2)
    radial = phisum.sum(axis=0)
    runsum = phisum.sum(axis=1)

    plt.figure()
    plt.title(f"Run {run_number} Kam Auto-Correlations")
    plt.imshow(kacf / n_frames, cmap='RdYlBu', interpolation=None)
    plt.ylabel('q = 2 $\pi$ sin $\Theta$ / $\lambda$  [$\AA^{-1}$]')
    plt.yticks(ys, yticks)
    plt.xlabel('$\phi$')
    plt.xticks([0, int(n_p/4), int(n_p / 2), int(2 * n_p / 3), n_p],
               ['0', '$\pi$/2', '$\pi$', '3$\pi$/2', '2$\pi$'])
    plt.colorbar()
    plt.gca().set_aspect('auto')
    plt.tight_layout()

    plt.figure()
    plt.title(f"Run {run_number} Polar Data Sum")
    plt.imshow(runsum, cmap='RdYlBu', interpolation=None)
    plt.ylabel('q = 2 $\pi$ sin $\Theta$ / $\lambda$  [$\AA^{-1}$]')
    plt.yticks(ys, yticks)
    plt.xlabel('$\phi$')
    plt.xticks([0, int(n_p/4), int(n_p / 2), int(2 * n_p / 3), n_p],
               ['0', '$\pi$/2', '$\pi$', '3$\pi$/2', '2$\pi$'])
    plt.colorbar()
    plt.gca().set_aspect('auto')
    plt.tight_layout()

    plt.figure()
    plt.title(f"Run {run_number} Radial")
    plt.plot(radial, ".")
    plt.xlabel('q')
    plt.xticks(ys, yticks)
    plt.gca().set_aspect('auto')
    plt.tight_layout()

    plt.figure()
    for c in range(n_q):
        plt.title(f"Run {run_number} Kam ACF (q={assembler.q_bin_centers[c] * 1e-10} $\AA^{-1}$)")
        plt.plot(c[2:-2], ".")
        plt.xlabel('$\phi$')
        plt.xticks([0, int(n_p/4), int(n_p / 2), int(2 * n_p / 3), n_p],
                   ['0', '$\pi$/2', '$\pi$', '3$\pi$/2', '2$\pi$'])
        plt.gca().set_aspect('auto')
        plt.tight_layout()
    plt.show()
