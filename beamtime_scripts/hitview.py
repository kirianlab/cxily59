#!/usr/bin/env python
import argparse
import numpy as np
from reborn.external.lcls import LCLSFrameGetter
from reborn.viewers.qtviews import PADView
from runstats import get_runstats
from config import get_config


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--run_number', type=int, default=35, help='Run number')
    #parser.add_argument('--calib', action="store_true", default=False, help="Use calib data, default is raw.")
    parser.add_argument('--raw', action="store_true", default=False, help="Use raw data, default is calib.")
    args = parser.parse_args()
    config = get_config(run_number=args.run_number)

    '''
    if args.calib:
        for pad_dict in config['pad_detectors']:
            pad_dict['data_type'] = 'calib'
    else:
        for pad_dict in config['pad_detectors']:
            pad_dict['data_type'] = 'raw'
    '''
    if args.raw:
        for pad_dict in config['pad_detectors']:
            pad_dict['data_type'] = 'raw'
    else:
        for pad_dict in config['pad_detectors']:
            pad_dict['data_type'] = 'calib'
    stats = get_runstats(run_number=args.run_number)
    frame_ids = stats['frame_ids']
    sums = stats['sums']
    idx = np.argsort(sums) #[::-1]
    frame_ids_sorted = np.array(frame_ids)[idx, :]
    fg = LCLSFrameGetter(experiment_id=config['experiment_id'], run_number=args.run_number,
                         pad_detectors=config['pad_detectors'], cachedir=config['cachedir'],
                         photon_wavelength_pv=config["photon_wavelength_pv"], event_ids=frame_ids_sorted)
    pv = PADView(frame_getter=fg, debug_level=1, percentiles=(10, 90), post_processors=[lambda x: np.log(x)])
    pv.set_mask_color([128, 0, 0, 75])
    pv.start()
