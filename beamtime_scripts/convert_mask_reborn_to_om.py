import argparse, os
import h5py
import numpy as np
import matplotlib.pyplot as plt
from reborn.detector import load_pad_geometry_list, load_pad_masks, save_mask_as_om_h5
from reborn.external import crystfel


if __name__ =="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--mask", type=str, help="Path to reborn mask file.",
                        default="./geometry/dead_pixels.mask")
    parser.add_argument("--geometry", type=str, help="Path to geometry file.",
                        default="./geometry/2023.08.05AgBhnt_r15.json")
    parser.add_argument("--out", type=str, help="Where to save OM mask.",
                        default="./geometry/dead_pixels_mask.h5")
    args = parser.parse_args()

    print("converting mask from reborn to OM format")

    basename, ext = os.path.splitext(args.geometry)
    if ext == 'geom':
        base_geometry = crystfel.geometry_file_to_pad_geometry_list(args.geometry)
    else:
        base_geometry = load_pad_geometry_list(file_name=args.geometry)
    base_mask = load_pad_masks(file_name=args.mask)
    save_mask_as_om_h5(base_mask, base_geometry, args.out)

