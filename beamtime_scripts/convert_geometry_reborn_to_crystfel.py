import argparse, os
import reborn
from reborn.detector import load_pad_geometry_list
from reborn.external.crystfel import write_geom_file_from_pad_geometry_list


if __name__ =="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--reborn", type=str, help="Path to reborn geometry file.",
                        default="./geometry/recentered_AgBe_run15.json")
    parser.add_argument("-e", "--energy", type=float, help="Photon energy in keV.",
                        default=7)
    parser.add_argument("--out", type=str,
                        help="Path where to save crystfel geometry file converted from reborn format.",
                        default=None)
    args = parser.parse_args()

    basename, ext = os.path.splitext(args.reborn)
    if args.out is None:
        args.out = basename+'.geom'
    print("converting geometry from reborn to crystfel format")
    beam = reborn.source.Beam(photon_energy=args.energy * 1e3 * reborn.const.eV)
    base_geometry = load_pad_geometry_list(file_name=args.reborn)
    write_geom_file_from_pad_geometry_list(pad_geometry=base_geometry,
                                           file_path=args.out,
                                           beam=beam)
