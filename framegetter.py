import h5py
import hdf5plugin
import numpy as np
import reborn
from glob import glob
from reborn import dataframe, detector, fileio, source
from config import get_config

class LY59FrameGetter(fileio.getters.FrameGetter):

    def __init__(
        self,
        run_number,
        beam=None,
        mask=None,
        data_type="calib",
        results_dir = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data",
        **kwargs
    ):
        r"""
        Arguments:
            run_number (int): run number
            beam (|Beam|): Beam info (optional, defaults to 7 keV beam)
            mask (str, |ndarray|): masked pixles 
                                    - if str: mask will be loaded in the standard reborn way
                                    - if |ndarray|: mask will be used as is
                                    - otherwise: no pixels will be masked
            data_type (str): "raw" or "calib" (defaults to calib)
                                - make sure data type has been converted from xtc
            results_dir (str): what directory has your data been saved in
                                - default is "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data"
        """
        super().__init__(**kwargs)
        self.experiment_id = "cxily5921"
        self.run_number = run_number
        self.data_type = data_type
        self.source = f"{results_dir}/{run_number}.h5"
        self.data_source = h5py.File(self.source, "r")
        self.event_ids = self.data_source["event_ids"][()]
        donef = glob(f"{results_dir}/{run_number}/*.h5")
        done_idx = np.unique(np.array([int(x.split("/")[-1].strip(".h5")) for x in donef]))  # indices of frames with data
        self.good_ids = self.event_ids[done_idx]
        self.n_frames = self.good_ids.shape[0]
        geometry_file = self.data_source["geometry_file"][()].decode()
        self.geometry = detector.PADGeometryList(filepath=geometry_file)
        if isinstance(beam, source.Beam):
            self.beam = beam
        else:
            self.beam = source.Beam(photon_energy=7 * 1e3 * reborn.const.eV)
        if isinstance(mask, str):
            self.mask = detector.load_pad_masks(filename=mask)
        elif isinstance(mask, np.ndarray):
            self.mask = mask
        else:
            self.mask = np.ones(self.geometry.n_pixels)

    def get_data(self, frame_number=0):
        df = dataframe.DataFrame()
        df.set_dataset_id(f"{self.experiment_id}:{self.run_number}")
        df.set_frame_id(self.good_ids[frame_number])
        df.set_frame_index(frame_number)
        df.set_pad_geometry(self.geometry)
        df.set_raw_data(self.data_source[f"data/{self.data_type}"][frame_number])
        df.set_beam(self.beam)
        df.set_mask(self.mask)
        return df


class LY59FrameGetterV2(fileio.getters.FrameGetter):

    def __init__(
        self,
        run_number,
        beam=None,
        mask=None,
        data_type="calib",
        results_dir = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v2/h5dat",
        **kwargs
    ):
        r"""
        Arguments:
            run_number (int): run number
            beam (|Beam|): Beam info (optional, defaults to 7 keV beam)
            mask (str, |ndarray|): masked pixles
                                    - if str: mask will be loaded in the standard reborn way
                                    - if |ndarray|: mask will be used as is
                                    - otherwise: no pixels will be masked
            data_type (str): "raw" or "calib" (defaults to calib)
                                - make sure data type has been converted from xtc
            results_dir (str): what directory has your data been saved in
                                - default is "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data"
        """
        super().__init__(**kwargs)
        self.config = get_config(run_number)
        self.experiment_id = "cxily5921"
        self.run_number = run_number
        self.data_type = data_type
        self.source = f"{results_dir}/{run_number:04d}/{run_number:04d}.h5"
        self.data_source = h5py.File(self.source, "r")
        self.event_ids = self.data_source["event_ids"][()]
        self.n_frames = self.event_ids.shape[0]
        self.geometry = self.config["pad_detectors"][0]["geometry"]
        self.beam = beam
        if beam is None:
            self.beam = self.config["beam"]
        if isinstance(mask, str):
            self.mask = detector.load_pad_masks(filename=mask)
        elif isinstance(mask, np.ndarray):
            self.mask = mask
        else:
            self.mask = np.ones(self.geometry.n_pixels)

    def get_data(self, frame_number=0):
        df = dataframe.DataFrame()
        df.set_dataset_id(f"{self.experiment_id}:{self.run_number}")
        df.set_frame_id(self.event_ids[frame_number])
        df.set_frame_index(frame_number)
        df.set_pad_geometry(self.geometry)
        df.set_raw_data(self.data_source[f"data/{self.data_type}"][frame_number])
        df.set_beam(self.beam)
        df.set_mask(self.mask)
        return df


class LY59FrameGetterV4(fileio.getters.FrameGetter):

    def __init__(
        self,
        run_number,
        beam=None,
        mask=None,
        data_type="calib",
        results_dir = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat",
        streakmask=None,
        **kwargs
    ):
        r"""
        Arguments:
            run_number (int): run number
            beam (|Beam|): Beam info (optional, defaults to 7 keV beam)
            mask (str, |ndarray|): masked pixles
                                    - if str: mask will be loaded in the standard reborn way
                                    - if |ndarray|: mask will be used as is
                                    - otherwise: no pixels will be masked
            data_type (str): "raw" or "calib" (defaults to calib)
                                - make sure data type has been converted from xtc
            results_dir (str): what directory has your data been saved in
                                - default is "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data"
        """
        super().__init__(**kwargs)
        self.config = get_config(run_number)
        self.experiment_id = "cxily5921"
        self.run_number = run_number
        self.data_type = data_type
        self.source = f"{results_dir}/{run_number:04d}/{run_number:04d}.h5"
        self.data_source = h5py.File(self.source, "r")
        self.event_ids = self.data_source["event_ids"][()]
        self.n_frames = self.event_ids.shape[0]
        self.geometry = self.config["pad_detectors"][0]["geometry"]
        self.beam = beam
        self.streakmask = None
        if streakmask is not None:
            try:
                self.streakmask = h5py.File(streakmask, "r")["/masks"]
                print(f"Loaded dataset '/masks' in {streakmask}")
            except:
                print(f"Cannot find streakmask at {streakmask}")
        if beam is None:
            self.beam = self.config["beam"]
        if isinstance(mask, str):
            self.mask = detector.load_pad_masks(filename=mask)
        elif isinstance(mask, np.ndarray):
            self.mask = mask
        else:
            self.mask = np.ones(self.geometry.n_pixels)

    def get_data(self, frame_number=0):
        df = dataframe.DataFrame()
        df.set_dataset_id(f"{self.experiment_id}:{self.run_number}")
        df.set_frame_id(self.event_ids[frame_number])
        df.set_frame_index(frame_number)
        df.set_pad_geometry(self.geometry)
        df.set_raw_data(self.data_source[f"/calib"][frame_number])
        df.set_beam(self.beam)
        mask = self.mask.copy()
        if self.streakmask is not None:
            print(f"Getting streakmask for frame {frame_number}")
            mask *= self.streakmask[frame_number, :]
        df.set_mask(mask)
        return df