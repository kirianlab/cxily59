#!/bin/bash

USER="$1"
if [ "$USER" == "" ]; then
       echo 'Please include your psana username'
       exit
fi

rsync -arv --exclude doc --exclude tests --exclude '*.pkl' --exclude joblib --exclude '*.h5' --exclude '*.so' --exclude '.git' --exclude '*.pyc' --exclude '__pycache__' --exclude '.idea' --exclude '*.md5' . "$USER"@s3dfdtn.slac.stanford.edu:/sdf/data/lcls/ds/cxi/cxily5921/scratch/"$USER"/cxily59
