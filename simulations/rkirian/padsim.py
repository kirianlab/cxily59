from reborn.const import torr
import simtools
conf = simtools.default_config()
energy = 0.1e-3
diameter = 100e-9
thickness = 100e-9
conf["fxs_polar_detector"] = True
conf["fxs_n_shots"] = 10000
conf["save_directory"] = "results/test"
conf["pulse_energy"] = energy
conf["beam_diameter"] = diameter
conf["container_thickness"] = thickness
conf["container_diameter"] = diameter
conf["container_bulk_scatter"] = True
conf["gas_background"] = True,  # Include gas background?
conf["gas_params"]["pressure"] = 3e-4 * torr
conf["poisson"] = True
conf["pad_binning"] = 1
conf["n_proteins"] = None  # None means calculate based on volume/concentration
objects = simtools.setup_simulate(conf)
pad_framegetter = objects["pad_framegetter"]
pad_framegetter.view()