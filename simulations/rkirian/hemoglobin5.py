# Script to check convergence rate as a function of number of particles, under
# ideal conditions (noise-free, background-free, ideal polar detector).
import os
import shutil
import numpy as np
import matplotlib.pyplot as plt
import simtools
from reborn.fileio.misc import load_pickle
savedir = "results/hemoglobin5/"
reload = True
view = 0
conf = simtools.default_config()
conf["fxs_polar_detector"] = True
conf["fxs_n_shots"] = 10000
# Simulate perfectly ideal correlations.  This will serve as a template to compare against others.
conf["save_directory"] = savedir + "ground_truth/"
simtools.simulate(conf, reload=reload)
results = simtools.pearson_convergence(conf["save_directory"], view=view)
template = results["template"]
# Simulate ideal correlations with varying numbers of particles.
ps = []
ns = []
for n in np.arange(5):
    n_prot = 2**n
    conf["n_proteins"] = n_prot
    conf["save_directory"] = savedir + f"{n_prot:04d}/"
    simtools.validate_config(conf)
    simtools.simulate(conf, reload=reload)
    results = simtools.pearson_convergence(conf["save_directory"], template=template, view=view)
    p = results["pearson"]
    ps.append(p)
    ns.append(n_prot)
frames = results["n_frames"]
qs = results["qs"]
# objects = load_pickle(conf["save_directory"] + "/objects.pkl")
# g = objects["polar_geom"]
# qs = np.linspace(g.q_bin_center_min, g.q_bin_center_max, g.n_q_bins)
# Now make a plot of the pearson correlation convergence
q_indices = [5, 10, 20, 30]
for qi in q_indices:
    plt.figure()
    for i in range(len(ns)):
        p = ps[i]
        plt.plot(frames, ps[i][:, qi], label=f"{ns[i]} particles")
    plt.legend()
    plt.title(f"Hemoglobin (q={qs[qi]*1e-10:0.3f}/A)")
    plt.xlabel("Number of patterns")
    plt.ylabel("Pearson correlation")
    plt.ylim(0.99, 1.001)
for p in ps:
    plt.figure()
    plt.imshow(np.array(p))
plt.show()
