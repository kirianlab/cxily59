import os
import glob
from time import time
import numpy as np
from numpy.fft import fftn, fftshift, ifftshift
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation
import pyqtgraph as pg
from joblib import Memory
from reborn import utils, source, detector, dataframe, const
from reborn.misc.interpolate import trilinear_interpolation
from reborn.misc import polar
from reborn.simulate.form_factors import sphere_form_factor
from reborn.target import crystal, atoms, placer
from reborn.fileio.getters import FrameGetter
from reborn.fileio.misc import save_pickle, load_pickle
from reborn.simulate import solutions, gas
from reborn.external import denss
from reborn.external.pyqtgraph import imview

memory = Memory("joblib")

###################################################################
# Constants
##########################################################################
eV = const.eV
r_e = const.r_e
NA = const.N_A
h = const.h
c = const.c
water_density = 1000
rad90 = np.pi / 2
mbar = const.mbar
torr = const.torr


def default_config():
    conf = dict(
        poisson=False,  # Include Poisson noise
        n_proteins=1,  # Fix the number of proteins.  Set to None if the number should be determined by concentration.
        incoherent=True,  # Remove coherence (i.e. sum intensities not amplitudes)
        particle_poisson=False,  # Poisson noise on particle counts
        create_bio_assembly=True,  # Expand atomic coordinates to include the biomolecular assembly (e.g. virus structure)
        container_type="sheet",  # Either drop or sheet
        container_shape_transform=False,
        container_bulk_scatter=False,
        container_diameter=100e-9,  # Average droplet diameter
        container_diameter_sdev=0,  # FWHM of droplet radius distribution (tophat distribution at present)
        container_thickness=10e-9,
        container_thickness_sdev=0,
        solvent_contrast=True,  # Include solvent contrast (water)
        pad_geometry_file="jungfrau.json",  # Can be list of detectors
        pad_binning=4,  # For speed, bin the pixels
        pad_translation=None,  # Translate after loading
        beamstop_size=0,  # Mask beamstop region
        photon_energy=7000 * eV,  # All units are SI
        pulse_energy=0.6e-3,
        beam_diameter=100e-9,  # FWHM of x-ray beam (tophat profile at present)
        map_resolution=0.99e-10,  # Minimum resolution for 3D density map.
        map_oversample=4,  # Oversampling factor for 3D density map.
        pdb_file="./2qsp.pdb",  # "LargeCluster2nm.pdb",  # '1jb0' '1SS8' '3IYF' '1PCQ' '2LYZ' 'BDNA25_sp.pdb'
        protein_concentration=10,  # Protein concentration in mg/ml = kg/m^3
        # Parameters for gas background calculation.  Give the full path through which x-rays interact with the gas.
        gas_background=False,  # Include gas background?
        gas_params={
            "path_length": [0, None],
            "gas_type": "he",
            "pressure": 3e-4 * torr,
            "n_simulation_steps": 20,
            "temperature": 293,
        },
        fxs_ring_resolution=1.4e-9,  # Autocorrelation ring resolution
        fxs_ring_oversampling=6,  # Oversampling of polar detector
        fxs_n_shots=100,  # Number of shots to simulate
        fxs_polar_detector=False,  # Simulate on an ideal polar detector (rather than PAD)
        fxs_subtract_mean=False,  # Correlate the mean subtracted rings (questionable, due to unequal pixel weights)
        debug=1,
        save_directory="results",  # Where to save results
        save_points=list(2 ** np.arange(30)),  # What frame numbers to save
    )
    return conf


def validate_config(config):
    r"""Validate configuration dictionary.  Check that the set of keys are identical to the set of keys
    in the default config.  Does not check if the values are sensible.  Raises ValueError exception if
    a problem is detected."""
    default = default_config()
    valid = config.keys() == default.keys()
    if not valid:
        raise ValueError(
            "Something is wrong with the dictionary.  Allowed keys are",
            list(default.keys()),
        )


def compare_configs(config1, config2, ignore_keys=[]):
    r"""Compare parameters in two config dictionaries.  Useful when determining if prior
    calculations can be used instead of re-running a simulation.

    Args:
        config1 (dict): Config dictionary
        config2 (dict): Config dictionary
        ignore_keys (list): Ignore these keys
    """
    # Check if keys match
    set1 = set(config1)
    set2 = set(config2)
    for key in ignore_keys:
        set1.remove(key)
        set2.remove(key)
    # Check if values match
    same = set1 == set2
    if same:
        for k in set1:
            s = config1[k] == config2[k]
            if not s:
                print("Mismatch on key", k)
                print(f"config1[{k}] =", config1[k])
                print(f"config2[{k}] =", config2[k])
                same = False
    return same


class DensityMap:
    def __init__(
        self,
        pdb_file,
        resolution,
        oversampling,
        solvent_contrast=True,
        bio_assembly=True,
        joblib_memory=None,
    ):
        r"""Container class for electron density with contrast against solvent (via DENSS).  Also holds the FFT
        for quick lookups when simulating Ewald slices.  Important: the arrays are centered in the sense that the
        zero-frequency value of the FFT is not at voxel index [0, 0, 0], and likewise the origin of real-space is not
        at [0, 0, 0]."""
        if joblib_memory:
            create_density_map = memory.cache(denss.create_density_map)
        else:
            create_density_map = denss.create_density_map
        rho, side, _ = create_density_map(
            pdb_file=pdb_file,
            solvent_contrast=solvent_contrast,
            create_bio_assembly=bio_assembly,
            map_resolution=resolution,
            map_oversampling=oversampling,
            ignore_waters=True,
        )
        self.rho = rho
        self.F = fftshift(fftn(ifftshift(rho))).copy()
        self.dr = side / rho.shape[0]  # This is close but not equal to map_resolution
        map_qmags = 2 * np.pi * np.fft.fftshift(np.fft.fftfreq(rho.shape[0], d=self.dr))
        self.q_min = [map_qmags[0]] * 3
        self.q_max = [map_qmags[-1]] * 3

    def view_projection(self, axis=2):
        pg.image(np.sum(self.rho.real, axis=axis), title="denss real-space map")
        pg.mkQApp().exec_()


class SolutionContainer:
    rng_diameter = None
    rng_thickness = None
    rng_volume = None
    rng_n_particles = None
    rng_coordinates = None

    def __init__(
        self,
        type="drop",
        diameter=None,
        molecule=None,
        concentration=None,
        geom=None,
        beam=None,
        thickness=None,
        diameter_sdev=0,
        thickness_sdev=0,
        particle_poisson=True,
        bulk_scatter=True,
        shape_transform=True,
        n_proteins=None,
    ):
        r"""Generic container for a solution of proteins.  Used to produce randomized protein coordinates, solvent
        scatter background, container diffraction amplitudes.

        Args:
            type (str): Either "drop" or "sheet"
            diameter (float): Diameter of sphere or diameter of beam in case of sheet
            molecule (Molecule): A reborn Molecule instance (has protein diameter, molecular weight)
            concentration (float): Concentration in mg/ml
            thickness (float): Thickness of sheet (has no effect on sphere).
            geom (PADGeometryList): Geometry
            beam (Beam): Beam info
            diameter_sdev (float): Standard deviation of diameter
            thickness_sdev (float): Standard deviation of thickness
            particle_poisson (bool): True enables Poisson statistics on particle counts
            bulk_scatter (bool): Turn on bulk solvent scatter
            shape_transform (bool): Turn on shape transform diffraction (drops only)
            n_proteins (int): Set to integer to fix the number of proteins (ignore concentration)
        """
        self.type = type
        self.diameter = diameter
        self.diameter_sdev = diameter_sdev
        self.n_proteins = n_proteins
        self.thickness = thickness
        self.thickness_sdev = thickness_sdev
        self.protein_diameter = molecule.max_atomic_pair_distance
        self.molecular_weight = molecule.get_molecular_weight()
        self.particle_poisson = particle_poisson
        self.bulk_scatter = bulk_scatter
        self.shape_transform = shape_transform
        self.number_density = concentration / self.molecular_weight
        self.f_dens_water = atoms.xraylib_scattering_density(
            "H2O", 1000, beam.photon_energy, approximate=True
        )
        self.q_mags = geom.q_mags(beam)
        waterprof = solutions.water_scattering_factor_squared(q=self.q_mags)
        waterprof *= solutions.water_number_density()
        self.waterprof = waterprof
        self.placer = None

    def rng(self):
        r"""Run this to generate new set of randomized parameters."""
        self.rng_diameter = np.random.normal(
            loc=self.diameter, scale=self.diameter_sdev
        )
        self.rng_thickness = np.random.normal(
            loc=self.thickness, scale=self.diameter_sdev
        )
        if self.type == "drop":
            self.rng_volume = (4 / 3.0) * np.pi * (self.rng_diameter / 2) ** 3
        elif self.type == "sheet":
            self.rng_volume = np.pi * (self.rng_diameter / 2) ** 2 * self.rng_thickness
        self.rng_n_particles = self.number_density * self.rng_volume
        if self.n_proteins is not None:
            self.rng_n_particles = self.n_proteins
        if self.particle_poisson:
            self.rng_n_particles = np.random.poisson(self.rng_n_particles)
        self.rng_n_particles = int(self.rng_n_particles)
        if self.type == "drop":
            self.rng_coordinates = placer.particles_in_a_sphere(
                sphere_diameter=self.rng_diameter,
                n_particles=self.rng_n_particles,
                particle_diameter=self.protein_diameter,
            )
        elif self.type == "sheet":
            self.rng_coordinates = placer.particles_in_a_cylinder(
                cylinder_diameter=self.rng_diameter,
                cylinder_length=self.rng_thickness,
                n_particles=self.rng_n_particles,
                particle_diameter=self.protein_diameter,
            )

    def rng_bulk_scatter(self):
        r"""Bulk (water) diffraction intensity.  Needs to be multiplied by r_e^2 J0 SA P"""
        if self.bulk_scatter:
            return self.waterprof * self.rng_volume
        return 0

    def rng_shape_amplitudes(self):
        r"""Shape amplitudes.  Needs to be squared and then multiplied by r_e^2 J0 SA P to get intensities."""
        if self.shape_transform and self.type == "drop":
            return self.f_dens_water * sphere_form_factor(
                radius=self.rng_diameter / 2, q_mags=self.q_mags
            )
        else:
            return 0


class PolarGeometry:
    def __init__(self, q_range, beam, particle_diameter, oversampling, distance=1):
        r"""A pure polar geometry.  Does not derive from a PAD geometry.  Attempts to emulate a PADGeometryList
        wherever possible/appropriate."""
        L = particle_diameter
        s = oversampling
        d = 2 * np.pi / q_range[1]
        max_theta = 2 * np.arcsin(beam.wavelength / 2 / d)
        dq = 2 * np.pi / L / s
        dp = d / L / s / np.cos(max_theta / 2)
        n_p = int(np.ceil(2 * np.pi / dp))
        n_q = int(np.ceil((q_range[1] - q_range[0]) / dq))
        dp = 2 * np.pi / n_p
        p_min = dp / 2
        p_max = 2 * np.pi - dp / 2
        dq = (q_range[1] - q_range[0]) / n_q
        q_min = q_range[0] + dq / 2
        q_max = q_range[1] - dq / 2
        q_bin_centers = np.linspace(q_min, q_max, n_q)
        p_bin_centers = np.linspace(p_min, p_max, n_p)
        q_bin_centers, p_bin_centers = np.meshgrid(
            q_bin_centers, p_bin_centers, indexing="ij"
        )
        t_bin_centers = 2 * np.arcsin(beam.wavelength * q_bin_centers / 4 / np.pi)
        pkout_vecs = np.vstack(
            [
                np.sin(t_bin_centers.ravel()) * np.cos(p_bin_centers.ravel()),
                np.sin(t_bin_centers.ravel()) * np.sin(p_bin_centers.ravel()),
                np.cos(t_bin_centers.ravel()),
            ]
        ).T.copy()
        self._q_vecs = 2 * np.pi / beam.wavelength * (pkout_vecs - np.array([0, 0, 1]))
        self._q_mags = utils.vec_mag(self._q_vecs)
        sa = (
            np.sin(t_bin_centers)
            * dp
            * (beam.wavelength / L / s / np.cos(t_bin_centers / 2))
        )
        self.sa = sa.ravel()
        self.svecs = self._q_vecs + (2 * np.pi / beam.wavelength) * beam.beam_vec
        self.pfacs = detector.polarization_factors(beam, self.svecs)
        self.sap = self.sa * self.pfacs
        self.distance = distance
        self.n_q_bins = n_q
        self.n_phi_bins = n_p
        self.q_bin_center_min = q_min
        self.q_bin_center_max = q_max
        self.p_bin_center_min = p_min
        self.p_bin_center_max = p_max
        self.n_pixels = self.n_phi_bins * self.n_q_bins
        self.shape = (self.n_q_bins, self.n_phi_bins)
        self.pt_bin_centers = t_bin_centers
        self.dq = dq
        self.dp = dp

    def reshape(self, dat):
        return np.reshape(dat, self.shape)

    def q_vecs(self, beam):
        return self._q_vecs

    def q_mags(self, beam):
        return self._q_mags

    def solid_angles(self):
        return self.sa

    def polarization_factors(self, beam):
        return self.pfacs

    def s_vecs(self):
        return self.svecs

    # def position_vecs(self):
    #     return self.svecs *


class PolarBinner:
    def __init__(self, beam, pad_geom, pad_mask, polar_geom):
        r"""Helper class that bins PAD data into polar coordinates."""
        self.beam = beam
        self.pad_mask = pad_mask
        self.polar_geom = polar_geom
        self.pad_geom = pad_geom
        self.pad_q_mags = pad_geom.q_mags(beam)
        self.pad_phi = pad_geom.azimuthal_angles(beam)
        self.sap = pad_geom.solid_angles() * pad_geom.polarization_factors(beam)
        self.weights = pad_mask * self.sap

    def bin_data(self, data):
        data = data * self.pad_mask / self.sap
        pgeom = self.polar_geom
        stats = polar.stats(
            data,
            self.pad_q_mags,
            self.pad_phi,
            weights=self.weights,
            n_q_bins=pgeom.n_q_bins,
            q_min=pgeom.q_bin_center_min,
            q_max=pgeom.q_bin_center_max,
            n_p_bins=pgeom.n_phi_bins,
            p_min=pgeom.p_bin_center_min,
            p_max=pgeom.p_bin_center_max,
            sum_=None,
            sum2=None,
            w_sum=None,
        )
        return stats


class Getter(FrameGetter):
    n_particles = 0

    def __init__(
        self,
        dmap,
        geom,
        beam,
        mask,
        container,
        gas,
        incoherent=False,
        poisson_noise=False,
        n_frames=1e6,
        verbose=False,
    ):
        r"""This is the simulation engine.  It is supposed to be generalized for both PADGeometry and for
        PolarGeometry detectors."""
        super().__init__()
        q_vecs = geom.q_vecs(beam)
        q_mags = geom.q_mags(beam)
        pf = geom.polarization_factors(beam)
        sa = geom.solid_angles()
        sap = sa * pf
        jre2 = r_e**2 * beam.photon_number_fluence
        self.n_frames = int(n_frames)
        self.dmap = dmap
        self.geom = geom
        self.beam = beam
        self.mask = mask
        self.container = container
        self.q_vecs = q_vecs
        self.q_mags = q_mags
        self.sap = sap
        self.jre2 = jre2
        self.gas = gas
        self.incoherent = incoherent
        self.poisson_noise = poisson_noise
        self.verbose = verbose

    def get_data(self, frame_number=0):
        intensities = self.get_intensities(frame_number=frame_number)
        df = dataframe.DataFrame(
            pad_geometry=self.geom, beam=self.beam, raw_data=intensities, mask=self.mask
        )
        return df

    def get_intensities(self, frame_number=0):
        t = time()
        # print(f"Simulating frame {frame_number}")
        np.random.seed(int(frame_number))
        dmap = self.dmap
        q_vecs = self.q_vecs
        f2p = self.jre2 * self.sap
        self.container.rng()
        n_particles = self.container.rng_n_particles
        p_vecs = self.container.rng_coordinates
        amps = 0
        ints = 0
        self.n_particles = n_particles
        for p in range(n_particles):
            R = Rotation.random().as_matrix()
            U = p_vecs[p, :]
            q = np.dot(q_vecs, R)
            a = trilinear_interpolation(dmap.F, q, x_min=dmap.q_min, x_max=dmap.q_max)
            if self.incoherent:
                ints += np.abs(a) ** 2
            else:
                a *= np.exp(1j * (np.dot(q_vecs, U)))
                amps += a
            # print(f"Particle {p}", end="\r")
        # print("")
        if self.incoherent:
            # TODO: add shape intensities
            intensities = ints
        else:
            # amps += container.rng_shape_amplitudes()
            intensities = np.abs(amps) ** 2
        # print("Simulated protein amplitudes in", time() - t, "seconds")
        intensities += self.container.rng_bulk_scatter()
        intensities += self.gas
        intensities *= f2p
        if self.poisson_noise:
            intensities = np.random.poisson(intensities).astype(float)
        return intensities



def qtview(data, pgeom, l):
    imview(
        data.T,
        ss_lims=[pgeom.p_bin_center_min, pgeom.p_bin_center_max],
        fs_lims=[pgeom.q_bin_center_min, pgeom.q_bin_center_max],
        ss_label="phi",
        fs_label="q",
        levels=[-l, l],
        hold=True,
        gradient="bipolar",
    )


def mpview(data, pgeom, title):
    qs = np.linspace(pgeom.q_bin_center_min, pgeom.q_bin_center_max, pgeom.n_q_bins)
    ys = np.arange(0, pgeom.n_q_bins, 5)
    n_phi = pgeom.n_phi_bins
    plt.figure()
    plt.title(title)
    plt.imshow(data, cmap='RdYlBu', interpolation=None)
    plt.ylabel(r'q = 2 $\pi$ sin $\Theta$ / $\lambda$  [$\AA^{-1}$]')
    plt.yticks(ys, np.round(qs[ys] * 1e-10, 5))
    plt.xlabel(r'$\phi$')
    plt.xticks([0, int(n_phi/4), int(pgeom.n_phi_bins / 2), int(2 * n_phi / 3), n_phi],
               ['0', r'$\pi$/2', r'$\pi$', r'3$\pi$/2', r'2$\pi$'])
    plt.colorbar()
    plt.gca().set_aspect('auto')
    plt.tight_layout()
    plt.show()


# def view_correlations(results):
#     r"""Quick viewer for correlations."""
#     conf = results["config"]
#     acf = results["acf"].copy()
#     weights = results["acf_weights"].copy()
#     pgeom = results["polar_geom"]
#     acf[weights > 0] /= weights[weights > 0]
#     acf = (acf.T - np.mean(acf, axis=1)).T
#     l = conf["n_proteins"] * 1e5
#     levels = [-l, l]
#     imview(
#         acf.T,
#         ss_lims=[pgeom.p_bin_center_min, pgeom.p_bin_center_max],
#         fs_lims=[pgeom.q_bin_center_min, pgeom.q_bin_center_max],
#         ss_label="phi",
#         fs_label="q",
#         levels=levels,
#         hold=True,
#         gradient="bipolar",
#     )

def view_correlations(results, qt=False):
    r"""
    Quick viewer for correlations.
    This will show the corrected auto-correlations (normalized)
    """
    # n = results["n_proteins"]
    pgeom = results["polar_geom"]
    acf = results["acf"].copy()
    # plot
    if qt:
        qtview(data=acf, pgeom=pgeom, l=n * 1e5)
    else:
        mpview(data=np.log(acf / acf.max() + 1),
               pgeom=pgeom,
               title=f" Particle Angular Auto-Correlations")


def view_correlations_differences(results1, results2, qt=False):
    pgeom = results1["polar_geom"]
    # n = results2["config"]["n_proteins"]
    acf1 = results1["acf"].copy()
    acf2 = results2["acf"].copy()
    # compute difference
    diff = np.abs(acf1 - acf2)
    # plot
    if qt:
        qtview(data=diff, pgeom=pgeom, l=1e5)
    else:
        mpview(data=np.log(diff + 1),
               pgeom=pgeom,
               title=f"Single Particle Angular Auto-Correlation Difference")


# def view_correlations_errors(results1, results2):
#     pgeom = results1["polar_geom"]
#     # compute errors
#     errors = correlation_errors(results1, results2)
#     # plot
#     n = results2["n_proteins"]
#     qs = np.linspace(pgeom.q_bin_center_min, pgeom.q_bin_center_max, pgeom.n_q_bins)
#     xs = np.arange(0, pgeom.n_q_bins, 5)
#     plt.figure()
#     plt.title(f"1 - {n} Particle RMS Errors")
#     plt.semilogy(errors, '--k')
#     plt.semilogy(errors, '*r')
#     plt.xlabel('q = 2 $\pi$ sin $\Theta$ / $\lambda$  [$\AA^{-1}$]')
#     plt.xticks(xs, np.round(qs[xs] * 1e-10, 5))
#     plt.ylabel('rms errors')
#     plt.gca().set_aspect('auto')
#     plt.tight_layout()
#     plt.show()


def fetch_simulate_sequence(directory):
    r"""View a series of correlations to see how the convergence looks.  Assumes various results
    are in su"""
    # files = sorted(glob.glob(directory + "objects.pkl"))
    # objects = load_pickle(files[0])
    files = sorted(glob.glob(directory + "0*.pkl"))
    frame_number = []
    acf = []
    acf_weights = []
    polar_sum = []
    polar_weights = []
    config = []
    for f in files:
        results = load_pickle(f)
        config.append(results["config"])
        frame_number.append(results["frame_number"])
        acf.append(results["acf"])
        acf_weights.append(results["acf_weights"])
        polar_sum.append(results["polar_sum"])
        polar_weights.append(results["polar_weights"])
    return dict(
        config=config,
        frame_number=frame_number,
        acf=acf,
        acf_weights=acf_weights,
        polar_sum=polar_sum,
        polar_weights=polar_weights,
    )


def pearson_convergence(directory, template=None, view=False):
    r"""Generate Pearson correlations for the results in a directory.  The directory is assumed to have results
    with ACF sums for various numbers of shots.  Pearson correlations are made by correlating C_n(q, dphi), which has
    n shots integrated, with a template C'(q, dphi).  If a "ground truth" template C' does not exist, then the final
    correlation C_N(q, dphi) is used by default.  One Pearson correlation coefficient is calculated for each q ring.
    The phi=0 bin is ignored since it may be a noise spike."""
    files = sorted(glob.glob(directory + "0*.pkl"))
    results = []
    for f in files:
        results.append(load_pickle(f))
    acfs = []
    frames = []
    for r in results:
        acf = r["acf"]
        weights = r["acf_weights"]
        acf[weights > 0] /= weights[weights > 0]
        # Remove phi=0 noise spike
        acf = acf[1:]
        # Subtract average along phi
        acf = (acf.T - np.mean(acf, axis=1)).T
        acfs.append(acf)
        frames.append(r["frame_number"])
    # The template to compare against others is the last ACF
    t = template
    if t is None:
        t = acfs[-1]
    ps = []
    for a in acfs:
        at = np.sum(a * t, axis=1)
        aa = np.sum(a * a, axis=1)
        tt = np.sum(t * t, axis=1)
        sraatt = np.sqrt(aa * tt)
        p = np.zeros_like(at)
        ind = sraatt > 0
        p[ind] = at[ind] / sraatt[ind]
        ps.append(p)
    ps = np.array(ps)
    objects = load_pickle(directory + "/objects.pkl")
    g = objects["polar_geom"]
    qs = np.linspace(g.q_bin_center_min, g.q_bin_center_max, g.n_q_bins)
    if view:
        app = pg.mkQApp()
        pg.image(ps)
        app.exec_()
    results = dict(pearson=ps, template=t, n_frames=np.array(frames), acfs=acfs, qs=qs)
    return results


def view_simulate_results(directory, key):
    r"""Quickly view results from simulate().  Should be able to show any of the output dictionary keys.
    Attempts to aggregate results from all the checkpoints in the save_directory."""
    files = sorted(glob.glob(directory + "0*.pkl"))
    results = []
    for f in files:
        results.append(load_pickle(f))
    stack = []
    for r in results:
        stack.append(r[key])
    pg.image(np.array(stack))
    pg.mkQApp().exec_()


def setup_simulate(conf):
    r"""Setup all the class instances needed to simulate FXS signals."""

    def debug_msg(*args, **kwargs):
        if conf["debug"]:
            print(*args, **kwargs, flush=True)

    validate_config(conf)
    debug_msg("Setting up beam...")
    beam = source.Beam(
        photon_energy=conf["photon_energy"],
        diameter_fwhm=conf["beam_diameter"],
        pulse_energy=conf["pulse_energy"],
    )
    debug_msg("Setting up geometry...")
    pad_geom = detector.load_pad_geometry_list(conf["pad_geometry_file"])
    pad_geom = pad_geom.binned(conf["pad_binning"])
    if conf["pad_translation"] is not None:
        pad_geom.translate(conf["pad_translation"])
    debug_msg("Setting up molecule...")
    molecule = crystal.CrystalStructure(
        conf["pdb_file"],
        create_bio_assembly=conf["create_bio_assembly"],
    ).molecule
    debug_msg("Setting up density map...")
    dmap = DensityMap(
        pdb_file=conf["pdb_file"],
        resolution=conf["map_resolution"],
        oversampling=conf["map_oversample"],
        solvent_contrast=conf["solvent_contrast"],
        bio_assembly=conf["create_bio_assembly"],
        joblib_memory=memory,
    )
    debug_msg("Setting up polar geometry...")
    polar_geom = PolarGeometry(
        q_range=[0, 2 * np.pi / conf["fxs_ring_resolution"]],
        beam=beam,
        particle_diameter=molecule.max_atomic_pair_distance,
        oversampling=conf["fxs_ring_oversampling"],
        distance=pad_geom.average_detector_distance(beam),
    )
    # Setups after this are specific to polar and pad
    polar_mask = np.ones(polar_geom.n_pixels)
    pad_mask = pad_geom.beamstop_mask(beam=beam, min_radius=conf["beamstop_size"])
    cargs = dict(
        type=conf["container_type"],
        diameter=conf["container_diameter"],
        molecule=molecule,
        concentration=conf["protein_concentration"],
        beam=beam,
        thickness=conf["container_thickness"],
        diameter_sdev=conf["container_diameter_sdev"],
        thickness_sdev=conf["container_thickness_sdev"],
        n_proteins=conf["n_proteins"],
        shape_transform=conf["container_shape_transform"],
        particle_poisson=conf["particle_poisson"],
        bulk_scatter=conf["container_bulk_scatter"],
    )
    polar_container = SolutionContainer(geom=polar_geom, **cargs)
    pad_container = SolutionContainer(geom=pad_geom, **cargs)
    pad_container.rng()
    if conf["gas_background"]:
        debug_msg("Simulating gas background...")
        get_gas_background = memory.cache(gas.get_gas_background)
        pad_gasbak = get_gas_background(pad_geom, beam, **conf["gas_params"])
        pad_gasbak /= pad_geom.f2phot(beam)
        print(
            "Warning: gas background is not implemented for polar simulations yet!!!!"
        )
        # polar_gasbak = gas.get_gas_background(polar_geom, beam, **conf["gas_params"])
        # polar_gasbak /= polar_geom.f2phot(beam)
        polar_gasbak = 0
    else:
        pad_gasbak = 0
        polar_gasbak = 0
    gargs = dict(
        dmap=dmap,
        beam=beam,
        incoherent=conf["incoherent"],
        n_frames=conf["fxs_n_shots"],
        poisson_noise=conf["poisson"],
    )
    polar_framegetter = Getter(
        geom=polar_geom,
        mask=polar_mask,
        container=polar_container,
        gas=polar_gasbak,
        **gargs,
    )
    pad_framegetter = Getter(
        geom=pad_geom, mask=pad_mask, container=pad_container, gas=pad_gasbak, **gargs
    )
    polar_binner = PolarBinner(
        beam=beam, pad_geom=pad_geom, pad_mask=pad_mask, polar_geom=polar_geom
    )
    objects = dict(
        conf=conf,
        beam=beam,
        pad_geom=pad_geom,
        molecule=molecule,
        dmap=dmap,
        polar_geom=polar_geom,
        polar_mask=polar_mask,
        polar_container=polar_container,
        pad_container=pad_container,
        polar_gasbak=polar_gasbak,
        pad_gasbak=pad_gasbak,
        pad_framegetter=pad_framegetter,
        polar_framegetter=polar_framegetter,
        polar_binner=polar_binner,
    )
    return objects


def simulate(conf, reload=False, fast_forward=False):
    r"""Simulate diffraction intensites on either a PAD (followed by polar binning) or directly
    on an ideal polar detector (faster than the PAD method).  See default_config() for options.
    Sorry, no documentation yet...

    Args:
        conf (dict): Configuration dictionary (see default_config())
        reload (bool): If True, attempt to reload final results from previous run (default: False)
        fast_forward (bool): If True, attempt to start from partially complete run (default: False)
    """
    if reload:
        return simulate_reload(conf, rerun=True)

    def debug_msg(*args, **kwargs):
        if conf["debug"]:
            print(*args, **kwargs, flush=True)

    objects = setup_simulate(conf)
    polar_geom = objects["polar_geom"]
    polar_framegetter = objects["polar_framegetter"]
    pad_framegetter = objects["pad_framegetter"]
    polar_binner = objects["polar_binner"]
    os.makedirs(conf["save_directory"], exist_ok=True)
    save_pickle(conf, conf["save_directory"] + "/config.pkl")
    save_pickle(objects, conf["save_directory"] + "/objects.pkl")
    savefiles = sorted(glob.glob(f"results/hemoglobin7/0.5mJ/water/{'[0-9]'*8}.pkl"))
    n_frames = conf["fxs_n_shots"]
    # Attempt to fast-forward if files exist from unfinished job
    if fast_forward and savefiles:
        savefile = savefiles[-1]
        dat = load_pickle(savefile)
        frame_numbers = range(dat["frame_number"] + 1, conf["fxs_n_shots"])
        acf = dat["acf"]
        acf_weights = dat["acf_weights"]
        print(f"Fast forward to frame {dat['frame_number']}")
        del dat
    else:
        range(conf["fxs_n_shots"])
        acf = 0
        acf_weights = 0
        frame_numbers = range(0, conf["fxs_n_shots"])
    savepoints = [i for i in conf["save_points"]]
    savepoints.append(conf["fxs_n_shots"])
    n_particles = 0
    t0 = time()
    counter = 0
    for i in frame_numbers:
        counter += 1
        # Print status update
        if counter in [1, 10, 100] or i % 100 == 0:
            frames_remaining = n_frames - i
            frame_time = (time() - t0) / counter
            time_remaining = frame_time * frames_remaining
            h = int(np.floor(time_remaining / 3600))
            m = int(np.floor((time_remaining - h * 3600) / 60))
            s = time_remaining - h * 3600 - m * 60
            print(
                f"Frame {i} of {n_frames} ({n_particles} particles) ({h:02d}:{m:02d}:{s:04.1f} remaining)"
            )
        if conf["fxs_polar_detector"]:
            polar_sum = polar_geom.reshape(polar_framegetter.get_intensities(i))
            polar_weights = polar_geom.reshape(polar_geom.sap)
            n_particles = polar_framegetter.n_particles
        else:
            intensities = pad_framegetter.get_intensities(i)
            stats = polar_binner.bin_data(intensities)
            polar_sum = stats["sum"]
            polar_weights = stats["weight_sum"]
            n_particles = pad_framegetter.n_particles
        acf += np.real(np.fft.ifft(np.abs(np.fft.fft(polar_sum)) ** 2))
        acf_weights += np.real(np.fft.ifft(np.abs(np.fft.fft(polar_weights)) ** 2))
        if i + 1 in savepoints:
            savefile = conf["save_directory"] + f"/{i+1:08d}.pkl"
            debug_msg("Saving", savefile)
            results = dict(
                config=conf,
                frame_number=i,
                acf=acf,
                acf_weights=acf_weights,
                polar_sum=polar_sum,
                polar_weights=polar_weights,
            )
            if not conf["fxs_polar_detector"]:
                results["intensities"] = intensities
            save_pickle(
                results,
                savefile,
            )
    objects["config"] = conf
    objects["acf"] = acf
    objects["acf_weights"] = acf_weights
    objects["polar_sum"] = polar_sum
    acf_corrected = acf.copy()
    inds = acf_weights > 0
    acf_corrected[inds] /= acf_weights[inds]
    objects["acf_corrected"] = acf_corrected
    savefile = conf["save_directory"] + "/final.pkl"
    debug_msg("Saving", savefile)
    save_pickle(objects, savefile)
    return objects


def simulate_reload(conf, rerun=False):
    r"""Reload results from previous simulate() run.  Optionally, re-run simulate().
    A reload of prior results will happen only if the config dictionary is identical to the
    saved dictionary.  Obviously, there is no check for changes in the simulate() function,
    and a single key change will cause a re-run."""
    savefile = conf["save_directory"] + "/final.pkl"
    if os.path.exists(savefile):
        print("Found file", savefile)
        print("Check for matching configurations...")
        results = load_pickle(savefile)
        same = compare_configs(conf, results["config"])
        if same:
            print("Values in config dictionaries match.")
            print("Returning saved results.")
            return results
        else:
            print("Mis-matched config dictionaries...")
    if rerun:
        print("Recalculating...")
        return simulate(conf)
