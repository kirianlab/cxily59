import numpy as np
import matplotlib.pyplot as plt
from reborn.external.pyqtgraph import imview
import simtools
reload = 1
view = 0
pulse_energy = 1e-3
savedir = f"results/hemoglobin7/{pulse_energy * 1e3}mJ/"
conf = simtools.default_config()
conf["n_proteins"] = 1
conf["fxs_polar_detector"] = True
conf["fxs_n_shots"] = 1000
conf["poisson"] = False
conf["save_directory"] = savedir + "/ground_truth/"
results = simtools.simulate(conf, reload=reload)
acfs1 = results["acf_corrected"]
del results
pearson1 = simtools.pearson_convergence(conf["save_directory"], view=view)
template = pearson1["template"]
conf["pulse_energy"] = pulse_energy
conf["beam_diameter"] = 100e-9
conf["container_thickness"] = 100e-9
conf["container_diameter"] = 100e-9
conf["container_bulk_scatter"] = True
conf["gas_background"] = True
conf["poisson"] = True
conf["fxs_n_shots"] = 1000000
conf["n_proteins"] = None
conf["save_directory"] = savedir + f"/water/"
results = simtools.simulate(conf, reload=reload)
acfs = results["acf_corrected"]
g = results["polar_geom"]
qs = np.linspace(g.q_bin_center_min, g.q_bin_center_max, g.n_q_bins)
phis = np.linspace(g.p_bin_center_min, g.p_bin_center_max, g.n_phi_bins)
results["pad_framegetter"].view()
del results
out = simtools.pearson_convergence(conf["save_directory"], template=template, view=0)
n_frames = out["n_frames"]
pearson = out["pearson"]
# simtools.view_correlations_series(conf["save_directory"])
# objects = load_pickle(conf["save_directory"] + "/objects.pkl")
qi = 5
q = qs[qi]
plt.figure()
plt.plot(n_frames, pearson[:, qi])
plt.title(f"Hemoglobin (q={q*1e-10:0.3f}/A)")
plt.xlabel("Number of patterns")
plt.ylabel("Pearson correlation")
plt.figure()
a1 = acfs1[qi, 1:]
a1 -= np.mean(a1)
a2 = acfs[qi, 1:]
a2 -= np.mean(a2)
p = phis[1:] * 180 / np.pi
c = np.sum(a1*a2)/np.sum(a2*a2)
plt.plot(p, c*a2, ".")
plt.plot(p, a1)
plt.xlabel("dphi (deg)")
plt.ylabel("C(q, q, dphi")
plt.show()
sequence = simtools.fetch_simulate_sequence(conf["save_directory"])
acf = np.array(sequence["acf"])
acf_weights = np.array(sequence["acf_weights"])
inds = acf_weights > 0
acf[inds] /= acf_weights[inds]
acf = acf[:, :, 1:]
meen = np.mean(acf, axis=2)
std = np.std(acf, axis=2)
at = acf.transpose([2, 0, 1])
at = at - meen
at = at / std
acf = at.transpose([1, 0, 2])
lev = 2
imview(
    acf,
    fs_lims=[qs[0] * 1e-10, qs[-1] * 1e-10],
    ss_lims=[phis[1], phis[-1]],
    fs_label="q/A",
    ss_label="dphi",
    hold=True,
    levels=[-lev, lev],
    gradient="bipolar"
)
