# Confirm that both PAD and polar detector simulations give the same result
import sys
import numpy as np
import pyqtgraph as pg
import simtools
reload = True
n_shots = 10000
conf = simtools.default_config()
conf["save_directory"] = "results/hemoglobin4/pad/"
conf["fxs_n_shots"] = n_shots
conf["map_oversample"] = 6
# First a PAD simulation
pad_results = simtools.simulate(conf, reload=reload)
pad_acf = pad_results["acf_corrected"]
pad_acf = (pad_acf.T - np.mean(pad_acf, axis=1)).T


pad_dat = pad_results["polar_sum"]
simtools.mpview(data=pad_dat,
                pgeom=pad_results["polar_geom"],
                title="Single Particle Fluctuation Intensity (PAD)")

simtools.view_correlations(pad_results, qt=False)
# del pad_results
# Next a polar simulation (no gaps, uniform solid angles, etc)
conf = simtools.default_config()
conf["save_directory"] = "results/hemoglobin4/polar/"
conf["fxs_n_shots"] = n_shots
conf["fxs_polar_detector"] = True
polar_results = simtools.simulate(conf, reload=reload)
polar_acf = polar_results["acf_corrected"]
polar_acf = (polar_acf.T - np.mean(polar_acf, axis=1)).T
simtools.view_correlations(polar_results, qt=False)
pad_dat = polar_results["polar_sum"]
simtools.mpview(data=pad_dat,
                pgeom=pad_results["polar_geom"],
                title="Single Particle Fluctuation Intensity (Polar)")
# del polar_results


simtools.view_correlations_differences(polar_results, pad_results)

# diff = np.abs(polar_acf - pad_acf)
# errors = np.sqrt(2*np.sum((polar_acf - pad_acf)**2, axis=1)/np.sum((polar_acf + pad_acf)**2, axis=1))
# pg.plot(errors)
# pg.image(diff)
# pg.mkQApp().exec_()
