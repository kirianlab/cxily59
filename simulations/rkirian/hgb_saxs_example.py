import numpy as np
import matplotlib.pyplot as plt
from reborn.external.denss import get_scattering_profile
q_mags = np.linspace(0, np.pi/1e-9, 1000)
_, profile = get_scattering_profile(pdb_file='2qsp.pdb', q_mags=q_mags)
plt.semilogy(q_mags/1e10, profile)
plt.xlabel('q [A]')
plt.ylabel('I(q)')
plt.show()