import h5py
import numpy as np
import matplotlib.pyplot as plt
import pyqtgraph as pg
import os
import sys
from time import time
from numpy.fft import fftn, ifftn, fftshift, ifftshift
from scipy.spatial.transform import Rotation
import reborn
import reborn.utils
from reborn import const
from reborn.analysis.fluctuations import data_correlation
from reborn.detector import PADGeometryList
from reborn.detector import load_pad_masks
from reborn.detector import PolarPADAssembler
from reborn.external import hdf5
from reborn.external.denss import get_crystal_structure
from reborn.external.denss import create_density_map
from reborn.misc.interpolate import trilinear_interpolation
from reborn.target import crystal
from reborn.target import placer
from reborn.simulate import gas
from reborn.simulate import solutions
from reborn.source import Beam
from reborn.viewers.qtviews import PADView
from saxstats import saxstats


# configurations
beam_config = {"photon energy": 7 * 1e3 * const.eV,  # All units are SI
               "pulse energy": 0.17 * 1e-3,  # mJ (0.5 mJ in experiment)
               "diameter": 100e-9,  # beam in experiment was closer to 300 nm
               "polarization": np.array((0, 1, 0))}  # linear polarization along y (vertical)
det_config = {"pad id":"jungfrau4M",
              "geometry": "../geometry/recentered_distance_refined_AgBe_run15.json",
              "data type": "calib",
              "mask": "../geometry/dead_pixels.mask",
              "distance": 0.0,  # 0.5,
              "binning": None}  # For speed, bin the pixels
# sample_config = {"name": "PSI Trimer",  # Naming for plots
#                  "pdb": "1jb0",
#                  "concentration": 10,  # Protein concentration in mg/ml = kg/m^3
#                  "diameter": 200e-10}
sample_config = {"name": "Hemoglobin",  # Naming for plots
                 "pdb": "2qsp",  # full molecule
                 "concentration": 10,  # Protein concentration in mg/ml = kg/m^3
                 "diameter": 110e-10}
dmap_config = {"resolution": 0.5e-9,  # Minimum resolution for 3D density map.
               "oversample": 3,  # Oversampling factor for 3D density map.
               "ignore waters": False,
               "bioassembly": True,  # Expand atomic coordinates (e.g. virus structure)
               "solvent contrast": True}  # solvent contrast (water)
gas_config = {"path length": [0, None],
              "type": "he",
              "pressure": 0.03,
              "n simulation steps": 5,
              "temperature": 293}
fxs_config = {"resolution": dmap_config["resolution"] / 2,  # Autocorrelation ring resolution
              "oversample": dmap_config["oversample"],
              "n shots": int(1e6)}
injector_conf = {"sheet thickness": 100e-9}
simulation_config = {"poisson": True,
                     "gas": True,
                     "protein": True,  # protein contrast in diffraction amplitudes
                     "one particle": False,  # Fix particle counts to one per drop
                     "sheet": True,  # Include sheet background
                     "bulk water": True,  # bulk water background
                     "use cached files": True,  # Cache files for speed
                     "random_seed": 2023}  # random number generator seed (use None to make it random)}
configs = {"beam": beam_config,
           "detector": det_config,
           "sample": sample_config,
           "density map": dmap_config,
           "gas": gas_config,
           "fxs": fxs_config,
           "injector": injector_conf,
           "simulation": simulation_config}
view_density_projections = False
# Fetch files
# h5file = f"{sample_config['pdb']}_{fxs_config['n shots']}.h5"
h5file = f"ly59_sims_{sample_config['pdb']}"
mrcfile = f"{sample_config['pdb']}.mrc"
if not os.path.exists(sample_config["pdb"]):
    print("Fetching PDB file.")
    sample_config["pdb"] = crystal.get_pdb_file(sample_config["pdb"],
                                                save_path=".",
                                                use_cache=False)
# Make reproducible random numbers
if simulation_config["random_seed"] is not None:
    np.random.seed(simulation_config["random_seed"])
print("*" * 85)
print("INPUT PARAMETERS")
for k, v in configs.items():
    print(f"\t{k}")
    for sk, sv in v.items():
        print(f"\t\t{sk}: {sv}")
print("*" * 80)
# setup experimental parameters
print("loading the beam")
beam = Beam(photon_energy=beam_config["photon energy"],
            pulse_energy=beam_config["pulse energy"],
            diameter_fwhm=beam_config["diameter"],
            polarization_vec=beam_config["polarization"])
hdf5.save_beam_as_h5(h5_file_path=f"{h5file}.h5", beam=beam)
print("loading the detector geometry")
geom = PADGeometryList(filepath=det_config["geometry"])
base_mask = load_pad_masks(file_name=det_config["mask"])
mask = geom.concat_data(np.array(base_mask))
if det_config["binning"] is not None:
    base_geom = geom.copy()
    geom = base_geom.binned(binning=det_config["binning"])
    # bin mask to match binned detector
    binned_mask = tuple(p.zeros() for p in geom)
    for p, m, bp, bm in zip(base_geom, base_mask, geom, binned_mask):
        bm += m.reshape(bp.n_fs, int(p.n_fs / bp.n_fs), bp.n_ss, int(p.n_ss / bp.n_ss)).mean(3).mean(1)
    mask = geom.concat_data(np.array(binned_mask))
hdf5.save_pad_geometry_as_h5(h5_file_path=f"{h5file}.h5", pad_geometry=geom)
f2p = geom.f2phot(beam=beam)
qvs = geom.q_vecs(beam=beam)
qms = geom.q_mags(beam=beam)
sheet_volume = np.pi * injector_conf["sheet thickness"] * (beam_config["diameter"] / 2) ** 2
# setup background signals
print("simulating water background")
h2obak = solutions.water_scattering_factor_squared(q=qms)
h2obak *= f2p * solutions.water_number_density() * sheet_volume
# assume isotropic gas background extends 10 mm in each direction from injector
print("simulating gas background")
gasbak = gas.get_gas_background(pad_geometry=geom,
                                beam=beam,
                                path_length=gas_config["path length"],
                                gas_type=gas_config["type"],
                                temperature=gas_config["temperature"],
                                pressure=gas_config["pressure"],
                                n_simulation_steps=gas_config["n simulation steps"],
                                poisson=False,
                                verbose=True,
                                he_percent=1.0,
                                air_percent=0.0,
                                water_percent=0.0)
# setup density map
print("setting up density map")
cryst = get_crystal_structure(pdb_file=sample_config["pdb"],
                              cell=sample_config["diameter"],
                              create_bio_assembly=dmap_config["bioassembly"])
protein_diameter = cryst.molecule.get_size()  # Nominal particle size
f = cryst.molecule.get_scattering_factors(beam=beam)
dmap = crystal.CrystalDensityMap(cryst=cryst,
                                 resolution=dmap_config["resolution"],
                                 oversampling=dmap_config["oversample"])
if not os.path.exists(mrcfile):
    rho, side, extra_info = create_density_map(
        pdb_file=sample_config["pdb"],
        solvent_contrast=dmap_config["solvent contrast"],
        create_bio_assembly=dmap_config["bioassembly"],
        map_resolution=dmap_config["resolution"],
        map_oversampling=dmap_config["oversample"],
        ignore_waters=dmap_config["ignore waters"])
    saxstats.write_mrc(rho=rho, side=side, filename=mrcfile)
else:
    rho, side = saxstats.read_mrc(filename=mrcfile, returnABC=False, float64=True)
if view_density_projections:
    pg.image(np.sum(ifftshift(rho.real), axis=2), title='denss')
    pg.mkQApp().exec_()
protein_number_density = (
    sample_config["concentration"] / cryst.molecule.get_molecular_weight()
)
if simulation_config["one particle"]:
    n_proteins = 1
else:
    n_proteins = int(protein_number_density * sheet_volume)
print(f"n_proteins: {n_proteins}")
# Prepare arrays for simulations
F = fftshift(fftn(ifftshift(rho)))
pvs = placer.particles_in_a_cylinder(
        cylinder_diameter=beam_config["diameter"],
        cylinder_length=injector_conf["sheet thickness"],
        n_particles=n_proteins,
        particle_diameter=protein_diameter)
amps = geom.zeros()
# setup analysis
print("setting up polar assembler")
pa = PolarPADAssembler.from_resolution(pad_geometry=geom,
                                       beam=beam,
                                       sample_diameter=protein_diameter,
                                       oversample=fxs_config["oversample"])
pmask = pa.bin_sum(array=mask * f2p, py=False)
acf_sum = np.zeros(pa.polar_shape)
acf_noisy_sum = np.zeros(pa.polar_shape)
acf_sheet_sum = np.zeros(pa.polar_shape)
# simulate
n_shots = fxs_config["n shots"]
powersof2 = set(int(2 ** x) for x in range(1, 24))
print(f"begining to simulate {n_shots} patterns")
t = time()
for i in range(n_shots):
    if (i + 1) in powersof2:
        doprint = True
        print(f"Shot {i+1} @ {time()-t:06f} seconds.", end="\r")
    else:
        print(f"Shot {i+1}", end="\r")
    if simulation_config["protein"]:
        amps[:] = 0 # + 0j
        rotations = Rotation.random(n_proteins).as_matrix()
        for p, R in enumerate(rotations):
            U = pvs[p, :]
            q = np.dot(qvs, R)
            a = trilinear_interpolation(densities=F.T,
                                        vectors=qvs,
                                        x_min=dmap.q_min,
                                        x_max=dmap.q_max)
            # a *= np.exp(-1j * (np.dot(q, U.T)))
            amps += np.abs(a) ** 2  # add amplitude incoherently
    intensities = f2p * amps
    ints = intensities
    ints_noisy = np.random.poisson(intensities).astype(np.float64)
    if simulation_config["bulk water"]:
        intensities += h2obak
    if simulation_config["gas"]:
        intensities += gasbak
    ints_sheet = np.random.poisson(intensities).astype(np.float64)
    pv = PADView(pad_geometry=geom, beam=beam, data=ints)
    pv1 = PADView(pad_geometry=geom, beam=beam, data=ints_sheet)
    pv.show()
    pv1.start()
    break
    pints = pa.bin_sum(array=ints, py=False)
    pints_noisy = pa.bin_sum(array=ints_noisy, py=False)
    pints_sheet = pa.bin_sum(array=ints_sheet, py=False)
    acf_sum += data_correlation(n=0,
                                data=pints,
                                mask=pmask,
                                cached=False)
    acf_noisy_sum += data_correlation(n=0,
                                      data=pints_noisy,
                                      mask=pmask,
                                      cached=False)
    acf_sheet_sum += data_correlation(n=0,
                                      data=pints_sheet,
                                      mask=pmask,
                                      cached=False)
    if (i + 1) in powersof2:  # save every 2^n frames
        with h5py.File(f"{h5file}_{i+1}.h5", "a") as hf:
            hf.create_dataset(f"acf_sum", data=acf_sum)
            hf.create_dataset(f"acf_noisy_sum", data=acf_noisy_sum)
            hf.create_dataset(f"acf_sheet_sum", data=acf_sheet_sum)

# look at results
# a = 2
# b = int(n_phi - a)
# p = phi[a:b] * 180 / np.pi

# plt.figure()
# plt.plot(p, I_ring[a:b], "--k", label="True")
# plt.plot(p, I_ring_noisy[a:b], ".g", label="Theoretical")
# plt.plot(p, I_ring_sheet[a:b], "xr", label="Background")
# plt.title(f'{name} ({res*1e10} $\AA$; {n_shots} shots)')
# plt.xlabel(r"$\Delta \phi$ (degrees)")
# plt.ylabel(r"$C(q, \Delta\phi)$")
# plt.legend()
# plt.show()

# # normalize the correlations
# acfs =  acf_sum[a:b] / acf_sum[a:b].max()
# acfsn = acf_sum_noisy[a:b] / acf_sum[a:b].max()
# acfss = acf_sum_sheet[a:b] / acf_sum[a:b].max()
# # compute errors
# msen = ((acfs - acfsn) ** 2).mean()
# mses = ((acfs - acfss) ** 2).mean()
# plt.figure()
# plt.plot(p, acfs, "--k", label="True")
# # set the noisy mean to match noise-free mean
# # acf_sum_noisy += np.mean(acf_sum[a:b]) - np.mean(acf_sum_noisy[a:b])
# plt.plot(p, acfsn, ".g", label="Theoretical")
# # acf_sum_sheet += np.mean(acf_sum[a:b]) - np.mean(acf_sum_sheet[a:b])
# plt.plot(p, acfss, "xr", label="Background")
# plt.title(f'{name} ({res*1e10} $\AA$; {n_shots} shots)')
# plt.xlabel(r"$\Delta \phi$ (degrees)")
# plt.ylabel(r"$C(q, \Delta\phi)$")
# plt.legend()
# plt.show()

# print(msen)
# print(mses)

# print("Done")
