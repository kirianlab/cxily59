import numpy as np
import scipy
from reborn import source, detector
from reborn.viewers.qtviews.padviews import PADView
from reborn.const import eV
geom_file = "./geometry/recentered_distance_refined_AgBe_run15.json"
phi = -0*np.pi/180
theta = 45*np.pi/180
diam = 100e-9#250e-9
geom = detector.load_pad_geometry_list(geom_file)
beam = source.Beam(photon_energy=7000*eV)
q_vecs = geom.q_vecs(beam)
jet_vec = np.array([0, 1, 0])
Rphi = np.array([[1, 0             , 0            ],
                 [0, np.cos(theta) , np.sin(theta)],
                 [0, -np.sin(theta), np.cos(theta)]])
Rth = np.array([[ np.cos(phi), 0, np.sin(phi)],
                [           0, 1, 0          ],
                [-np.sin(phi), 0, np.cos(phi)]])
R = Rth @ Rphi
jet_vec = R @ jet_vec
q_par = q_vecs @ jet_vec
q_perp = np.sqrt(np.sum(q_vecs**2, 1) - q_par**2) 
pat = scipy.special.j1(q_perp*diam/2)/(q_perp*diam/2)
pat = np.abs(pat)**2
#pat[np.abs(q_par) > 0.003e10] = 0
pat *= 10/np.max(pat)
#pat += np.random.poisson(np.ones(pat.shape)*0.03)
pv = PADView(data=pat, pad_geometry=geom, beam=beam, levels=[0, np.max(pat)])
pv.start()
