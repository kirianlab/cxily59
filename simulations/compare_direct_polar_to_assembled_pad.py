import h5py
import numpy as np
import matplotlib.pyplot as plt
import pyqtgraph as pg
import os
import sys
from time import time
from numpy.fft import fftn, ifftn, fftshift, ifftshift
from scipy.spatial.transform import Rotation
import reborn
import reborn.utils
from reborn import const
from reborn.analysis import fluctuations
from reborn.analysis.fluctuations import data_correlation
from reborn.detector import PADGeometryList
from reborn.detector import load_pad_masks
from reborn.detector import PolarPADAssembler
from reborn.external import hdf5
from reborn.external.denss import get_crystal_structure
from reborn.external.denss import create_density_map
from reborn.misc.interpolate import trilinear_interpolation
from reborn.target import crystal
from reborn.target import placer
from reborn.simulate import gas
from reborn.simulate import solutions
from reborn.source import Beam
from reborn.viewers.qtviews import PADView
from saxstats import saxstats


# configurations
beam_config = {"photon energy": 7 * 1e3 * const.eV,  # All units are SI
               "pulse energy": 0.17 * 1e-3,  # mJ (0.5 mJ in experiment)
               "diameter": 100e-9,  # beam in experiment was closer to 300 nm
               "polarization": np.array((0, 1, 0))}  # linear polarization along y (vertical)
det_config = {"pad id":"jungfrau4M",
              "geometry": "../geometry/recentered_distance_refined_AgBe_run15.json",
              "data type": "calib",
              "mask": "../geometry/dead_pixels.mask",
              "distance": 0.0,  # 0.5,
              "binning": None}  # For speed, bin the pixels
sample_config = {"name": "Hemoglobin",  # Naming for plots
                 "pdb": "2qsp",  # full molecule
                 "concentration": 10,  # Protein concentration in mg/ml = kg/m^3
                 "diameter": 110e-10}
dmap_config = {"resolution": 0.5e-9,  # Minimum resolution for 3D density map.
               "oversample": 3,  # Oversampling factor for 3D density map.
               "ignore waters": False,
               "bioassembly": True,  # Expand atomic coordinates (e.g. virus structure)
               "solvent contrast": False}  # solvent contrast (water)
fxs_config = {"resolution": dmap_config["resolution"] / 2,  # Autocorrelation ring resolution
              "oversample": dmap_config["oversample"],
              "n shots": int(2 ** 12)}
injector_config = {"sheet thickness": 200e-9}
simulation_config = {"poisson": False,
                     "protein": True,  # protein contrast in diffraction amplitudes
                     "one particle": True,  # Fix particle counts to one per drop
                     "use cached files": True,  # Cache files for speed
                     "random_seed": 2023}  # random number generator seed (use None to make it random)}
configs = {"beam": beam_config,
           "detector": det_config,
           "sample": sample_config,
           "density map": dmap_config,
           "injector": injector_config,
           "fxs": fxs_config,
           "simulation": simulation_config}
view_density_projections = False
# Fetch files
h5file = f"{sample_config['pdb']}_{fxs_config['n shots']}.h5"
mrcfile = f"{sample_config['pdb']}.mrc"
if not os.path.exists(sample_config["pdb"]):
    print("Fetching PDB file.")
    sample_config["pdb"] = crystal.get_pdb_file(sample_config["pdb"],
                                                save_path=".",
                                                use_cache=False)
# Make reproducible random numbers
if simulation_config["random_seed"] is not None:
    np.random.seed(simulation_config["random_seed"])
print("*" * 85)
print("INPUT PARAMETERS")
for k, v in configs.items():
    print(f"\t{k}")
    for sk, sv in v.items():
        print(f"\t\t{sk}: {sv}")
print("*" * 80)
# setup experimental parameters
print("loading the beam")
beam = Beam(photon_energy=beam_config["photon energy"],
            pulse_energy=beam_config["pulse energy"],
            diameter_fwhm=beam_config["diameter"],
            polarization_vec=beam_config["polarization"])
hdf5.save_beam_as_h5(h5_file_path=h5file, beam=beam)
print("loading the detector geometry")
geom = PADGeometryList(filepath=det_config["geometry"])
base_mask = load_pad_masks(file_name=det_config["mask"])
mask = geom.concat_data(np.array(base_mask))
if det_config["binning"] is not None:
    base_geom = geom.copy()
    geom = base_geom.binned(binning=det_config["binning"])
    # bin mask to match binned detector
    binned_mask = tuple(p.zeros() for p in geom)
    for p, m, bp, bm in zip(base_geom, base_mask, geom, binned_mask):
        bm += m.reshape(bp.n_fs, int(p.n_fs / bp.n_fs), bp.n_ss, int(p.n_ss / bp.n_ss)).mean(3).mean(1)
    mask = geom.concat_data(np.array(binned_mask))
hdf5.save_pad_geometry_as_h5(h5_file_path=h5file, pad_geometry=geom)
f2p = geom.f2phot(beam=beam)
qvs = geom.q_vecs(beam=beam)
qms = geom.q_mags(beam=beam)
# setup density map
print("setting up density map")
cryst = get_crystal_structure(pdb_file=sample_config["pdb"],
                              cell=sample_config["diameter"],
                              create_bio_assembly=dmap_config["bioassembly"])
protein_diameter = cryst.molecule.get_size()  # Nominal particle size
f = cryst.molecule.get_scattering_factors(beam=beam)
dmap = crystal.CrystalDensityMap(cryst=cryst,
                                 resolution=dmap_config["resolution"],
                                 oversampling=dmap_config["oversample"])
if not os.path.exists(mrcfile):
    rho, side, extra_info = create_density_map(
        pdb_file=sample_config["pdb"],
        solvent_contrast=dmap_config["solvent contrast"],
        create_bio_assembly=dmap_config["bioassembly"],
        map_resolution=dmap_config["resolution"],
        map_oversampling=dmap_config["oversample"],
        ignore_waters=dmap_config["ignore waters"])
    saxstats.write_mrc(rho=rho, side=side, filename=mrcfile)
else:
    rho, side = saxstats.read_mrc(filename=mrcfile, returnABC=False, float64=True)
if view_density_projections:
    pg.image(np.sum(ifftshift(rho.real), axis=2), title='denss')
    pg.mkQApp().exec_()
protein_number_density = (
    sample_config["concentration"] / cryst.molecule.get_molecular_weight()
)
if simulation_config["one particle"]:
    n_proteins = 1
else:
    n_proteins = int(protein_number_density * sheet_volume)
print(f"n_proteins: {n_proteins}")
# Prepare arrays for simulations
F = fftshift(fftn(ifftshift(rho)))
pvs = placer.particles_in_a_cylinder(
        cylinder_diameter=beam_config["diameter"],
        cylinder_length=injector_config["sheet thickness"],
        n_particles=n_proteins,
        particle_diameter=protein_diameter)
amps = geom.zeros()
# setup analysis
print("setting up polar assembler")
pa = PolarPADAssembler.from_resolution(pad_geometry=geom,
                                       beam=beam,
                                       sample_diameter=protein_diameter,
                                       oversample=fxs_config["oversample"])
pmask = pa.bin_sum(array=mask * f2p, py=False)
n_q, n_p = pa.polar_shape
# setup ideal polar ring
lam = beam.wavelength
res, s, n_shots = fxs_config.values()
d = protein_diameter
qring = 2 * np.pi / res
idx = np.abs(pa.q_bin_centers - qring).argmin()  # find q bin nearest q ring
ring_q = pa.q_bin_centers[idx]
ring_theta = 2 * np.arcsin(ring_q * lam / 4 / np.pi)
ring_dtheta = lam / s / d / np.cos(ring_theta / 2)  # Angular step in theta
ring_dphi = 2 * np.pi / n_p  # Update dphi to integer phis
ring_phi = np.arange(n_p) * ring_dphi
st = np.sin(ring_theta)
ring_sa = st * ring_dphi * ring_dtheta  # ring bin solid angle
ring_pf = 1 - np.sin(ring_phi) ** 2  # ring bin polarization factors
ring_f2p = const.r_e ** 2 * beam.photon_number_fluence * ring_sa * ring_pf
ring_qvs = (
    2 * np.pi / lam
    * np.vstack(
        [st * np.cos(ring_phi),
         st * np.sin(ring_phi),
         1 - np.cos(ring_theta) * np.ones(n_p)]
    ).T.copy()
)
ring_qms = reborn.utils.vec_mag(ring_qvs)
ring_mask = pmask[idx]
ring_amps = np.zeros(n_p)

acf_sum_pads = np.zeros((n_q, n_p))
acf_sum_ring = np.zeros(n_p)
# simulate
powersof2 = set(int(2 ** x) for x in range(1, 24))
print(f"begining to simulate {n_shots} patterns")
t = time()
for i in range(n_shots):
    if (i + 1) in powersof2:
        print(f"Shot {i+1} @ {time()-t:06f} seconds.", end="\r")
    else:
        print(f"Shot {i+1}", end="\r")
    if simulation_config["protein"]:
        amps[:] = 0
        ring_amps[:] = 0
        rotations = Rotation.random(n_proteins).as_matrix()
        for p, R in enumerate(rotations):
            # U = pvs[p, :]
            q = np.dot(qvs, R)
            r_q = np.dot(ring_qvs, R)
            a = trilinear_interpolation(densities=F.T,
                                        vectors=qvs,
                                        x_min=dmap.q_min,
                                        x_max=dmap.q_max)
            r_a = trilinear_interpolation(densities=F.T,
                                          vectors=ring_qvs,
                                          x_min=dmap.q_min,
                                          x_max=dmap.q_max)
            # a *= np.exp(-1j * (np.dot(q, U.T)))
            # add amplitudes incoherently
            amps += np.abs(a) ** 2
            ring_amps += np.abs(r_a) ** 2
    intensities = f2p * amps
    ring_intensities = ring_f2p * ring_amps * ring_mask
    pints = pa.bin_sum(array=intensities, py=False)
    acf_sum_pads += data_correlation(n=0,
                                     data=pints,
                                     mask=pmask,
                                     cached=False)
    ring_data = ring_intensities - ring_intensities.sum() / ring_mask.sum()
    ring_data[ring_mask == 0] = 0
    ring_dfft = np.fft.fft(ring_data)
    ring_mfft = np.fft.fft(ring_mask)
    d_acf = np.real(np.fft.ifft(ring_dfft * ring_dfft.conj()))
    m_acf = np.real(np.fft.ifft(ring_mfft * ring_mfft.conj()))
    acf_sum_ring += np.divide(d_acf, m_acf, where=m_acf != 0)
    if (i + 1) in powersof2:  # save every 2^n frames
        with h5py.File(h5file, "w") as hf:
            hf.create_dataset(f"{i+1}/acf_sum_pads", data=acf_sum_pads)
            hf.create_dataset(f"{i+1}/acf_sum_ring", data=acf_sum_ring)
