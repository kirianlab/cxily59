# -*- coding: utf-8 -*-
import time
import numpy as np
from reborn import const, source, target, dataframe, detector
from reborn.simulate.examples import CrystalSimulatorV1
from reborn.viewers.qtviews.padviews import PADView
from reborn.fileio.getters import FrameGetter
beam = source.Beam(photon_energy=9000*const.eV)
beam.photon_energy_fwhm = beam.photon_energy * 0.00
beam.diameter_fwhm = 2e-6
beam.beam_divergence_fwhm = 0.00
beam.pulse_energy = beam.photon_energy * 1e12
geom = detector.load_pad_geometry_list('calib/jungfrau_v01.json')
geom.set_average_detector_distance(0.15, beam=beam)
cryst = target.crystal.CrystalStructure('2LYZ')
cryst.crystal_size = 10e-6
cryst.mosaic_domain_size = 0.2e-6
cryst.crystal_size_fwhm = cryst.crystal_size * 0.0
cryst.mosaic_domain_size_fwhm = cryst.mosaic_domain_size * 0.1
cryst.mosaicity_fwhm = 0.0
masks = geom.beamstop_mask(q_min=2*np.pi/500e-10, beam=beam)
simulator = CrystalSimulatorV1(pad_geometry=geom, beam=beam, crystal_structure=cryst, n_iterations=10,
                               approximate_shape_transform=True, expand_symmetry=False,
                               cl_double_precision=False, cl_group_size=32, poisson_noise=True)
class MyFrameGetter(FrameGetter):
    df = dataframe.DataFrame(pad_geometry=geom, beam=beam)
    def get_data(self, frame_number=1):
        pad_data = simulator.generate_pattern()
        self.df.set_raw_data(pad_data)
        return self.df.copy()
frame_getter = MyFrameGetter()
for i in range(10):
    t = time.time()
    dat = frame_getter.get_frame(i)
    datstack = geom.reshape(dat.get_raw_data_flat())
    print(datstack.shape)
    print(time.time() - t, 'seconds')
print(geom)
import pyqtgraph as pg
pg.image(datstack)
pg.mkQApp().exec_()
if 0:
    padview = PADView(frame_getter=frame_getter, mask_data=masks)
    padview.set_levels(-1, 10)
    padview.start()
