import h5py
import numpy as np
import os
import sys
from time import time
from numpy.fft import fftn, ifftn, fftshift, ifftshift
from scipy.spatial.transform import Rotation
import reborn.utils
from reborn import const
from reborn.source import Beam
from reborn.detector import PADGeometryList
from reborn.detector import load_pad_masks
from reborn.misc.interpolate import trilinear_interpolation
from reborn.target import crystal, atoms, placer
from reborn.simulate import solutions, gas, clcore
from reborn.external import denss

t = time()
# Constants
r_e = const.r_e
h = const.h
c = const.c
water_density = 1000
rad90 = np.pi / 2

# configurations
beam_config = {"photon energy": 7 * 1e3 * const.eV,  # All units are SI
               "pulse energy": 0.01e-3,
               "diameter": 110e-9}  # FWHM of x-ray beam (tophat profile at present)
det_config = {"pad id":"jungfrau4M",
              "geometry": "../geometry/recentered_distance_refined_AgBe_run15.json",
              "data type": "calib",
              "mask": "../geometry/dead_pixels.mask",
              "distance": 0.5,
              "binning": [2, 2]}  # For speed, bin the pixels
sample_config = {"name": "PSI Trimer",  # Naming for plots
                 "pdb": "1jb0",
                 "concentration": 20,  # Protein concentration in mg/ml = kg/m^3
                 "diameter": 200e-10}
# sample_config = {"name": "Hemoglobin",  # Naming for plots
#                  "pdb": "2qsp",  # full molecule
#                  "concentration": 20,  # Protein concentration in mg/ml = kg/m^3
#                  "diameter": 110e-10}
dmap_config = {"resolution": 1e-10,  # Minimum resolution for 3D density map.
               "oversample": 3,  # Oversampling factor for 3D density map.
               "ignore waters": False,
               "bioassembly": True,  # Expand atomic coordinates (e.g. virus structure)
               "solvent contrast": True}  # solvent contrast (water)
gas_config = {"path length": [0, None],
              "type": "he",
              "pressure": 100e-6,
              "n simulation steps": 5,
              "temperature": 293}
fxs_config = {"resolution": 0.25e-10,  # Autocorrelation ring resolution
              "oversample": dmap_config["oversample"],
              "n shots": int(1e6)}
injector_conf = {"sheet thickness": 300e-9}
simulation_config = {"poisson": True,
                     "gas": True,
                     "protein": True,  # protein contrast in diffraction amplitudes
                     "one particle": True,  # Fix particle counts to one per drop
                     "sheet": True,  # Include sheet background
                     "bulk water": True,  # bulk water background
                     "use cached files": True,  # Cache files for speed
                     "random_seed": 2023}  # random number generator seed (use None to make it random)}
configs = {"beam": beam_config,
           "detector": det_config,
           "sample": sample_config,
           "density map": dmap_config,
           "gas": gas_config,
           "fxs": fxs_config,
           "injector": injector_conf,
           "simulation": simulation_config}
view_density_projections = True
# Fetch files
if not os.path.exists(sample_config["pdb"]):
    print("Fetching PDB file.")
    sample_config["pdb"] = crystal.get_pdb_file(sample_config["pdb"],
                                                save_path=".",
                                                use_cache=False)
print("*" * 85)
print("INPUT PARAMETERS")
for k, v in configs.items():
    print(f"\t{k}")
    for sk, sv in v.items():
        print(f"\t\t{sk}: {sv}")
print("*" * 80)

beam = Beam(photon_energy=beam_config["photon energy"],
            pulse_energy=beam_config["pulse energy"],
            diameter_fwhm=beam_config["diameter"])
geom = PADGeometryList(filepath=det_config["geometry"])
mask = load_pad_masks(file_name=det_config["mask"])

cryst = denss.get_crystal_structure(pdb_file=sample_config["pdb"],
                                    create_bio_assembly=dmap_config["bioassembly"],
                                    ignore_waters=dmap_config["ignore waters"])
dmap = crystal.CrystalDensityMap(cryst=cryst,
                                 resolution=dmap_config["resolution"],
                                 oversampling=dmap_config["oversample"])
rho, side, extra = denss.create_density_map(
    pdb_file=sample_config["pdb"],
    solvent_contrast=dmap_config["solvent contrast"],
    create_bio_assembly=dmap_config["bioassembly"],
    map_resolution=dmap_config["resolution"],
    map_oversampling=dmap_config["oversample"],
    ignore_waters=dmap_config["ignore waters"]
)
# mrc_file = pdb_file + ".mrc"
# if not os.path.exists(mrc_file):
#     if use_cached_files:
#         print('Loading', mrc_file)
#         saxstats.write_mrc(rho, side, mrc_file)
# else:
#     rho, side = saxstats.read_mrc(mrc_file)
print(time()-t)
if view_density_projections:
    pg.image(np.sum(rho.real, axis=2), title="denss")
    pg.mkQApp().exec_()

hi

# Prepare arrays for simulations
F = fftshift(fftn(ifftshift(rho)))
q_min = dmap.q_min
q_max = dmap.q_max
sheet_volume = np.pi * injector_conf["sheet thickness"] * beam_config["diameter"] ** 2
protein_number_density = (
    sample_config["concentration"] / cryst.molecule.get_molecular_weight()
)
if simulation_config["one particle"]:
    n_proteins = 1
else:
    n_proteins = int(protein_number_density * sheet_volume)
protein_diameter = cryst.molecule.get_size()  # Nominal particle size

lam = beam.wavelength
res, s, n_shots = fxs_config.values()
name, _pdb, concentration, _d = sample_config.values()
d = protein_diameter

q = 2 * np.pi / res
dq = 2 * np.pi / d / s
theta = 2 * np.arcsin(q * lam / 4 / np.pi)
dtheta = lam / s / d / np.cos(theta / 2)  # Angular step in theta
qperp = q * np.cos(theta / 2)  # Component of qvec perpendicular to beam
dphi = dq / qperp
n_phi = int(2 * np.pi / dphi)  # Num. bins in ring
n_phi += n_phi % 2  # Make it even
dphi = 2 * np.pi / n_phi  # Update dphi to integer phis
phi = np.arange(n_phi) * dphi
st = np.sin(theta)
sa = st * dphi * dtheta  # ring bin solid angle
pf = 1 - np.sin(phi) ** 2  # ring bin polarization factors
f2p = const.r_e ** 2 * beam.photon_number_fluence * sa * pf
q_vecs = (
    2 * np.pi / lam
    * np.vstack(
        [st * np.cos(phi),
         st * np.sin(phi),
         (1 - np.cos(theta)) * np.ones(n_phi)]
    ).T.copy()
)
q_mags = reborn.utils.vec_mag(q_vecs)
amps = np.zeros(n_phi, dtype=complex)
acf_sum = np.zeros(n_phi)
acf_sum_noisy = np.zeros(n_phi)
acf_sum_sheet = np.zeros(n_phi)

waterprof = solutions.water_scattering_factor_squared(q=q)
waterprof *= f2p * solutions.water_number_density()

t = time()
for i in range(n_shots):
    doprint = False
    if ((i + 1) % 10 ** np.ceil(np.log10(i + 1))) == 0:
        doprint = True
        print(f"Shot {i+1} @ {time()-t} seconds.")#, end="\r")
    p_vecs = placer.particles_in_a_cylinder(
            cylinder_diameter=beam_config["diameter"],
            cylinder_length=injector_conf["sheet thickness"],
            n_particles=n_proteins,
            particle_diameter=d,
        )
    if simulation_config["protein"]:
        amps[:] = 0
        rotations = Rotation.random(n_proteins).as_matrix()
        for p, R in enumerate(rotations):
            U = p_vecs[p, :]
            q = np.dot(q_vecs, R)
            a = trilinear_interpolation(F.T, q, x_min=q_min, x_max=q_max)
            a *= np.exp(-1j * (np.dot(q, U.T)))
            amps += a
    intensities = f2p * np.abs(amps) ** 2
    I_ring = intensities
    I_ring_noisy = np.random.poisson(I_ring).astype(np.float64)
    I_ring -= np.mean(I_ring)
    acf_sum += np.real(np.fft.ifft(np.abs(np.fft.fft(I_ring)) ** 2))
    I_ring_noisy -= np.mean(I_ring_noisy)
    acf_sum_noisy += np.real(np.fft.ifft(np.abs(np.fft.fft(I_ring_noisy)) ** 2))
    if simulation_config["bulk water"]:
        intensities += waterprof * sheet_volume
    if simulation_config["gas"]:
        print("No gas background yet.")
        # intensities += gasbak
    I_ring_sheet = np.random.poisson(intensities).astype(np.float64)
    I_ring_sheet -= np.mean(I_ring_sheet)
    acf_sum_sheet += np.real(np.fft.ifft(np.abs(np.fft.fft(I_ring_sheet)) ** 2))
    if doprint:
        msg = f"""
        {nppd} particle(s)
        {np.sum(I_ring)} photons in ring
        {np.sum(I_ring)/n_phi} photons per pixel"""
        print(msg)
        with h5py.File(f"{_pdb}_{n_shots}.h5", "w") as hf:
            hf.create_dataset(f"{i+1}/acf_sum", data=acf_sum)
            hf.create_dataset(f"{i+1}/acf_sum_noisy", data=acf_sum_noisy)
            hf.create_dataset(f"{i+1}/acf_sum_sheet", data=acf_sum_sheet)
# look at results
a = 2
b = int(n_phi - a)
p = phi[a:b] * 180 / np.pi
plt.figure()
plt.plot(p, acf_sum[a:b], "-k", label="True")
# set the noisy mean to match noise-free mean
acf_sum_noisy += np.mean(acf_sum[a:b]) - np.mean(acf_sum_noisy[a:b])
plt.plot(p, acf_sum_noisy[a:b], ".g", label="Theoretical")
acf_sum_sheet += np.mean(acf_sum[a:b]) - np.mean(acf_sum_sheet[a:b])
plt.plot(p, acf_sum_sheet[a:b], "xr", label="Background")
plt.title(f'{name} ({res*1e10} $\AA$; {n_shots} shots)')
plt.xlabel(r"$\Delta \phi$ (degrees)")
plt.ylabel(r"$C(q, \Delta\phi)$")
plt.legend()
plt.show(block=True)
print("Done")
