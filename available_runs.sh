#!/bin/bash
xtcfiles=$(ls /sdf/data/lcls/ds/cxi/cxily5921/xtc | grep s00-c00 | cut -d '-' -f 2 | sed 's/r//g')
h5v4files=$(find /sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat -name '[0-9][0-9][0-9][0-9].h5' | cut -d '/' -f 12)
for r in $xtcfiles; do
        h=' xtc'
        [ "$(echo $h5v4files | grep $r)" = "" ] || h=$h' h5v4'
        echo $r $h
done
