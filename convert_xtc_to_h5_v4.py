r"""
README!

This script converts calibrated xtc data (provided by psana) to HDF5.
Strictly speaking this is not necessary, but improves data load times by about half.
The script has no knowledge of previously converted files.
This functionality would be nice, but would complicate the logic.

The script can convert an arbitrary number of exposures (as long as they exist in the run),
and begin/end at an arbitrary frame. This functionality is more useful for debugging.

To convert a select set of data you would want to write your own script.
    - re-use the "conversion_worker" method
    - re-write the main to do what you want
        - likely you provide the event ids you want
        - or sort them by some particular metric
        - save them to your own file (useful for data reduction/hit finding)

The conversion is parallelized. Use:
    python convert_xtc_to_h5.py -h [--help]
to see options.

The data will be chunked.
Missing pad_data will be skipped.
Missing data from smaller detectors (chamber pressure, beam energy, etc.) will be empty.
(see: https://docs.h5py.org/en/stable/high/dataset.html#creating-and-reading-empty-or-null-datasets-and-attributes)
Each file will contain approximately 1000 exposures.

There will be one file generated for each run (named {run#}.h5) in:
    /Users/rca/Developer/sdf/data/lcls/ds/cxi/cxily5921/scratch/data
    ** DO NOT ** write anything to this directory (please)
This is the file you should read when trying to analyze data. The data will be stored there as virtual datasets.
You do not need to worry about this, just know that the data will appear to be large arrays in the single file.
DO NOT load the whole dataset, this will likely cause your session to crash.
100 patterns are ~3.3 GB if loaded as 64 bit arrays; this is a reasonable amount of data to load (per process if
running in parallel).

Useful keys in {run#}.h5
    - event_ids_all: all event ids found by psana
        - shape is (n_events, 3)
        - timestamp format at LCLS is (time[s], time[ns], fiducial)
    - event_ids_good: all event ids which have data saved to disk
        - shape is (n_good_events, 3)
    - data/{data_type}/{event_id}: the pad detector data
        - event_id is string with format: {time[s]}_{time[ns]}_{fiducial}
        - data_type is defaulted to "calib"; you may use "raw" at your discretion
        - shape is (n_good_events, n_pads * n_fs * n_ss)

    ** for the following keys **
        shape is (n_detected_events,)
        there is a corresponding {key}_ids to map the event ids to the data

    - diode: wave-8 laser power diode
    - photon_energy[ev]: xray photon energy
    - gas_monitor: laser power gas monitor
    - pressure[torr]: sample chamber pressure in torr

Thanks for reading.
"""
import os
import sys
from time import time
from glob import glob
import argparse
import h5py
import hdf5plugin
import numpy as np
import psana
from Detector import DetectorTypes
from joblib import Parallel, delayed
from reborn.detector import PADGeometryList

from config import get_config


def conversion_worker(experiment_id, run_number, pad_geometry, event_ids, results_dir, start, stop):
    n_frames_converted = 0
    n_frames_total = event_ids.shape[0]
    n_frames_chunk = stop - start
    n_pixels = pad_geometry.n_pixels
    data_source = psana.DataSource(f"exp={experiment_id}:run={run_number}:idx")
    pad_det = psana.Detector("jungfrau4M", accept_missing=True)
    gas_det = psana.Detector("FEEGasDetEnergy")  # gas monitor
    diode_det = psana.Detector("CXI-USR-DIO")  # diode
    pressure_det = psana.Detector("CXI:SC2:GCC:01:PMON")
    wavelength_det = psana.Detector("SIOC:SYS0:ML00:AO192")  # Backup in case ebeam is missing
    ebeam_det = psana.Detector("EBeam")
    if isinstance(pad_det, DetectorTypes.MissingDet):  # psana missing detector class
        raise TypeError(f"Run {run_number} is missing detector jungfrau4M.")
    run = data_source.runs().__next__()
    chunk_dir = f"{results_dir}/chunks/"
    os.makedirs(chunk_dir, exist_ok=True)
    thisfile = f"{chunk_dir}/{start:06d}-{stop:06d}.h5"
    if os.path.exists(thisfile):
        print(f"Chunk {start}-{stop} exists.  Skip.")
        return
    h5file = h5py.File(thisfile, "w")
    pad_data = h5file.create_dataset("calib", shape=(n_frames_chunk, n_pixels), dtype=np.float32, chunks=(1, n_pixels), **hdf5plugin.Bitshuffle())
    gas_data = np.zeros(n_frames_chunk, dtype=np.float32)
    diode_data = np.zeros(n_frames_chunk, dtype=np.float32)
    pressure_data = np.zeros(n_frames_chunk, dtype=np.float32)
    phot_data = np.zeros(n_frames_chunk, dtype=np.float32)
    wavelength_data = np.zeros(n_frames_chunk, dtype=np.float32)
    event_id_data = np.zeros((n_frames_chunk, 3), dtype=int)
    start_time = time()
    for i in range(n_frames_chunk):
        if start+i >= n_frames_total:
            break
        event_id = event_ids[start+i]
        ts = event_id[0].copy()
        ns = event_id[1].copy()
        tf = event_id[2].copy()
        event_id_data[i, :] = event_id
        hz = i/(time() - start_time)
        missing = " "
        try:
            event = run.event(psana.EventTime(int((ts << 32) | ns), tf))
        except:
            missing += "event,"
            event = None
        try:
            data = pad_det.calib(event)
            data = pad_geometry.concat_data(data)
            data[data < 2] = 0
        except:
            data = np.zeros((pad_geometry.n_pixels))
            missing += "calib,"
        h5file["calib"][i, :] = data
        try:
            gasm = gas_det.get(event)
            gas_data[i] = gasm.f_11_ENRC()
        except:
            missing += "gas,"
        try:
            dio = diode_det.get(event)
            diode_data[i] = -1.0 * dio.peakA()[15]  # diode readout on channel 15
        except:
            missing += "diode,"
        try:
            pressure_data[i] = pressure_det(event)
        except:
            missing += "pressure,"
        try:
            phot_data[i] = (ebeam_det.get(event).ebeamPhotonEnergy())
        except:
            missing += "photon,"
        try:
            wavelength_data[i] = wavelength_det(event)
        except:
            missing += "wavelength,"
        print(f"Run {run_number}. {n_frames_total} frames.  Frame {i} of chunk {start:05d}-{stop:05d}.  {hz:0.2f} Hz. (Missing: {missing[:-1]})")
        n_frames_converted += 1
    pad_data.resize((n_frames_converted, n_pixels))
    h5file["gas_monitor"] = gas_data[:n_frames_converted]
    h5file["diode"] = diode_data[:n_frames_converted]
    h5file["pressure"] = pressure_data[:n_frames_converted]
    h5file["photon_energy"] = phot_data[:n_frames_converted]
    h5file["wavelength_pv"] = wavelength_data[:n_frames_converted]
    h5file["event_ids"] = event_id_data[:n_frames_converted, :]
    h5file.close()
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--run", type=int, default=158, help="Run number")
    parser.add_argument("--start", type=int, default=None, help="start frame number")
    parser.add_argument("--stop", type=int, default=None, help="stop frame number")
    parser.add_argument("--max_events", type=int, default=1e7, help="Maximum number of events to process")
    parser.add_argument("-j", "--n_processes", type=int, default=12, help="Number of parallel processes")
    parser.add_argument("--chunk_size", type=int, default=1000, help="Chunk size (n frames per file)")
    parser.add_argument("--results_dir", type=str, default="", help="Where to save results")
    # parser.add_argument("--geometry_file", type=str, default="./geometry/current.json", help="Path to geometry file (for flattening)")
    args = parser.parse_args()
    tik = time()
    # configurations
    run_number = args.run
    config = get_config(run_number)
    experiment_id = config["experiment_id"]
    njobs = args.n_processes
    pad_geometry = config["pad_detectors"][0]["geometry"]
    results_dir = "/sdf/data/lcls/ds/cxi/cxily5921/scratch/data/v4/h5dat/"+f"{run_number:04d}"  # FIXME
    h5_filepath = f"{results_dir}/{run_number:04d}.h5"
    if os.path.exists(h5_filepath):
        print(f"H5 file already exists: {h5_filepath}")
        sys.exit()
    print(f"Saving hdf5 files to {results_dir}")
    print(f"Master file will be here: {h5_filepath}")
    os.makedirs(results_dir, exist_ok=True)
    data_source = psana.DataSource(f"exp={experiment_id}:run={run_number}:smd")
    print("Finding event ids...")
    event_id_file = f"{results_dir}/all_event_ids.h5"
    if os.path.exists(event_id_file):
        with h5py.File(event_id_file, "r") as f:
            event_ids = f["event_ids"][:]
    else:
        event_ids = []
        counter = 0
        for evt in data_source.events():
            counter += 1
            if not counter%10: print(f"{counter}", end="\r")
            evtId = evt.get(psana.EventId)
            sec = evtId.time()[0]
            ns = evtId.time()[1]
            fid = evtId.fiducials()
            event_ids.append((sec, ns, fid))
        event_ids = np.array(event_ids)
        with h5py.File(event_id_file, "w") as f:
            f["event_ids"] = event_ids
    print(f"{event_ids.shape[0]} events.")
    if args.start is not None:
        event_ids = event_ids[args.start:]
    if args.stop is not None:
        event_ids = event_ids[:args.stop]
    if args.max_events < event_ids.shape[0]:
        event_ids = event_ids[:args.max_events]
    n_events = len(event_ids)
    chunk_size = int(np.ceil(n_events / njobs))
    if chunk_size > args.chunk_size:
        chunk_size = args.chunk_size
    n_chunks = int(np.ceil(n_events / chunk_size))
    starts = np.arange(n_chunks)*chunk_size
    jobids = np.array_split(event_ids, njobs)  # each process will handle these frames
    out = Parallel(n_jobs=njobs)(delayed(conversion_worker)(experiment_id=experiment_id,
                                                            run_number=run_number,
                                                            pad_geometry=pad_geometry,
                                                            event_ids=event_ids,
                                                            results_dir=results_dir,
                                                            start=s,
                                                            stop=s + chunk_size
                                                            )
                                 for s in starts)
    g = sorted(glob(f"{results_dir}/chunks/*.h5"))
    h5files = [h5py.File(f, 'r') for f in g]
    dataset_names = list(h5files[0].keys())
    n_frames = 0
    for f in h5files:
        n_frames += f[dataset_names[0]].shape[0]

    h5out = h5py.File(h5_filepath, "w")
    for dataset_name in dataset_names:
        shape = list(h5files[0][dataset_name].shape)
        shape[0] = n_frames
        shape = tuple(shape)
        layout = h5py.VirtualLayout(shape=shape, dtype=h5files[0][dataset_name].dtype)
        idx = 0
        for dataset in [f[dataset_name] for f in h5files]:
            n = dataset.shape[0]
            vl_slice = [slice(None)] * len(shape)
            vl_slice[0] = slice(idx, idx+n)
            vl_slice = tuple(vl_slice)
            vsource = h5py.VirtualSource(dataset)
            layout[vl_slice] = vsource
            idx += n
        h5out.create_virtual_dataset(dataset_name, layout)
    h5out["/data/calib"] = h5py.SoftLink("/calib")
    h5out["/jungfrau4M/calib"] = h5py.SoftLink("/calib")
    h5out.close()
    print(f"Wrote file {h5_filepath}")
